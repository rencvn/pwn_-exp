#include "stdlib.h"
#include "stdio.h"
#include "unistd.h"
char * heap_list[20];
int size_list[20];
void add()
{
	int index, size;
	puts("index:");

	scanf("%d",&index);
	if(index < 0 || index > 12 || heap_list[index])
	{
		puts("NO WAY");
		exit(1);
	}
	puts("size:");
	scanf("%d",&size);
	size_list[index] = size;
	char * buf = malloc(size);
	heap_list[index] = buf;
	puts("content:");
	read(0,buf,size);
}
void show()
{
	puts("No Baby~");
}
void edit()
{
	int index;
	puts("index:");
	scanf("%d",&index);
	if(index < 0 || index > 19 || !heap_list[index])
	{
		puts("NO WAY");
		exit(1);
	}
	puts("content:");
	read(0,heap_list[index],strlen(heap_list[index]));
}
void del()
{
	int index;
	puts("index:");
	scanf("%d",&index);
	if(index < 0 || index > 19 || !heap_list[index])
	{
		puts("NO WAY");
		exit(1);
	}
	free(heap_list[index]);
	heap_list[index] = 0;
	size_list[index] = 0;
}
void menu()
{
	puts("1:add");
	puts("2:show");
	puts("3:edit");
	puts("4:delete");
}
int main()
{
	setvbuf(stdout, 0, 2, 0);
  	setvbuf(stdin, 0, 1, 0);
  	while(1)
  	{
		puts("I think it is boring and so easy\n");
		menu();
		int choice;
		scanf("%d",&choice);
		switch(choice)
		{
			case 1:
			{
				add();
				break;
			}
			case 2:
			{
				show();
				break;
			}
			case 3:
			{
				edit();
				break;
			}
			case 4:
			{
				del();
				break;
			}
			default:
			{
				exit(1);
			}
		}
	}
	puts("goodbye!");
	return 0;
}
