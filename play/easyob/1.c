
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CHUNK_SIZE 65536
#define NUM_CHUNKS 100

typedef struct spod_chunk {
  // Keep track of references to this object.
  int ref_count;

  char *variable_name;
  void *actual_chunk;
} * chunk_t;

chunk_t GLOBAL_CHUNK_LIST[NUM_CHUNKS];

// Searches for a chunk with a given name.
chunk_t __internal_get_chunk(char *lookup);
chunk_t __internal_create_chunk(char *name);

void *get_variable(char *variable_name) {
  chunk_t chunk = __internal_get_chunk(variable_name);
  // Found it, return it.
  if (chunk != NULL) {
    chunk->ref_count++;
    return chunk->actual_chunk;
  }

  chunk = malloc(sizeof(struct spod_chunk));
  if (chunk == NULL) {
    return NULL;
  }

  chunk->ref_count++;
  return chunk;
}

void del_variable(char *variable_name) {
  chunk_t chunk = __internal_get_chunk(variable_name);
  if (chunk == NULL) {
    return;
  }

  chunk->ref_count--;
  if (chunk->ref_count <= 0) {
    free(chunk->variable_name);
    free(chunk->actual_chunk);
    free(chunk);
  }
}

// NOT EXPORTED.
chunk_t __internal_get_chunk(char *lookup) {
  if (lookup == NULL) {
    return NULL;
  }

  for (int i = 0; i < CHUNK_SIZE; i++) {
    if (GLOBAL_CHUNK_LIST[i] == NULL) {
      continue;
    }

    chunk_t chunk = GLOBAL_CHUNK_LIST[i];
    if (strncmp(chunk->variable_name, lookup, strlen(chunk->variable_name)) ==
        0) {
      return chunk;
    }
  }

  return NULL;
}

// NOT EXPORTED.
chunk_t __internal_create_chunk(char *name) {
  if (name == NULL) {
    return NULL;
  }

  // Find a free spot in out list.
  for (int i = 0; i < CHUNK_SIZE; i++) {
    if (GLOBAL_CHUNK_LIST[i] == NULL) {
      chunk_t chunk = malloc(sizeof(struct spod_chunk));
      if (chunk == NULL) {
        return NULL;
      }

      chunk->ref_count = 1;
      chunk->variable_name = strdup(name);
      if (chunk->variable_name == NULL) {
        free(chunk);
        return NULL;
      }

      chunk->actual_chunk = malloc(CHUNK_SIZE);
      if (chunk->actual_chunk == NULL) {
        free(chunk->variable_name);
        free(chunk);
        return NULL;
      }

      GLOBAL_CHUNK_LIST[i] = chunk;
      return chunk;
    }
  }

  return NULL;
}
