#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "47.108.112.88"
port = 21532
#io = remote(ip,port)
#io = remote("node4.buuoj.cn",26889)
io = process('./stkof')
elf = ELF('./stkof')
libc = elf.libc
#libc = ELF('./libc-2.27.so')
context(log_level='debug',os='linux',arch='amd64')

def choice(c):
	io.sendline(str(c))
def add(size):
	choice(1)
	io.sendline(str(size))
def free(index):
	choice(3)
	io.sendline(str(index))
def edit(index,content):
	choice(2)
	io.sendline(str(index))
	io.sendline(content)

add(0x1000)

gdb.attach(io)

io.interactive()