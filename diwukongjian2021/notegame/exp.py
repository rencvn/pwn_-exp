#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "node4.buuoj.cn"
port = 26123
#io = remote(ip,port)
io = process('./notegame')
elf = ELF('./notegame')
#libc = ELF('./libc-2.27.so')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')


def choice(c):
	io.recvuntil("Note@Game:~$")
	if c == 1:
		io.sendline("AddNote")
	elif c == 2:
		io.sendline("ViewInfo")
	elif c == 3:
		io.sendline("DelNote")
	elif c == 4:
		io.sendline("EditNote")	
	elif c == 5:
		io.sendline("ShowNote")	
	elif c == 6:
		io.sendline("TempNote")	
	elif c == 7:
		io.sendline("B4ckD0or")

def add(size,content):
	choice(1)
	io.recvuntil(":")
	io.sendline(str(size))
	io.recvuntil(":")
	io.sendline(content)

def show(index):
	choice(5)
	io.recvuntil(":")
	io.sendline(str(index))

def free(index):
	choice(3)
	io.recvuntil(":")
	io.sendline(str(index))

def viewinfo():
	choice(2)

def edit(index,content):
	choice(4)
	io.recvuntil(":")
	io.sendline(str(index))
	io.recvuntil(":")
	io.sendline(content)

def Temp(addr,content):
	choice(6)
	io.recvuntil(":")
	io.sendline(addr)
	io.recvuntil(":")
	io.sendline(content)

def backdoor(addr):
	choice(7)
	io.recvuntil(":")
	io.sendline(addr)

add(0x20,'AAAA')
add(0x20,'AAAA')

gdb.attach(io)
io.interactive()