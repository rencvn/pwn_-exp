#!/usr/bin/env python3
import random
import os
import re

MODULE_NAME_RE = re.compile(r"^\w+$")

def show_source():
    print("Source code:")
    with open(__file__) as f:
        print(f.read())

def exam():
    for i in range(100):
        e = '+'.join(f"{random.randint(0, 10)}**{random.randint(0, 10)}" for _ in range(10))
        ans = eval(e)
        ans0 = int(input(e+" = "))
        if ans != ans0:
            print("How dare you are?")
            break
    else:
        print("Good job!Now i give you a chance")
        key = input("key = ")
        value = input("value = ")
        os.environ[key] = value

def calc():
    module = input("I can give you a module: ")
    c = input("Give me your problem: ")
    MATH_RE = re.compile(fr"(?:{module}(?:\.\w+)?)|[()+\-*/&|^%<>=,?:]|(?:\d+\.?\d*(?:e\d+)?)| ")
    if len(MATH_RE.sub('', c))!=0:
        print("You are bad bad.")
    else:
        exec(f"""
import {module}
print({c})
        """)

print(r"""
                                               
           _            _       _             
  ___ __ _| | ___ _   _| | __ _| |_ ___  _ __ 
 / __/ _` | |/ __| | | | |/ _` | __/ _ \| '__|
| (_| (_| | | (__| |_| | | (_| | || (_) | |   
 \___\__,_|_|\___|\__,_|_|\__,_|\__\___/|_|   
                                              
      _                                         
  ___| | __ _ ___ ___ _ __ ___   ___  _ __ ___  
 / __| |/ _` / __/ __| '__/ _ \ / _ \| '_ ` _ \ 
| (__| | (_| \__ \__ \ | | (_) | (_) | | | | | |
 \___|_|\__,_|___/___/_|  \___/ \___/|_| |_| |_|
                                                
""")
while True:
    i = input("1. exam\n2. calc\n3. source code\n>> ")
    if i=="1":
        exam()
    elif i=="2":
        calc()
    elif i=="3":
        show_source()
    else:
        break

