#!/usr/bin/env python
#coding=utf-8

from pwn import*


ip = "113.201.14.253"
port = 16088

#io = remote(ip,port)
io = process('./ciscn_2019_n_4')
#elf = ELF('./rheap')
#libc = ELF('./libc-2.27.so')
libc = ELF('/lib/x86_64-linux-gnu/libc.so.6')
context(log_level='debug',os='linux',arch='amd64')


def chocie(c):
	io.recvuntil(":")
	io.sendline(str(c))

def add(size,content):
	chocie(1)
	io.recvuntil("?")
	io.sendline(str(size))
	io.recvuntil("?")
	io.sendline(content)


io.interactive()