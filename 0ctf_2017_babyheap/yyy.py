from pwn import *

io = process("./0ctf_2017_babyheap")
#io = remote("node4.buuoj.cn",25004)
elf = ELF("./0ctf_2017_babyheap")
libc = ELF("./libc-2.23.so")
#libc = elf.libc
context.log_level = 'debug'


io.recvuntil("Command:")
io.sendline("1")

io.recvuntil("Size:")
io.sendline("20")

io.interactive()