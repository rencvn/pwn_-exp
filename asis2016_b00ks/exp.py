#!/usr/bin/env python2
#coding=utf-8

from pwn import *

io = process('./b00ks')
elf = ELF('./b00ks')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

def start():
	io.recvuntil("Enter author name: ")
	io.sendline("RenCvn")
def choice(c):
	io.recvuntil("> ")
	io.sendline(str(c))
def add(n_size,n_content,d_size,d_content):
	choice(1)
	io.recvuntil("Enter book name size:")
	io.sendline(str(n_size))
	io.recvuntil("Enter book name (Max 32 chars):")
	io.sendline(n_content)
	io.recvuntil("Enter book description size:")
	io.sendline(str(d_size))
	io.recvuntil("Enter book description:")
	io.sendline(d_content)
def free(index):
	choice(2)
	io.recvuntil("Enter the book id you want to delete:")
	io.sendline(str(index))
def edit_d(index,d_content):
	choice(3)
	io.recvuntil("Enter the book id you want to edit:")
	io.sendline(str(index))
	io.recvuntil("Enter new book description: ")
	io.sendline(d_content)
def edit_n(index,name):
	choice(5)
	io.recvuntil("Enter author name:")
	io.sendline(name)
def show():
	choice(4)
def exit():
	choice(6)


start()
add(0x40,'AAAA',0x40,'BBBB')
gdb.attach(io)
edit_d(1)

io.interactive()